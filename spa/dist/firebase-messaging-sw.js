// firebase-messaging-sw.js
importScripts('https://www.gstatic.com/firebasejs/5.5.9/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.5.9/firebase-messaging.js');

const config = {
  apiKey: 'AIzaSyCFoTFviyE2w-BupzS1IDKG9nO1X4ZC5dI',
  authDomain: 'tikkurila-club.firebaseapp.com',
  databaseURL: 'https://tikkurila-club.firebaseio.com',
  projectId: 'tikkurila-club',
  storageBucket: 'tikkurila-club.appspot.com',
  messagingSenderId: '904937621994',
  appId: '1:904937621994:web:a593cb9414de02bb',
};
firebase.initializeApp(config);

const messaging = firebase.messaging();