import Vue from 'vue';
import Router from 'vue-router';
/* AUTH */
import Login from './pages/auth/login/Login.vue';
import LoginExternal from './pages/auth/loginExternal/LoginExternal.vue';
import Registration from './pages/auth/registration/Registration.vue';
import RemindPassword from './pages/auth/RemindPassword.vue';
/* PRIZES */
import Orders from './pages/prizes/Orders.vue';
import Prizes from './pages/prizes/Prizes.vue';
import EpsCard from './pages/prizes/EpsCard.vue';
import EpsCart from './pages/prizes/EpsCart.vue';
/* PROFILE */
import Dashboard from './pages/profile/Dashboard.vue';
import ProfilePassport from './pages/profile/ProfilePassport.vue';
import ProfileEdit from './pages/profile/ProfileEdit.vue';
import ProfilePurse from './pages/profile/ProfilePurse.vue';
import TestWebpush from './pages/profile/TestWebpush.vue';
/* COURSES */
import Courses from './pages/courses/Courses.vue';
import Course from './pages/courses/Course.vue';
import Test from './pages/courses/Test.vue';
import Results from './pages/courses/Results.vue';
/* SALES */
import Actions from './pages/sales/Actions.vue';
import Sales from './pages/sales/Sales.vue';
import AddSale from './pages/sales/addSale/AddSale.vue';
/* NEWS & INSTRUCTIONS */
import News from './pages/news/News.vue';
import Publication from './pages/news/Publication.vue';
import Instructions from './pages/news/Instructions.vue';
import Instruction from './pages/news/Instruction.vue';
/* TICKETS */
import Tickets from './pages/tickets/Tickets.vue';
import Ticket from './pages/tickets/Ticket.vue';
import AddTicket from './pages/tickets/AddTicket.vue';
/* NOTIFICATIONS */
import MobileNotifications from './pages/mobileNotifications/MobileNotifications.vue';
/* COMMON */
import Feedback from './pages/Feedback.vue';
import NotFound from './pages/NotFound.vue';
import Reset from './pages/Reset.vue';

Vue.use(Router);

const routes = [
  { path: '/', component: Login, meta: { requiresGuest: true, requiresChecked: true } },
  { path: '/login-external/:id/:token', component: LoginExternal, props: true, meta: { requiresGuest: true } },
  { path: '/registration', component: Registration, meta: { requiresGuest: true, requiresChecked: true } },
  { path: '/remind', component: RemindPassword, meta: { requiresGuest: true } },
  { path: '/orders', component: Orders, meta: { requiresAuth: true, requiresChecked: true } },
  { path: '/prizes', component: Prizes, meta: { requiresAuth: true, requiresChecked: true } },
  { path: '/eps/:type', component: EpsCard, props: true, meta: { requiresAuth: true, requiresChecked: true } },
  { path: '/eps-cart', component: EpsCart, meta: { requiresAuth: true, requiresChecked: true } },
  { path: '/dashboard', component: Dashboard, meta: { requiresAuth: true } },
  { path: '/passport', component: ProfilePassport, meta: { requiresAuth: true, requiresChecked: true } },
  { path: '/profile', component: ProfileEdit, meta: { requiresAuth: true } },
  { path: '/purse', component: ProfilePurse, meta: { requiresAuth: true, requiresChecked: true } },
  { path: '/webpush', component: TestWebpush, meta: { requiresAuth: true, requiresChecked: true } },
  { path: '/courses', component: Courses, meta: { requiresAuth: true, requiresChecked: true } },
  { path: '/courses/:id', component: Course, props: true, meta: { requiresAuth: true, requiresChecked: true } },
  { path: '/test/:id', component: Test, props: true, meta: { requiresAuth: true, requiresChecked: true } },
  { path: '/results/:id', component: Results, props: true, meta: { requiresAuth: true, requiresChecked: true } },
  { path: '/actions', component: Actions, meta: { requiresAuth: true, requiresChecked: true } },
  { path: '/sales', component: Sales, meta: { requiresAuth: true, requiresChecked: true } },
  { path: '/sale/:id', component: AddSale, props: true, meta: { requiresAuth: true, requiresChecked: true } },
  { path: '/news', component: News, meta: { requiresAuth: true, requiresChecked: true } },
  { path: '/publication/:id', component: Publication, props: true, meta: { requiresAuth: true, requiresChecked: true } },
  { path: '/instructions', component: Instructions, meta: { requiresAuth: true, requiresChecked: true } },
  { path: '/instruction/:id', component: Instruction, props: true, meta: { requiresAuth: true, requiresChecked: true } },
  { path: '/notifications', component: MobileNotifications, meta: { requiresAuth: true, requiresChecked: true } },
  { path: '/tickets/', component: Tickets, meta: { requiresAuth: true, requiresChecked: true } },
  { path: '/add-ticket/', component: AddTicket, meta: { requiresAuth: true, requiresChecked: true } },
  { path: '/ticket/:id', component: Ticket, props: true, meta: { requiresAuth: true, requiresChecked: true } },
  { path: '/reset', component: Reset },
  { path: '/feedback', component: Feedback },
  { path: '*', component: NotFound },
];

const router = new Router({
  mode: 'history',
  routes,
});

router.beforeEach((to, from, next) => {
  Vue.notify({ group: 'error', clean: true });
  const { requiresGuest, requiresAuth, requiresChecked } = to.meta;
  let isAuthenticated = false;
  let isAdminChecked = false;
  const storage = localStorage.getItem('promo');

  if (storage !== null) {
    const { token, user } = JSON.parse(storage);
    if (token !== undefined && token !== null) {
      isAuthenticated = true;
    }

    if (user !== null
      && ((user.role === 'profclub') || (user.role === 'regular' && user.checked_at !== null))) {
      isAdminChecked = true;
    }
  }

  if (requiresGuest && isAuthenticated) {
    next('/dashboard');
  } else {
    next();
  }

  if (requiresAuth && !isAuthenticated) {
    next('/');
  } else {
    next();
  }

  if (requiresChecked && isAuthenticated && !isAdminChecked) {
    next('/dashboard');
  } else {
    next();
  }
});

export default router;
