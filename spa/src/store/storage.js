import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import axios from 'axios';
import eps from './modules/eps';
import payments from './modules/payments';
import surveys from './modules/surveys';
import courses from './modules/courses';
import notifications from './modules/notifications';
import mobileNotifications from './modules/mobileNotifications';
import sales from './modules/sales';
import news from './modules/news';
import tickets from './modules/tickets';
import banners from './modules/banners';
import webpush from '../pages/profile/store/webpush';

Vue.use(Vuex);

const baseURL = process.env.NODE_ENV === 'production' ? 'https://api-tikkurila.msforyou.ru' : 'http://tikkurila.f';

const xToken = '83fkWcODoNgGFm0vLl7udIxpCXHd0YfjXidUb7NaBOWYL34e4toNRAZEPmB9ipIz';

const store = new Vuex.Store({
  plugins: [createPersistedState({ key: 'promo', ssr: false })],
  state: {
    loading: false,
    dialogs: {
      surveys: true,
      notifications: true,
    },
    profile_id: null,
    token: null,
    user: null,
    userTransactions: [],
    paymentsInfo: {},
    settings: {},
  },
  mutations: {
    menuSide(state, settings) {
      state.settings.layout = settings;
    },
    SetSettings(state, settings) {
      state.settings = settings;
    },
    SetUser(state, payload) {
      state.user = payload;
    },
    SetDialog(state, payload) {
      state.dialogs[payload.dialog] = payload.value;
    },
    SetUserTransactions(state, payload) {
      state.userTransactions = payload;
    },
    SetLogged(state, payload) {
      state.profile_id = payload.profile_id;
      state.token = payload.token;
    },
    SetLoading(state, payload) {
      state.loading = payload;
    },
    SetPaymentsSettings(state, payload) {
      state.paymentsInfo.methods = payload;
    },
    Logout(state) {
      state.profile_id = null;
      state.user = null;
      state.token = null;
    },
  },
  actions: {
    UnsetError() {
      Vue.notify({ group: 'error', clean: true });
    },
    UnsetInfo() {
      Vue.notify({ group: 'info', clean: true });
    },
    ShowInfo({ dispatch }, message) {
      dispatch('UnsetError');
      dispatch('UnsetInfo');
      dispatch('ShowInfoMessage', message);
    },
    ShowError({ dispatch }, message) {
      dispatch('UnsetError');
      dispatch('UnsetInfo');
      dispatch('ShowErrorMessage', message);
    },
    ShowErrorMessage({}, message) {
      const text = `<i class="v-icon v-icon--left material-icons mr-1" style="vertical-align:bottom">error_outline</i> ${message}`;
      Vue.notify({ group: 'error', type: 'error', text, duration: 60000 });
    },
    ShowInfoMessage({}, message) {
      const text = `<i class="v-icon v-icon--left material-icons mr-1" style="vertical-align:bottom">notifications</i> ${message}`;
      Vue.notify({ group: 'info', type: 'info', text, duration: 7000 });
    },
    HandleError({ dispatch }, error) {
      if (error.response) {
        // Error recieved from the server
        const { data } = error.response;
        let errors = [];
        if ('error' in data) {
          errors = [data.error];
        } else if ('errors' in data) {
          if (Array.isArray(data.errors)) {
            ({ errors } = data);
          } else {
            Object.keys(data.errors).forEach((k) => {
              errors.push(data.errors[k]);
            });
          }
        }
        // maximum show 2 errors
        if (errors.length > 2) {
          errors = errors.slice(0, 2);
        }
        errors.forEach((message) => {
          dispatch('ShowErrorMessage', message);
        });
      } else if (error.request) {
        // The request was made but no response was received
        dispatch('ShowErrorMessage', 'С вашим подключением не все так просто. Пожалуйста, проверьте доступ к интернету');
      } else {
        // Something happened in setting up the request that triggered an Error
        dispatch('ShowErrorMessage', 'Что-то пошло не так');
        console.log(error.config);
      }
    },
    GetSettings({ commit, getters }) {
      getters.ax.post('api/settings')
        .then((response) => {
          commit('SetSettings', response.data.settings);
        });
    },
    GetProfile({ commit, state, getters }) {
      if (state.profile_id) {
        getters.ax.post('profiles/api/auth/get-profile', { profile_id: state.profile_id })
          .then((response) => {
            commit('SetUser', response.data.profile);
          });
      }
    },
    GetTransactions({ commit, state, getters }) {
      if (state.profile_id) {
        getters.ax.post('profiles/api/transaction/list', { profile_id: state.profile_id })
          .then((response) => {
            commit('SetUserTransactions', response.data.transactions);
          });
      }
    },
    GetPaymentsSettings({ commit, state, getters }) {
      if (state.profile_id) {
        getters.ax.post('payments/api-v3/settings/view')
          .then((response) => {
            commit('SetPaymentsSettings', response.data.settings);
          });
      }
    },
    UserLogin({ commit, dispatch, state }, payload) {
      commit('SetLogged', payload);
      if (state.profile_id) {
        dispatch('eps/GetOrders', state.profile_id);
        dispatch('payments/GetPayments', state.profile_id);
        dispatch('sales/GetSalesList', { profile_id: state.profile_id });
        dispatch('sales/GetActionsList', { profile_id: state.profile_id });
      }
      dispatch('ShowInfoMessage', 'Добро пожаловать!');
    },
    UserLogout({ commit, dispatch }) {
      commit('Logout');
      dispatch('eps/ClearCart');
      localStorage.clear();
      dispatch('GetSettings');
      dispatch('sales/ClearAction');
      dispatch('sales/ClearSale');
      dispatch('sales/ClearSales');
      commit('eps/ClearOrders');
      commit('payments/SetPayments', []);
    },
    DownloadFile({ getters, dispatch }, info) {
      getters.axf.post(info.url, info.data).then((response) => {
        const blob = new Blob([response.data]);
        if (navigator.msSaveBlob) {
          navigator.msSaveBlob(blob, info.filename);
        } else {
          const url = window.URL.createObjectURL(new Blob([response.data]));
          const link = document.createElement('a');
          link.href = url;
          link.setAttribute('download', info.filename);
          document.body.appendChild(link);
          link.click();
          link.parentNode.removeChild(link);
        }
      }).catch((error) => {
        dispatch('HandleError', error);
      });
    },
  },
  getters: {
    ax(state) {
      const ax = axios.create({
        baseURL,
        timeout: 60000,
        headers: { 'Content-Type': 'application/json', 'X-Token': xToken },
      });
      ax.interceptors.request.use(
        (config) => {
          if (state.token !== undefined && state.token) {
            config.headers.Authorization = `Bearer ${state.token}`;
          }
          return config;
        },
        error => Promise.reject(error),
      );
      return ax;
    },
    axl(state) {
      const ax = axios.create({
        baseURL,
        timeout: 60000,
        headers: { 'Content-Type': 'application/json', 'X-Token': xToken },
      });
      ax.interceptors.request.use(
        (config) => {
          if (state.token !== undefined && state.token) {
            config.headers.Authorization = `Bearer ${state.token}`;
          }
          state.loading = true;
          return config;
        },
        error => Promise.reject(error),
      );
      ax.interceptors.response.use(
        (response) => {
          state.loading = false;
          Vue.notify({ group: 'error', clean: true });
          return response;
        },
        (error) => {
          state.loading = false;
          Vue.notify({ group: 'error', clean: true });
          return Promise.reject(error);
        },
      );
      return ax;
    },
    axf(state) {
      const ax = axios.create({
        baseURL,
        timeout: 60000,
        headers: { 'Content-Type': 'application/json', 'X-Token': xToken },
        responseType: 'blob',
      });
      ax.interceptors.request.use(
        (config) => {
          if (state.token !== undefined && state.token) {
            config.headers.Authorization = `Bearer ${state.token}`;
          }
          state.loading = true;
          return config;
        },
        error => Promise.reject(error),
      );
      ax.interceptors.response.use(
        (response) => {
          state.loading = false;
          Vue.notify({ group: 'error', clean: true });
          return response;
        },
        (error) => {
          state.loading = false;
          Vue.notify({ group: 'error', clean: true });
          return Promise.reject(error);
        },
      );
      return ax;
    },
    token(state) {
      return state.token;
    },
    isAuthenticated(state) {
      return state.profile_id !== null && state.profile_id !== undefined
        && state.token !== null && state.token !== undefined;
    },
    isAdminChecked(state) {
      return state.user !== null
        && (state.user.role === 'profclub'
          || (state.user.role === 'regular' && state.user.checked_at !== null));
    },
    hasClubcard(state) {
      return state.user !== null && state.user.role === 'profclub';
    },
  },
  modules: {
    courses,
    eps,
    payments,
    surveys,
    notifications,
    mobileNotifications,
    sales,
    webpush,
    news,
    tickets,
    banners,
  },
});

export default store;
