export default {
  namespaced: true,
  state: {
    bannersList: [],
  },
  actions: {
    GetBannersList({ commit, rootGetters }) {
      rootGetters.ax.post('banners/api/banners/by-group', { group: 'guest-index' })
        .then((response) => {
          commit('setBannersList', response.data.banners);
        });
    },
  },
  mutations: {
    setBannersList(state, payload) {
      state.bannersList = payload;
    },
  },
};
