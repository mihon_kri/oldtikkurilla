export default {
  namespaced: true,
  state: {
    coursesList: [],
    course: {},
    try: null,
    testsList: [],
    testResultsList: [],
  },
  actions: {
    GetCoursesList({ commit, rootGetters }) {
      rootGetters.ax.post('courses/api/course/list')
        .then((response) => {
          commit('clearCourses');
          commit('setCoursesList', response.data.courses);
        });
    },
    GetCourse({ commit, rootGetters }, id) {
      rootGetters.ax.post('courses/api/course/view', { course_id: id })
        .then((response) => {
          commit('setCourse', response.data.course);
        });
    },
    startTry({ commit, rootGetters, dispatch }, payload) {
      rootGetters.ax.post('courses/api/test-try/index', { profile_id: payload.profile_id, test_id: payload.test_id })
        .then((response) => {
          commit('setTry', response.data.try);
        })
        .catch((error) => {
          dispatch('HandleError', error, { root: true });
        });
    },
    GetResultsList({ commit, rootGetters }, payload) {
      rootGetters.ax.post('courses/api/test-try/list', { profile_id: payload.profile_id, test_id: payload.test_id })
        .then((response) => {
          commit('setResultsList', response.data.tries);
        });
    },
  },
  mutations: {
    setCoursesList(state, payload) {
      state.coursesList = payload;
    },
    setCourse(state, payload) {
      state.course = payload;
    },
    setResultsList(state, payload) {
      state.testResultsList = payload;
    },
    setTry(state, payload) {
      state.try = payload;
    },
    clearCourses(state) {
      state.coursesList = [];
      state.course = {};
      state.try = null;
      state.testsList = [];
      state.testResultsList = [];
    },
  },
};
