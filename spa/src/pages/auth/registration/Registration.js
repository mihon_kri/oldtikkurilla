const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
export default {
  data() {
    return {
      profclub: {
        role: 'profclub',
        register: {
          hidePw: true,
          clubcard: '',
          lastName: '',
          firstName: '',
          middleName: '',
          phone: '',
          email: '',
          city: '',
          password: '',
          confirm: '',
          checkedRules: false,
          checkedPers: false,
          code: '',
          sentCode: false,
          token: null,
        },
      },
      regular: {
        role: 'regular',
        register: {
          hidePw: true,
          lastName: '',
          firstName: '',
          middleName: '',
          phone: '',
          email: '',
          city: '',
          workPlace: '',
          password: '',
          confirm: '',
          checkedRules: false,
          checkedPers: false,
          code: '',
          sentCode: false,
          token: null,
        },
      },
      active: 1,
      searchProfclubCity: '',
      searchRegularCity: '',
      cities: [],
      isLoading: false,
      pageRules: null,
      pagePers: null,
      dialogRules: false,
      dialogPers: false,
      rules: {
        required: v => !!v || 'Поле должно быть заполнено',
        email: v => pattern.test(v) || 'Поле E-mail заполнено с ошибками',
        password: v => !!v || 'Укажите пароль',
        phone: v => (!v.length || v.length >= 17) || 'Номер телефона заполнен не полностью',
        clubCard: v => (!v.length || v.length >= 8) || 'Номер клубной карты заполнен не полностью',
        code: v => !!v || 'Укажите проверочный код',
      },
    };
  },
  computed: {
    loading() {
      return this.$store.state.loading;
    },
    requireRules() {
      return this.$store.state.settings.register_rules === true;
    },
    requirePers() {
      return this.$store.state.settings.register_pers === true;
    },
    /** валидация совпадения пароля и поля повтора пароля профклубовцев */
    compareProfclubPasswords() {
      return this.profclub.register.password === this.profclub.register.confirm ? true : 'Пароли не совпадают';
    },
    /** валидация совпадения пароля и поля повтора пароля регуляров */
    compareRegularPasswords() {
      return this.regular.register.password === this.regular.register.confirm ? true : 'Пароли не совпадают';
    },
    /** Валидация полей регистрации профклубовцев */
    profclubRegisterValidate() {
      const reg = this.profclub.register;
      return reg.firstName && reg.lastName && reg.middleName && reg.phone
        && reg.clubcard && reg.email && reg.city.id && reg.password
        && reg.checkedRules && reg.checkedPers
        && reg.confirm && (reg.password === reg.confirm);
    },
    /** Валидация полей регистрации регуляров */
    regularRegisterValidate() {
      const reg = this.regular.register;
      return reg.firstName && reg.lastName && reg.middleName && reg.phone
        && reg.email && reg.city.id && reg.workPlace && reg.password
        && reg.checkedRules && reg.checkedPers
        && reg.confirm && (reg.password === reg.confirm);
    },
  },
  methods: {
    /**
     * Метод отправки смс на телефон регистрирующегося пользователя
     * @param item объект пользователя (профиклубовец или регуляр)
     */
    SendCode(item) {
      const app = this;
      const data = { phone: item.register.phone };
      if (item.role === 'profclub') {
        data.type = 'sms_profile_unregistered';
      } else if (item.role === 'regular') {
        data.type = 'sms';
      }
      app.$store.getters.axl.post('api/token/get-sms', data)
        .then(() => {
          item.register.sentCode = true;
        })
        .catch((error) => {
          app.$store.dispatch('HandleError', error);
        });
    },
    /**
     * Метод проверки кода из смс на регистрирующегося пользователя
     * @param item объект пользователя (профиклубовец или регуляр)
     */
    CheckCode(item) {
      const app = this;
      const data = {
        phone: item.register.phone,
        code: item.register.code,
      };
      if (item.role === 'profclub') {
        data.type = 'sms_profile_unregistered';
      } else if (item.role === 'regular') {
        data.type = 'sms';
      }
      app.$store.getters.axl.post('api/token/check-sms', data)
        .then((response) => {
          item.register.token = response.data.token;
          app.LoadRegInfo(item);
        })
        .catch((error) => {
          app.$store.dispatch('HandleError', error);
        });
    },
    /**
     * Метод предзагрузки данных пользователя если он заведен в админ части.
     * @param item объект пользователя (профиклубовец или регуляр)
     */
    LoadRegInfo(item) {
      const app = this;
      app.$store.getters.ax.post('profiles/api/register/info', { phone: item.register.phone })
        .then((response) => {
          app.pagePers = response.data.pagePers;
          app.pageRules = response.data.pageRules;
          const { profile } = response.data;
          if (profile) {
            item.register.firstName = profile.first_name;
            item.register.lastName = profile.last_name;
            item.register.middleName = profile.middle_name;
            item.register.email = profile.email;
            item.register.city = {
              id: profile.city_id,
              title: profile.city_local,
            };

            if (item.role === 'profclub') {
              item.register.clubcard = profile.clubcard;
            } else if (item.role === 'regular') {
              item.register.workPlace = profile.work_place;
            }
          }
        });
    },
    /**
     * Метод отправки данных для регистрации пользователей
     * @param item объект пользователя (профиклубовец или регуляр)
     */
    UserRegister(item) {
      const app = this;
      const data = {
        role: item.role,
        first_name: item.register.firstName,
        last_name: item.register.lastName,
        middle_name: item.register.middleName,
        phone: item.register.phone,
        email: item.register.email,
        city_id: +item.register.city.id,
        password: item.register.password,
        passwordConfirm: item.register.confirm,
        checkedRules: item.register.checkedRules ? 1 : 0,
        checkedPers: item.register.checkedPers ? 1 : 0,
        token: item.register.token,
      };
      if (item.role === 'profclub') {
        data.clubcard = +item.register.clubcard;
      } else if (item.role === 'regular') {
        data.work_place = item.register.workPlace;
      }
      app.$store.getters.axl.post('profiles/api/register', data)
        .then((response) => {
          app.$store.commit('SetLogged', response.data);
          // app.Reset();
          app.$store.dispatch('ShowInfo', 'Добро пожаловать в программу!');
          app.$router.push('/dashboard');
        })
        .catch((error) => {
          app.$store.dispatch('HandleError', error);
        });
    },
  },
  watch: {
    searchProfclubCity(val) {
      this.isLoading = true;
      val && this.$store.getters.ax.post('profiles/api/city/list', val && { title: val })
        .then((result) => {
          this.cities = result.data.cities;
          this.isLoading = false;
        });
    },
    searchRegularCity(val) {
      this.isLoading = true;
      val && this.$store.getters.ax.post('profiles/api/city/list', val && { title: val })
        .then((result) => {
          this.cities = result.data.cities;
          this.isLoading = false;
        });
    },
  },
};
