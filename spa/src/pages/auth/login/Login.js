export default {
  data() {
    return {
      profclub: {
        role: 'profclub',
        login: {
          hidePw: true,
          login: '',
          password: '',
        },
      },
      regular: {
        role: 'regular',
        login: {
          hidePw: true,
          login: '',
          password: '',
        },
      },
      active: 1,
      isLoading: false,
      rules: {
        required: v => !!v || 'Поле должно быть заполнено',
        password: v => !!v || 'Укажите пароль',
        phone: v => (!v.length || v.length >= 17) || 'Номер телефона заполнен не полностью',
        clubCard: v => (!v.length || v.length >= 8) || 'Номер клубной карты заполнен не полностью',
      },
    };
  },
  computed: {
    loading() {
      return this.$store.state.loading;
    },
    /** Валидация полей авторизации профклубовцев */
    profclubLoginValidate() {
      return this.profclub.login.login && this.profclub.login.password;
    },
    /** Валидация полей авторизации регуляров */
    regularLoginValidate() {
      return this.regular.login.login && this.regular.login.password;
    },
  },
  methods: {
    /**
     * Метод для входа в программу зарегистрированных пользователей.
     * @param item объект пользователя (профиклубовец или регуляр)
     */
    UserLogin(item) {
      const app = this;
      const data = {
        login: item.login.login,
        password: item.login.password,
        role: item.role,
      };
      app.$store.getters.axl.post('profiles/api/auth/login', data)
        .then((response) => {
          app.$store.dispatch('UserLogin', response.data);
          app.$router.push('/dashboard');
        })
        .catch((error) => {
          app.$store.dispatch('HandleError', error);
        });
    },
  },
};
