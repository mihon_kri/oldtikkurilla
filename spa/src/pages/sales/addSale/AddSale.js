export default {
  props: ['id'],
  data() {
    return {
      tables: {
        headers: [
          { text: 'Название', value: 'name', sortable: false, align: 'left' },
          { text: 'Кол-во', value: 'quantity', sortable: false, align: 'center' },
          { text: 'Цена (руб)', value: 'bonuses', sortable: false, align: 'center' },
          { text: '', value: '', sortable: false, align: '' },
        ],
      },
      saleID: this.$props.id,
      showProductForm: false,
      showHistory: false,
      productsSelected: [],
      product: null,
      group: '',
      category: '',
      number: '',
      sum: 0,
      documents: [],
      document: {
        base64: '',
        name: '',
        filename: '',
        url: '',
      },
      searchGroup: '',
      selectedGroups: [],
      searchCategory: '',
      selectedCategories: [],
      searchProduct: '',
      selectedProducts: [],
      isLoading: false,
      isBlocked: false,
      menu: false,
      date: '',
      rules: {
        required: value => !!value || 'Поле должно быть заполнено',
      },
    };
  },
  computed: {
    user() {
      return this.$store.state.user;
    },
    isAuthenticated() {
      return this.$store.getters.isAuthenticated;
    },
    loading() {
      return this.$store.state.loading;
    },
    isEnabled() {
      return !!this.name && !!this.email && !!this.phone && !!this.message;
    },
    sale() {
      return this.$store.state.sales.sale;
    },
    action() {
      return this.$store.state.sales.action;
    },
    groups() {
      return this.$store.state.sales.groups;
    },
    categories() {
      return this.group ? this.$store.state.sales.categories : [];
    },
    soldOn() {
      if (this.date) {
        const [year, month, day] = this.date.split('-');
        return `${day}.${month}.${year}`;
      }
      return '';
    },
    products() {
      if (this.group && this.category) {
        return this.$store.state.sales.products.filter(
          item => (
            (+item.group_id === +this.group.id) && (+item.category_id === +this.category.id)
          ),
        );
      }
      return [];
    },
    validation() {
      return !!(this.productsSelected.length && this.documents.length
        && this.number && this.soldOn);
    },
  },
  methods: {
    /** Получить timestamp. */
    setUniqueId() {
      return (new Date()).getTime();
    },
    /** Устанавливаем список продуктов. */
    SetProducts() {
      this.productsList = this.products.map(item => item.name);
    },
    /** Очищаем форму добавления продукта */
    clearProductForm() {
      this.group = null;
      this.category = null;
      this.product = null;
      this.searchGroup = '';
      this.searchCategory = '';
      this.searchProduct = '';
      this.selectedGroups = [];
      this.selectedCategories = [];
      this.selectedProducts = [];
      this.showProductForm = false;
    },
    /** Добавляем продукт в список */
    AddProduct() {
      if (this.productsSelected.find(item => item.id === this.product.id) === undefined) {
        this.productsSelected.push({
          listID: this.setUniqueId(),
          id: this.product.id,
          photoUrl: this.product.photo_url,
          name: this.product.name,
          bonuses: '',
          quantity: 1,
        });
        this.clearProductForm();
        this.CalculateSum();
      }
    },
    /**
     * Удалить продукт из списка.
     * @param id Идентификатор элемента в списке продуктов.
     * @constructor
     */
    DeleteProduct(id) {
      this.productsSelected = this.productsSelected.filter(item => item.listID !== id);
      this.CalculateSum();
    },
    /**
     * Изменение количества бонусов.
     * @param val {String} Значение поля.
     * @param item {Object} объект выбранного продукта.
     */
    ChangeBonus(val, item) {
      item.bonuses = +val > 0 ? Math.round(+val) : 0;
      this.CalculateSum();
    },
    /**
     * Подсчет суммы баллов за добавленные продукты.
     */
    CalculateSum() {
      this.sum = 0;
      let sum = 0;
      this.isBlocked = false;
      if (this.productsSelected.length) {
        for (let i = 0; i < this.productsSelected.length; i += 1) {
          sum += (this.productsSelected[i].bonuses * this.productsSelected[i].quantity);
        }
      }
      this.sum = sum;
    },
    /**
     * Проверка типа документа.
     * @param item {Object} объект документа.
     * @returns {*|boolean}
     */
    CheckPDF(item) {
      if (item.base64) {
        return (item.base64.indexOf('/pdf;') !== -1);
      }
      if (item.url) {
        return (item.url.indexOf('.pdf') !== -1);
      }
      return false;
    },
    /**
     * @param item {Object} объект документа.
     * @return {*|boolean}
     */
    CheckXLSX(item) {
      if (item.base64) {
        return (item.base64.indexOf('/vnd.openxmlformats-officedocument.spreadsheetml.sheet;') !== -1);
      }
      if (item.url) {
        return (item.url.indexOf('.xlsx') !== -1);
      }
      return false;
    },
    /**
     * Получение пути до файла.
     * @param item объект документа.
     * @returns {*}
     */
    GetFilePath(item) {
      if (item.base64 || item.url) {
        return item.url ? item.url : item.base64;
      }
      return '';
    },
    /** Прикрепить документ. */
    PickDocument() {
      this.$refs.document.click();
    },
    OnDocument(e) {
      this.OnFilePicked(e);
    },
    /**
     * Добавление документа к продаже.
     * @param e эвент
     */
    OnFilePicked(e) {
      const app = this;
      const { files } = e.target;
      if (files[0] !== undefined) {
        const [file] = files;
        if (file.name.lastIndexOf('.') <= 0) {
          return;
        }
        app.document.name = file.name;

        const fr = new FileReader();
        fr.readAsDataURL(file);
        fr.addEventListener('load', () => {
          app.document.filename = null;
          app.document.id = null;
          app.document.base64 = fr.result;
          app.documents.push(app.document);
          app.document = { filename: '', base64: '', name: '', url: '', id: null };
          app.$forceUpdate();
        });
      } else {
        app.document = { filename: '', base64: '', name: '', url: '', id: null };
      }
    },
    /**
     * Удаление документа из списка прикрепленных файлов.
     * @param item объект прикрепленного файла.
     * @param index номер в списке документов.
     */
    deleteFile(item, index) {
      item.name === this.documents[index].name && this.documents.splice(index, 1);
    },
    /**
     * Установка предзагруженных значений при редактировании продажи.
     */
    setDataPreload() {
      if (this.sale && this.saleID) {
        const [day, month, year] = this.sale.sold_on_local.split('.');
        this.date = `${year}-${month}-${day}`;
        this.number = this.sale.number;
        this.productsSelected = this.sale.positions.map(item => (
          {
            bonuses: item.bonuses,
            id: item.product.id,
            listID: this.setUniqueId() + item.product.id,
            name: item.product.name,
            photoUrl: item.product.photo_url,
            quantity: item.quantity,
          }
        ));
        this.documents = this.sale.documents.map(item => (
          {
            id: item.id,
            base64: '',
            filename: null,
            name: item.name,
            url: item.url,
          }
        ));
        this.CalculateSum();
      }
    },
    clearForm() {
      this.productsSelected = [];
      this.product = null;
      this.group = '';
      this.category = '';
      this.soldOn = '';
      this.number = '';
      this.sum = 0;
      this.documents = [];
      this.document = { base64: '', name: '', filename: '', url: '' };
      this.searchGroup = '';
      this.selectedGroups = [];
      this.searchCategory = '';
      this.selectedCategories = [];
      this.searchProduct = '';
      this.selectedProducts = [];
    },
    goBack() {
      this.clearForm();
      this.$store.dispatch('sales/ClearAction');
      this.$router.push('/actions/');
    },
    /**
     * Метод добавления новой продажи.
     */
    onSubmit() {
      const app = this;
      app.isLoading = true;
      let restURL = 'sales/api/sale/create';
      const sale = {
        profile_id: app.user.profile_id,
        action_id: app.action.id,
        number: app.number,
        sold_on_local: app.soldOn,
        positions: app.productsSelected.map(item => ({
          product_id: item.id,
          bonuses: +item.bonuses,
          quantity: item.quantity,
        })),
      };

      if (app.saleID !== 'null') {
        restURL = 'sales/api/sale/update';
        sale.sale_id = app.sale.id;
        sale.documents = app.documents.map((item) => {
          const doc = { name: item.name };
          if (item.id) {
            doc.id = item.id;
            doc.url = item.url;
          } else {
            doc.base64 = item.base64;
          }
          return doc;
        });
      } else {
        app.isBlocked = true;
        sale.documents = app.documents.map(item => ({
          name: item.name,
          base64: item.base64,
        }));
      }

      app.$store.getters.axl.post(restURL, sale)
        .then(() => {
          app.clearForm();
          app.isLoading = false;
          app.$store.dispatch('ShowInfo', 'Регистрация продажи отправлена на проверку');
          app.$router.push('/sales/');
        })
        .catch((error) => {
          app.isLoading = false;
          app.isBlocked = false;
          app.$store.dispatch('HandleError', error);
        });
    },
  },
  mounted() {
    this.SetProducts();
    this.saleID && this.setDataPreload();
  },
};
