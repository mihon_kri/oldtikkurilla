var $ = require('gulp-load-plugins')();
var gulp = require('gulp');
var webpack = require('webpack-stream');
var es = require('event-stream');

var isProd = process.env.NODE_ENV === 'prod';

gulp.task('watch', function () {
    gulp.watch([
        'app/backend/assets/src/sass/**/*.*'
    ], ['sass']);
});

gulp.task('sass', function() {
    let backend = gulp.src(['app/backend/assets/src/sass/*.scss'])
        .pipe($.sass().on('error', $.sass.logError))
        .pipe($.autoprefixer({browsers: ['last 2 versions']}))
        .pipe($.if(isProd, $.cleanCss()))
        .pipe(gulp.dest('app/backend/assets/dist/css'));

    return es.concat(backend);
});

gulp.task('set-node-env-prod', function() {
    return process.env.NODE_ENV = 'prod';
});

gulp.task('default', ['sass', 'watch']);
gulp.task('build', ['set-node-env-prod', 'default']);