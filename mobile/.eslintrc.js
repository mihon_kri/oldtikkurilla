module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module',
  },
  env: {
    browser: true,
  },
  extends: 'airbnb-base',
  plugins: [
    'html',
    'import',
  ],
  rules: {
    'generator-star-spacing': 'off',
    'no-empty-pattern': 'off',
    'no-param-reassign': 'off',
    'no-console': 'off',
    'no-debugger': 'off',
    'no-new': 'off',
    'no-undef': 'off',
    'camelcase': 'off',
    'quote-props': 'off',
    'object-curly-newline': 'off',
    'no-unused-expressions': 'off',
    'no-trailing-spaces': ['error', { 'skipBlankLines': true }],
  },
};
