#!/usr/bin/env bash

# подготовка: обновляем модули-генераторы и cordova до 8:
# npm install -g npm@6.7.0
# npm install -g cordova-splash
# npm install -g cordova-icon
# npm install -g cordova@8.1.2

# Подключаем свой телефон к компу через USB, активируем на телефоне передачу файлов через USB

# 1. Убиваем процесс, который блокирует удаление папки
WMIC PROCESS WHERE "NAME LIKE '%Java(TM)%'" CALL TERMINATE
WMIC PROCESS WHERE "NAME LIKE '%Node.js%'" CALL TERMINATE

# 2. Удаляем платформу (если присуствует) и устанавливаем платформу
cordova platform rm android && cordova platform add android@8.0.0

# 4. Запускаем генерацию иконок и сплэшскрина:
# Перед этим необходимо создать в корне проекта файл "icon.png" размером 1024x1024 без альфа-канала.
# Перед этим необходимо создать в корне проекта файл "splash.png" размером 2208x2208 без альфа-канала.

sed -i -- 's/com.google.gms:google-services:4.1.0/com.google.gms:google-services:4.2.0/g' ./platforms/android/build.gradle
sed -i -- 's/ic_launcher/icon/g' ./platforms/android/app/src/main/AndroidManifest.xml
cp icon_android.png icon.png
cordova-icon && cordova-splash

# 5. Запуск приложения на девайсе:
cordova run android --device