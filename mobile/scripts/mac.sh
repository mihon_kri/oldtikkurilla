#!/usr/bin/env bash

# настройка MacBook Imagick
# brew update
# brew rm imagemagick
# brew install imagemagick@6
# brew link imagemagick@6 --force
# bundle

# настройка MacBook NPM
# npm install -g npm@6.7.0
# npm install -g cordova-splash
# npm install -g app-icon
# npm install -g cordova@7.1.0

# Для начала надо зайти из-под обычного пользователя. И права выставить ему на всю папку "mobile".
# Дело в том, что CocoaPods "pod init" не запустится из-под рута.
# chown -R msforyou *
# su msforyou

# Создаем директорию кордовы и удаляем все платформы
mkdir www
cordova platform rm browser
cordova platform rm android
cordova platform rm ios

# Добавляем платформу iOS и подготавливаем кордову
cordova platform add ios@5.0.0
cordova prepare

# Удаляем дефолтные инонки кордовы и сплэшскрины, очищаем кэш XCode
rm platforms/ios/*/Images.xcassets/AppIcon.appiconset/icon*
rm platforms/ios/*/Images.xcassets/LaunchImage.launchimage/Default*
rm -rf /Users/*/Library/Developer/Xcode/DerivedData/*

# Копируем иконку iOS, генерируем иконки и сплэшскрин
cp icon_ios.png icon.png
app-icon generate
cordova-splash
