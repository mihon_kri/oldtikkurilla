#!/usr/bin/env bash

# Сборка Android для Play Market

# подготовка: обновляем модули-генераторы и cordova до 8:
# npm install -g npm@6.7.0
# npm install -g cordova-splash
# npm install -g cordova-icon
# npm install -g cordova@8.1.2

# 1. Нужно добавить ключ для приложения, делаем один раз. Заменяем {project} на название проекта. Скажем, "promo".
# Пароль точно такойже задаем, {project}.
# шаблон: keytool -v -genkey -v -keystore {project}.keystore -alias {project} -keyalg RSA -validity 10000
# пример: keytool -v -genkey -v -keystore promo.keystore -alias promo -keyalg RSA -validity 10000
# Кладем файл в папку "mobile/{project}.kestore",
# а рядом пишем файл-подсказку с паролем в "mobile/{project}.kestore.info"
# Получившийся файл обязательно сохраняем в корне проекта, в папку "keystore",
# а рядышком сохраняем файл инфо с паролем и алиасом. Что бы инфа с ключом всегда была репозитории и не потерялась.

# 2. Убиваем процесс, который блокирует удаление папки
WMIC PROCESS WHERE "NAME LIKE '%Java(TM)%'" CALL TERMINATE
WMIC PROCESS WHERE "NAME LIKE '%Node.js%'" CALL TERMINATE

# 3. Удаляем платформу (если присуствует) и устанавливаем платформу:
cordova platform rm android && cordova platform add android@8.0.0

# 4. Запускаем генерацию иконок и сплэшскрина:
# Перед этим необходимо создать в корне проекта файл "icon.png" размером 1024x1024 без альфа-канала.
# Перед этим необходимо создать в корне проекта файл "splash.png" размером 2732x2732 без альфа-канала.

sed -i -- 's/com.google.gms:google-services:4.1.0/com.google.gms:google-services:4.2.0/g' ./platforms/android/build.gradle
sed -i -- 's/ic_launcher/icon/g' ./platforms/android/app/src/main/AndroidManifest.xml
cp icon_android.png icon.png
cordova-icon && cordova-splash

# 5. Сборка приложения под Android:
cordova build android --release

# 6. Подписываем ключем наш релиз:
# шаблон: jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore {file} -storepass {password} platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk {alias}
# шаблон: jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore keystore/{project}.keystore -storepass {project} platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk {project}
# пример: jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore keystore/promo.keystore -storepass promo platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk promo

jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore keystore/tikkurila.keystore -storepass tikkurila platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk tikkurila

# 7. Компануем с сжатием наш релиз:
zipalign -f -v 4 platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk scripts/signed.apk

# 8. signed.apk -> https://play.google.com/apps/publish/