/* eslint-disable max-len */
export default {
  props: ['id'],
  data() {
    return {
      tables: {
        headers: [
          { text: 'Название', value: 'name', sortable: false, align: 'left' },
          { text: 'Кол-во', value: 'quantity', sortable: false, align: 'center' },
          { text: 'Цена', value: 'bonuses', sortable: false, align: 'center' },
          { text: '', value: '', sortable: false, align: '' },
        ],
      },
      // file: '/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wgARCAACAAIDAREAAhEBAxEB/8QAFAABAAAAAAAAAAAAAAAAAAAACP/EABQBAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhADEAAAAVSf/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABBQJ//8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAgBAwEBPwF//8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAgBAgEBPwF//8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQAGPwJ//8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPyF//9oADAMBAAIAAwAAABCf/8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAgBAwEBPxB//8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAgBAgEBPxB//8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxB//9k=',
      file: '',
      isDev: false,
      calendar: null,
      showHistory: false,
      saleID: this.$props.id,
      productsSelected: [],
      product: null,
      group: '',
      category: '',
      soldOn: null,
      number: '',
      sum: 0,
      documents: [],
      document: {
        base64: '',
        name: '',
        filename: '',
        url: '',
      },
      searchGroup: '',
      selectedGroups: [],
      searchCategory: '',
      selectedCategories: [],
      searchProduct: '',
      selectedProducts: [],
      isLoading: false,
      isBlocked: false,
      rules: {
        required: value => !!value || 'Поле должно быть заполнено',
      },
    };
  },
  computed: {
    user() {
      return this.$store.state.user;
    },
    isAuthenticated() {
      return this.$store.getters.isAuthenticated;
    },
    loading() {
      return this.$store.state.loading;
    },
    isEnabled() {
      return !!this.name && !!this.email && !!this.phone && !!this.message;
    },
    sale() {
      return this.$store.state.sales.sale;
    },
    action() {
      return this.$store.state.sales.action;
    },
    groups() {
      return this.$store.state.sales.groups;
    },
    categories() {
      return this.group ? this.$store.state.sales.categories : [];
    },
    products() {
      if (this.group && this.category) {
        return this.$store.state.sales.products.filter(
          item => (
            (+item.group_id === +this.group)
            && (+item.category_id === +this.category)
            && item.enabled
          ),
        );
      }
      return [];
    },
    validation() {
      return !!(this.productsSelected.length && this.documents.length
        && this.number && this.soldOn);
    },
  },
  methods: {
    /** Кастомный date-picker */
    OpenCalendar() {
      if (this.calendar == null) {
        this.SetCalendar();
      }
      this.calendar.open();
    },
    SetCalendar() {
      const app = this;
      this.calendar = this.$f7.calendar.create({
        inputEl: '#calendar-create-sale',
        value: [new Date()],
        convertToPopover: false,
        closeOnSelect: true,
        dateFormat: 'dd.mm.yyyy',
        toolbarCloseText: 'Готово',
        dayNamesShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        monthNames: ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь',
          'Июль', 'Авг', 'Сент', 'Окт', 'Нояб', 'Дек'],
        on: {
          close() {
            const date = new Date(app.calendar.value[0]);
            const d = date.getDate();
            const m = date.getMonth() + 1;
            const y = date.getFullYear();
            app.soldOn = `${d <= 9 ? `0${d}` : d}.${m <= 9 ? `0${m}` : m}.${y}`;
          },
        },
      });
    },
    /** Открытие даиалога с предложением загрузить фото. */
    PhotoPopup() {
      this.$f7.actions.open('#register_document');
    },
    PhotoCamera() {
      this.GetPhoto(Camera.PictureSourceType.CAMERA);
    },
    PhotoLibrary() {
      this.GetPhoto(Camera.PictureSourceType.PHOTOLIBRARY);
    },
    /** Загрузка фото или предустановленного файла при isDev=true */
    GetPhoto(type) {
      const app = this;
      if (app.isDev) {
        app.SetPhoto(app.file); // testing purpose
      } else {
        navigator.camera.getPicture(
          (imageData) => {
            app.SetPhoto(imageData);
          },
          (error) => {
            console.log('photo error', error);
          },
          {
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: type,
            quality: 50,
            correctOrientation: true,
          },
        );
      }
    },
    /**
     * Загрузка файла в формате base64 в соответствующий объект поля в стэйте.
     * @param img загружаемый файл в виде строки base64.
     */
    SetPhoto(img) {
      this.documents.push({
        name: `img_${this.setUniqueId()}.jpg`,
        filename: null,
        base64: img,
        id: null,
        url: '',
      });
    },
    /**
     * Метод возвращает url или base64 строку в превью таба.
     * @param {Object} item объект изображения.
     * @returns {string} url или base64 формат изображения.
     */
    showPhoto(item) {
      if (item.url) {
        return item.url;
      }
      if (item.base64) {
        return `data:image/jpeg;base64,${item.base64}`;
      }
      return '';
    },
    /**
     * Удаление документа из списка прикрепленных файлов.
     * @param item объект прикрепленного файла.
     * @param index номер в списке документов.
     */
    deleteFile(item, index) {
      item.name === this.documents[index].name && this.documents.splice(index, 1);
    },
    /**
     * Проверка типа документа.
     * @param item {Object} объект документа.
     * @returns {*|boolean}
     */
    CheckPDF(item) {
      if (item.base64) {
        return (item.base64.indexOf('/pdf;') !== -1);
      }
      if (item.url) {
        return (item.url.indexOf('.pdf') !== -1);
      }
      return false;
    },
    /**
     * Добавить номер продажи
     * @param {Object} e event.
     */
    setNumber(e) {
      this.number = e.target.value;
    },
    /** Получить timestamp. */
    setUniqueId() {
      return (new Date()).getTime();
    },
    /**
      * Метод изменения количества добавленной продуктовой позиции.
      * @param {string} value Значение количества добавленного продукта.
      * @param {number} listID Идентификатор продуктовой позиции в списке выбранных.
      */
    setProductQuantity(value, listID) {
      this.productsSelected = this.productsSelected.map((item) => {
        if (item.listID === listID) {
          item.quantity = value;
        }
        this.CalculateSum();
        return item;
      });
    },
    /**
     * Добавление бонусов к позиции товара.
     * @param e эвент.
     * @param item объект добавленного товара.
     */
    setBonuses(e, item) {
      item.bonuses = e.target.value;
      this.CalculateSum();
    },
    /** Устанавливаем список продуктов. */
    SetProducts() {
      this.productsList = this.products.map(item => item.name);
    },
    /** Добавляем продукт в список */
    AddProduct(e) {
      const filteredProduct = this.products.filter(item => +item.id === +e.target.value);
      console.log(filteredProduct);
      if (filteredProduct.length) {
        this.productsSelected.push({
          listID: this.setUniqueId(),
          id: filteredProduct[0].id,
          photoUrl: filteredProduct[0].photo_url,
          name: filteredProduct[0].name,
          bonuses: '',
          quantity: 1,
        });
      }
      this.product = null;
      this.searchCategory = '';
      this.searchGroup = '';
      this.searchProduct = '';
      this.selectedGroups = [];
      this.selectedCategories = [];
      this.selectedProducts = [];

      this.CalculateSum();
    },
    /**
     * Удалить продукт из списка.
     * @param id Идентификатор элемента в списке продуктов.
     * @constructor
     */
    DeleteProduct(id) {
      this.productsSelected = this.productsSelected.filter(item => item.listID !== id);
      this.CalculateSum();
    },
    /**
     * Изменение количества бонусов.
     * @param e {Object} эвент.
     * @param item {Object} объект выбранного продукта.
     */
    ChangeBonus(val, item) {
      item.bonuses = val;
      this.CalculateSum();
    },
    /**
     * Подсчет суммы баллов за добавленные продукты.
     */
    CalculateSum() {
      this.sum = 0;
      let sum = 0;
      this.isBlocked = false;
      if (this.productsSelected.length) {
        for (let i = 0; i < this.productsSelected.length; i += 1) {
          sum += (this.productsSelected[i].bonuses * this.productsSelected[i].quantity);
        }
      }
      this.sum = sum;
    },
    /**
     * Установка предзагруженных значений при редактировании продажи.
     */
    setDataPreload() {
      if (this.sale && this.saleID) {
        this.soldOn = this.sale.sold_on_local;
        this.number = this.sale.number;
        this.productsSelected = this.sale.positions.map(item => (
          {
            bonuses: item.bonuses,
            id: item.product.id,
            listID: this.setUniqueId() + item.product.id,
            name: item.product.name,
            photoUrl: item.product.photo_url,
            quantity: item.quantity,
          }
        ));
        this.documents = this.sale.documents.map(item => (
          {
            id: item.id,
            base64: '',
            filename: null,
            name: item.name,
            url: item.url,
          }
        ));
        this.CalculateSum();
      }
    },
    clearForm() {
      this.productsSelected = [];
      this.product = null;
      this.group = '';
      this.category = '';
      this.soldOn = '';
      this.number = '';
      this.sum = 0;
      this.documents = [];
      this.document = { base64: '', name: '', filename: '', url: '' };
      this.searchGroup = '';
      this.selectedGroups = [];
      this.searchCategory = '';
      this.selectedCategories = [];
      this.searchProduct = '';
      this.selectedProducts = [];
    },
    goBack() {
      this.clearForm();
      this.$store.dispatch('sales/ClearAction');
      this.$f7.router.app.views.main.router.navigate('/actions/');
    },
    /**
     * Метод добавления новой продажи.
     */
    onSubmit() {
      const app = this;
      app.isLoading = true;
      let restURL = 'sales/api/sale/create';
      const sale = {
        profile_id: app.user.profile_id,
        action_id: app.action.id,
        number: app.number,
        sold_on_local: app.soldOn,
        positions: app.productsSelected.map(item => ({
          product_id: item.id,
          bonuses: +item.bonuses,
          quantity: item.quantity,
        })),
      };

      if (app.saleID !== 'null') {
        restURL = 'sales/api/sale/update';
        sale.sale_id = app.sale.id;
        sale.documents = app.documents.map((item) => {
          const doc = { name: item.name };
          if (item.id) {
            doc.id = item.id;
            doc.url = item.url;
          } else {
            doc.base64 = item.base64;
          }
          return doc;
        });
      } else {
        app.isBlocked = true;
        sale.documents = app.documents.map(item => ({
          name: item.name,
          base64: `data:image/jpeg;base64,${item.base64}`,
        }));
      }

      app.$store.getters.axl.post(restURL, sale)
        .then(() => {
          app.clearForm();
          app.isLoading = false;
          app.$store.dispatch('ShowInfo', 'Регистрация продажи отправлена на проверку');
          app.$f7.router.app.views.main.router.navigate('/sales/');
        })
        .catch((error) => {
          app.isLoading = false;
          app.isBlocked = false;
          app.$store.dispatch('HandleError', error);
        });
    },
  },
  mounted() {
    this.SetProducts();
    this.saleID && this.setDataPreload();
  },
};
