export default {
  data() {
    return {
      REST_URL: this.$root.REST_URL,
      STORAGE_ID: this.$root.STORAGE_ID,
      suggestions: [],
      form: null,
      calendar: null,
      // eslint-disable-next-line max-len
      file: '/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wgARCAACAAIDAREAAhEBAxEB/8QAFAABAAAAAAAAAAAAAAAAAAAACP/EABQBAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhADEAAAAVSf/8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABBQJ//8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAgBAwEBPwF//8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAgBAgEBPwF//8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQAGPwJ//8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPyF//9oADAMBAAIAAwAAABCf/8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAgBAwEBPxB//8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAgBAgEBPxB//8QAFBABAAAAAAAAAAAAAAAAAAAAAP/aAAgBAQABPxB//9k=',
      // file: '',
      isDev: true,
    };
  },
  computed: {
    user() {
      return this.$store.state.user;
    },
    validate() {
      return !!(this.form.birthday_on_local && this.form.last_name
        && this.form.first_name && this.form.middle_name
        && this.form.inn && this.form.address && this.form.document_series_and_number
        && this.form.document1_api.url && this.form.document2_api.url);
    },
  },
  methods: {
    InitForm() {
      this.form = {
        birthday_on_local: this.user.birthday_on || '',
        last_name: this.user.last_name || '',
        first_name: this.user.first_name || '',
        middle_name: this.user.middle_name || '',
        inn: '',
        address: '',
        address_data: null,
        document_series_and_number: '',
        status: '',
        status_html: '',
        status_label: '',
        comments: [],
        document1: { filename: '', webpath: '', src: '' },
        document2: { filename: '', webpath: '', src: '' },
        document1_api: { file: '', url: '', name: '' },
        document2_api: { file: '', url: '', name: '' },
      };
    },
    /** Кастомный date-picker */
    OpenCalendar() {
      if (this.calendar == null) {
        this.SetCalendar();
      }
      this.calendar.open();
    },
    SetCalendar() {
      const app = this;
      this.calendar = this.$f7.calendar.create({
        inputEl: '#calendar-create-sale',
        value: [new Date()],
        convertToPopover: false,
        closeOnSelect: true,
        dateFormat: 'dd.mm.yyyy',
        toolbarCloseText: 'Готово',
        dayNamesShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        monthNames: ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь',
          'Июль', 'Авг', 'Сент', 'Окт', 'Нояб', 'Дек'],
        on: {
          close() {
            const date = new Date(app.calendar.value[0]);
            const d = date.getDate();
            const m = date.getMonth() + 1;
            const y = date.getFullYear();
            app.form.birthday_on_local = `${d <= 9 ? `0${d}` : d}.${m <= 9 ? `0${m}` : m}.${y}`;
          },
        },
      });
    },
    Save() {
      const app = this;

      const data = {
        address: app.form.address,
        birthday_on_local: app.form.birthday_on_local,
        document_series_and_number: app.form.document_series_and_number,
        inn: app.form.inn,
        first_name: app.form.first_name,
        last_name: app.form.last_name,
        middle_name: app.form.middle_name,
      };
      data.profile_id = app.user.profile_id;
      data.document1_api = app.form.document1_api;
      data.document2_api = app.form.document2_api;

      const preloader = this.$f7.dialog.preloader('Подождите...');

      this.$store.getters.axl.post('taxes/api/ndfl/save-passport', data)
        .then(
          () => {
            preloader.close();
            app.$f7.dialog.alert('Анкета сохранена!', 'Уведомление');
          },
          (response) => {
            preloader.close();
            app.$store.dispatch('ShowErrow', response.data);
          },
        );
    },
    // SavePassport() {
    //   const app = this;
    //   const data = app.form;
    //   data.profile_id = app.user.profile_id;
    //
    //   app.$store.getters.axl.post('taxes/api/ndfl/save-passport', data)
    //     .then((response) => {
    //       app.$store.dispatch('ShowInfo', 'Анкета сохранена!');
    //       console.log(response.data);
    //     })
    //     .catch((error) => {
    //       app.$store.dispatch('HandleError', error);
    //     });
    // },
    PhotoPopup(number) {
      if (number === 1) {
        this.$f7.actions.open('#passport-photo-1');
      } else if (number === 2) {
        this.$f7.actions.open('#passport-photo-2');
      }
    },
    getPictureFromCamera(number) {
      this.getPicture(Camera.PictureSourceType.CAMERA, number);
    },
    getPictureFromLibrary(number) {
      this.getPicture(Camera.PictureSourceType.PHOTOLIBRARY, number);
    },
    getPicture(type, number) {
      const options = {
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: type,
        quality: 50,
        correctOrientation: true,
      };
      if (this.isDev) {
        this.addDocument(this.file, number);
      } else {
        navigator.camera.getPicture(
          (imageData) => {
            this.addDocument(imageData, number);
          },
          () => {
            if (this.$f7.device.desktop === true) {
              this.addDocument('test', number);
            }
          },
          options,
        );
      }
    },
    addDocument(image, number) {
      const picture = {
        url: `data:image/jpeg;base64,${image}`,
        file: '',
        name: '',
      };
      if (number === 1) {
        this.form.document1_api = picture;
      } else if (number === 2) {
        this.form.document2_api = picture;
      }
    },
    removeDocument(number) {
      if (number === 1) {
        this.form.document1_api = { file: '', url: '', name: '' };
      } else if (number === 2) {
        this.form.document2_api = { file: '', url: '', name: '' };
      }
    },
  },
  mounted() {
    this.InitForm();
    this.SetCalendar();
  },
};
