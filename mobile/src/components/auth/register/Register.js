const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
export default {
  data() {
    return {
      profclub: {
        role: 'profclub',
        clubcard: '',
        lastName: '',
        firstName: '',
        middleName: '',
        phone: '',
        email: '',
        city: '',
        password: '',
        confirm: '',
        checkedRules: false,
        checkedPers: false,
        code: '',
        sentCode: false,
        token: null,
      },
      regular: {
        role: 'regular',
        lastName: '',
        firstName: '',
        middleName: '',
        phone: '',
        email: '',
        city: '',
        dealer: '',
        password: '',
        confirm: '',
        checkedRules: false,
        checkedPers: false,
        code: '',
        sentCode: false,
        token: null,
      },
      searchProfclubCity: '',
      searchRegularCity: '',
      isLoading: false,
      pageRules: null,
      pagePers: null,
      dialogRules: false,
      dialogPers: false,
      searchCity: '',
      selectedCity: null,
      cities: [],
      rules: {
        required: v => !!v || 'Поле должно быть заполнено',
        email: v => pattern.test(v) || 'Поле E-mail заполнено с ошибками',
        password: v => !!v || 'Укажите пароль',
        phone: v => (!v.length || v.length >= 17) || 'Номер телефона заполнен не полностью',
        clubCard: v => (!v.length || v.length >= 8) || 'Номер клубной карты заполнен не полностью',
        code: v => !!v || 'Укажите проверочный код',
      },
    };
  },
  computed: {
    loading() {
      return this.$store.state.loading;
    },
    requireRules() {
      return 'register_rules' in this.$store.state.settings
        && this.$store.state.settings.register_rules === true;
    },
    requirePers() {
      return 'register_pers' in this.$store.state.settings
        && this.$store.state.settings.register_pers === true;
    },
    /** валидация совпадения пароля и поля повтора пароля профклубовцев */
    compareProfclubPasswords() {
      return this.profclub.password === this.profclub.confirm ? true : 'Пароли не совпадают';
    },
    /** валидация совпадения пароля и поля повтора пароля регуляров */
    compareRegularPasswords() {
      return this.regular.password === this.regular.confirm ? true : 'Пароли не совпадают';
    },
    /** Валидация полей регистрации профклубовцев */
    profclubValidate() {
      const reg = this.profclub;
      return !!(reg.firstName && reg.lastName && reg.middleName && reg.phone
        && reg.clubcard && reg.email && reg.city.id && reg.password
        && reg.checkedRules && reg.checkedPers
        && reg.confirm && (reg.password === reg.confirm));
    },
    /** Валидация полей регистрации регуляров */
    regularValidate() {
      const reg = this.regular;
      return !!(reg.firstName && reg.lastName && reg.middleName && reg.phone
        && reg.email && reg.city.id && reg.dealer && reg.password
        && reg.checkedRules && reg.checkedPers
        && reg.confirm && (reg.password === reg.confirm));
    },
  },
  methods: {
    /** Автопоказ городов и установка выбранного города для регуляра */
    AutocompleteCitiesRegular() {
      const app = this;
      app.$f7.autocomplete.create({
        inputEl: '#regCityRegular input',
        openIn: 'dropdown',
        source: (query, render) => {
          const results = [];
          if (query.length === 0) {
            render(results);
            return;
          }
          for (let i = 0; i < app.cities.length; i += 1) {
            const cityName = app.cities[i].title;
            if (cityName.toLowerCase().indexOf(query.toLowerCase()) >= 0) {
              results.push(cityName);
            }
          }
          render(results);
        },
        on: {
          change: (value) => {
            // eslint-disable-next-line
            app.$store.getters.ax.post('profiles/api/city/list', { title: value[0] })
              .then((response) => {
                // eslint-disable-next-line
                app.regular.city = response.data.cities[0];
              });
          },
        },
      });
    },
    /** Автопоказ городов и установка выбранного города для регуляра */
    AutocompleteCitiesProfclub() {
      const app = this;
      app.$f7.autocomplete.create({
        inputEl: '#regCityProfclub input',
        openIn: 'dropdown',
        source: (query, render) => {
          const results = [];
          if (query.length === 0) {
            render(results);
            return;
          }
          for (let i = 0; i < app.cities.length; i += 1) {
            const cityName = app.cities[i].title;
            if (cityName.toLowerCase().indexOf(query.toLowerCase()) >= 0) {
              results.push(cityName);
            }
          }
          render(results);
        },
        on: {
          change: (value) => {
            // eslint-disable-next-line
            app.$store.getters.ax.post('profiles/api/city/list', { title: value[0] })
              .then((response) => {
                // eslint-disable-next-line
                app.profclub.city = response.data.cities[0];
              });
          },
        },
      });
    },
    /** Подгрузка городов при вводе */
    LoadCities(title = 'Москва') {
      const app = this;
      app.$store.getters.ax.post('profiles/api/city/list', { title })
        .then((response) => {
          app.cities = response.data.cities;
        });
    },
    /**
     * Метод отправки смс на телефон регистрирующегося пользователя
     * @param item объект пользователя (профиклубовец или регуляр)
     */
    SendCode(item) {
      const app = this;
      const data = { phone: item.phone };
      if (item.role === 'profclub') {
        data.type = 'sms_profile_unregistered';
      } else if (item.role === 'regular') {
        data.type = 'sms';
      }
      app.$store.getters.axl.post('api/token/get-sms', data)
        .then(() => {
          item.sentCode = true;
        })
        .catch((error) => {
          app.$store.dispatch('HandleError', error);
        });
    },
    /**
     * Метод проверки кода из смс на регистрирующегося пользователя
     * @param item объект пользователя (профиклубовец или регуляр)
     */
    CheckCode(item) {
      const app = this;
      const data = {
        phone: item.phone,
        code: item.code,
      };
      if (item.role === 'profclub') {
        data.type = 'sms_profile_unregistered';
      } else if (item.role === 'regular') {
        data.type = 'sms';
      }
      app.$store.getters.axl.post('api/token/check-sms', data)
        .then((response) => {
          item.token = response.data.token;
          app.LoadRegInfo(item);
        })
        .catch((error) => {
          app.$store.dispatch('HandleError', error);
        });
    },
    /**
     * Устанавливаем город посетителя.
     * @param e эвент.
     * @param form объект пользователя (профиклубовец или регуляр).
     */
    setCity(e, form) {
      const cityArr = this.cities.filter(item => +item.id === +e.target.value);
      if (cityArr.length) {
        form.city = { ...cityArr[0] };
      }
    },
    /**
     * Устанавливаем торговую точку.
     * @param e эвент.
     * @param form объект пользователя (профиклубовец или регуляр).
     */
    setDealer(e, form) {
      const dealerArr = this.dealers.filter(item => +item.id === +e.target.value);
      if (dealerArr.length) {
        form.dealer = { ...dealerArr[0] };
      }
    },
    /**
     * Метод предзагрузки данных пользователя если он заведен в админ части.
     * @param item объект пользователя (профиклубовец или регуляр)
     */
    LoadRegInfo(item) {
      const app = this;
      app.$store.getters.ax.post('profiles/api/register/info', { phone: item.phone })
        .then((response) => {
          app.pagePers = response.data.pagePers;
          app.pageRules = response.data.pageRules;
          const { profile } = response.data;
          if (profile) {
            item.firstName = profile.first_name;
            item.lastName = profile.last_name;
            item.middleName = profile.middle_name;
            item.email = profile.email;
            item.city = { ...profile.city };

            if (item.role === 'profclub') {
              item.clubcard = profile.clubcard;
            } else if (item.role === 'regular') {
              item.dealer = {
                id: profile.dealer.id,
                name: profile.dealer.name,
              };
            }
          }
        });
    },
    /**
     * Метод отправки данных для регистрации пользователей
     * @param item объект пользователя (профиклубовец или регуляр)
     */
    UserRegister(item) {
      const app = this;
      const data = {
        role: item.role,
        first_name: item.firstName,
        last_name: item.lastName,
        middle_name: item.middleName,
        phone: item.phone,
        email: item.email,
        city_id: item.city.id,
        password: item.password,
        passwordConfirm: item.confirm,
        checkedRules: item.checkedRules ? 1 : 0,
        checkedPers: item.checkedPers ? 1 : 0,
        token: item.token,
      };
      if (item.role === 'profclub') {
        data.clubcard = +item.clubcard;
      } else if (item.role === 'regular') {
        data.work_place = item.dealer;
      }
      app.$store.getters.axl.post('profiles/api/register', data)
        .then((response) => {
          app.$store.dispatch('UserLogin', response.data);
          app.$store.dispatch('ShowInfo', 'Добро пожаловать в программу!');
          app.$f7.router.app.views.main.router.navigate('/dashboard/');
        })
        .catch((error) => {
          app.$store.dispatch('HandleError', error);
        });
    },
  },
  watch: {
    searchCity(val) {
      val && this.LoadCities(val);
    },
  },
  mounted() {
    this.LoadCities();
    this.AutocompleteCitiesRegular();
    this.AutocompleteCitiesProfclub();
  },
};
