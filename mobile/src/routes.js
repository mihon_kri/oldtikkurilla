import Menu from './components/Menu.vue';
import Login from './components/auth/Login.vue';
import Logout from './components/auth/Logout.vue';
import Register from './components/auth/register/Register.vue';
import RemindPassword from './components/auth/RemindPassword.vue';
import Dashboard from './components/profile/Dashboard.vue';
import Eps from './components/prizes/Eps.vue';
import EpsCard from './components/prizes/EpsCard.vue';
import EpsCart from './components/prizes/EpsCart.vue';
import EpsOrders from './components/prizes/EpsOrders.vue';
import Payments from './components/prizes/Payments.vue';
import Feedback from './components/Feedback.vue';
import Push from './components/auth/Push.vue';
import Rules from './components/rules/Rules.vue';
import Actions from './components/sales/Actions.vue';
import Sales from './components/sales/Sales.vue';
import AddSale from './components/sales/addSale/AddSale.vue';
import News from './components/news/News.vue';
import Publication from './components/news/Publication.vue';
import Instructions from './components/news/Instructions.vue';
import Instruction from './components/news/Instruction.vue';
import Notifications from './components/mobileNotifications/MobileNotifications.vue';
import Scan from './components/scan/Scan.vue';
import Geo from './components/geo/Geo.vue';
import Tickets from './components/tickets/Tickets.vue';
import Ticket from './components/tickets/Ticket.vue';
import AddTicket from './components/tickets/AddTicket.vue';
import Passport from './components/passport/Passport.vue';

export default [
  { path: '/menu/', component: Menu },
  { path: '/', component: Login, meta: { requiresGuest: true } },
  { path: '/logout/', component: Logout, meta: { requiresGuest: true } },
  { path: '/register/', component: Register, meta: { requiresGuest: true, back: true } },
  { path: '/remind/', component: RemindPassword, meta: { requiresGuest: true, back: true } },
  { path: '/dashboard/', component: Dashboard, meta: { requiresAuth: true } },
  { path: '/eps/', component: Eps, meta: { requiresAuth: true, requiresChecked: true, back: true } },
  { path: '/eps-card/', component: EpsCard, meta: { requiresAuth: true, requiresChecked: true, back: true } },
  { path: '/eps-orders/', component: EpsOrders, meta: { requiresAuth: true, requiresChecked: true, back: true } },
  { path: '/eps-cart/', component: EpsCart, meta: { requiresAuth: true, requiresChecked: true, back: true } },
  { path: '/payments/', component: Payments, meta: { requiresAuth: true, requiresChecked: true, back: true } },
  { path: '/feedback/', component: Feedback, meta: { requiresAuth: true, back: true } },
  { path: '/push/', component: Push, meta: { requiresAuth: true, requiresChecked: true } },
  { path: '/rules/', component: Rules, meta: { requiresAuth: true, back: true } },
  { path: '/actions', component: Actions, meta: { requiresAuth: true, requiresChecked: true, back: true } },
  { path: '/sales/', component: Sales, meta: { requiresAuth: true, requiresChecked: true, back: true } },
  { path: '/sale/:id', component: AddSale, props: true, meta: { requiresAuth: true, requiresChecked: true, back: true } },
  { path: '/news/', component: News, meta: { requiresAuth: true, requiresChecked: true, back: true } },
  { path: '/publication/:id', component: Publication, props: true, meta: { requiresAuth: true, requiresChecked: true, back: true } },
  { path: '/instructions/', component: Instructions, meta: { requiresAuth: true, requiresChecked: true, back: true } },
  { path: '/instruction/:id', component: Instruction, props: true, meta: { requiresAuth: true, requiresChecked: true, back: true } },
  { path: '/notifications/', component: Notifications, meta: { requiresAuth: true, requiresChecked: true } },
  { path: '/scan/', component: Scan, meta: { requiresAuth: true, requiresChecked: true, back: true } },
  { path: '/geo/', component: Geo, meta: { requiresAuth: true, requiresChecked: true } },
  { path: '/tickets/', component: Tickets, meta: { requiresAuth: true, requiresChecked: true, back: true } },
  { path: '/add-ticket/', component: AddTicket, meta: { requiresAuth: true, requiresChecked: true, back: true } },
  { path: '/ticket/:id', component: Ticket, props: true, meta: { requiresAuth: true, requiresChecked: true, back: true } },
  { path: '/passport/', component: Passport, meta: { requiresAuth: true, requiresChecked: true, back: true } },
];
