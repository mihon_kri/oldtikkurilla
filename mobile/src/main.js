// Import Vue
import Vue from 'vue';

// Import Framework7
import Framework7 from 'framework7/framework7.esm.bundle';

// Import Framework7 Vue
import Framework7Vue from 'framework7-vue/framework7-vue.esm.bundle';

import Dom7 from 'dom7';

import VueTheMask from 'vue-the-mask';

// Import F7 Style
// eslint-disable-next-line
import Framework7CSS from 'framework7/css/framework7.bundle.min.css';

// Import F7 iOS Icons
// eslint-disable-next-line
import Framework7Icons from 'framework7-icons/css/framework7-icons.css';

// Import Material Icons
// eslint-disable-next-line
import MaterialIcons from 'material-design-icons/iconfont/material-icons.css';

// Import Fontawesome Theme Styles
// eslint-disable-next-line
import FontAwesome from 'font-awesome/css/font-awesome.css';

// Import App Component
import app from './App.vue';

// Import Vuex Storage
import store from './store/storage';

// Import custom Style
import './assets/sass/main.scss';

// Different F7-Vue plugin initialization with f7 v3.0
Framework7.use(Framework7Vue);
Vue.use(VueTheMask);

// Global helpers
window.$$ = Dom7;

// установка статусбара
function SetStatusBar() {
  device.platform.toLowerCase() === 'ios'
    ? StatusBar.overlaysWebView(false)
    : StatusBar.overlaysWebView(true);

  // это стиль статусбара темного текста на светлом фоне
  // StatusBar.styleDefault();
  // StatusBar.backgroundColorByHexString('#f5f5f5'); // обычно совпадает с фоном #navbar

  // это стиль статусбара светлого текста на темном фоне
  StatusBar.styleLightContent();
  StatusBar.backgroundColorByHexString('#923a37'); // обычно совпадает с фоном #navbar

  StatusBar.show();
}

// On Device Ready
document.addEventListener('deviceready', () => {
  SetStatusBar();
});

// Init Vue App
const vueMainApp = new Vue({
  // Root Element
  el: '#app',
  store,
  render: c => c('app'),
  components: {
    app,
  },
});

window.vueMain = vueMainApp;
