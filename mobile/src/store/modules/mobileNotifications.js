export default {
  namespaced: true,
  state: {
    notificationsList: [],
    notificationsNew: [],
    hasNotifications: false,
  },
  actions: {
    GetNotifications({ commit, state, rootState, rootGetters }) {
      if (rootState.profile_id) {
        commit('setField', { name: 'notificationsList', value: [] });
        commit('setField', { name: 'hasNotifications', value: false });
        commit('setField', { name: 'notificationsNew', value: [] });
        rootGetters.ax.post('mobile/api/notifications', { profile_id: rootState.profile_id })
          .then((response) => {
            commit('setField', { name: 'notificationsList', value: response.data.notifications });
            const newNotifications = response.data.notifications.filter(item => !item.readed);
            commit('setField', { name: 'notificationsNew', value: newNotifications });
          })
          .then(() => {
            state.notificationsList.length && commit('setField', { name: 'hasNotifications', value: true });
          });
      }
    },
    setNotificationsRead({ rootGetters }, payload) {
      rootGetters.ax.post('mobile/api/notifications/readed', { profile_id: payload.profile_id, notification_ids: payload.notification_ids })
        .then((response) => {
          console.log(response.data);
        });
    },
  },
  mutations: {
    setField(state, payload) {
      state[payload.name] = payload.value;
    },
  },
};
