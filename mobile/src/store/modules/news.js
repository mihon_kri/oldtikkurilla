export default {
  namespaced: true,
  state: {
    news: [],
    publication: null,
    instructions: [],
    instruction: null,
  },
  actions: {
    GetNews({ commit, rootGetters }, id) {
      commit('setField', { name: 'news', value: [] });
      rootGetters.ax.post('news/api/news', { profile_id: id })
        .then((response) => {
          commit('setField', { name: 'news', value: response.data.news });
        });
    },
    SetPublication({ state, commit }, id) {
      commit('setField', { name: 'publication', value: null });
      const tmpArray = state.news.filter(item => item.id === id);
      tmpArray.length && commit('setField', { name: 'publication', value: tmpArray[0] });
    },
    SetPublicationReaded({ rootGetters }, payload) {
      rootGetters.ax.post('news/api/news/readed', {
        profile_id: payload.profile_id,
        news_id: payload.news_id,
      });
    },
    GetInstructions({ commit, rootGetters }, id) {
      commit('setField', { name: 'instructions', value: [] });
      rootGetters.ax.post('news/api/instructions', { profile_id: id })
        .then((response) => {
          commit('setField', { name: 'instructions', value: response.data.instructions });
        });
    },
    SetInstruction({ state, commit }, id) {
      commit('setField', { name: 'instruction', value: null });
      const tmpArray = state.instructions.filter(item => item.id === id);
      tmpArray.length && commit('setField', { name: 'instruction', value: tmpArray[0] });
    },
    SetInstructionReaded({ rootGetters }, payload) {
      rootGetters.ax.post('news/api/instructions/readed', {
        profile_id: payload.profile_id,
        instruction_id: payload.instruction_id,
      });
    },
  },
  mutations: {
    setField(state, payload) {
      state[payload.name] = payload.value;
    },
  },
};
