export default {
  namespaced: true,
  state: {
    cities: [],
    dealers: [],
    selectedCities: [],
  },
  actions: {
    GetCities({ commit, rootGetters }) {
      commit('setField', { name: 'cities', value: [] });
      rootGetters.ax.post('profiles/api/city/list')
        .then((response) => {
          commit('setField', { name: 'cities', value: response.data.cities });
        });
    },
    GetDealers({ commit, rootGetters }) {
      commit('setField', { name: 'dealers', value: [] });
      rootGetters.ax.post('profiles/api/dealer/list')
        .then((response) => {
          commit('setField', { name: 'dealers', value: response.data.dealers });
        });
    },
  },
  mutations: {
    setField(state, payload) {
      state[payload.name] = payload.value;
    },
  },
};
