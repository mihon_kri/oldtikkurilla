export default {
  namespaced: true,
  state: {
    payments: [],
    paymentsMethods: [],
  },
  getters: {
  },
  actions: {
    GetPayments({ commit, rootGetters }, id) {
      rootGetters.ax.post('payments/api-v3/payments/by-profile', { profile_id: id })
        .then((response) => {
          commit('setField', { name: 'payments', value: response.data.payments });
        });
    },
    GetPaymentsSettings({ commit, rootGetters }, payload) {
      if (payload.profile_id) {
        rootGetters.ax.post('payments/api-v3/settings/view')
          .then((response) => {
            if (payload.enabled) {
              commit('setField', {
                name: 'paymentsMethods',
                value: response.data.settings.filter(item => item.enabled),
              });
            } else {
              commit('setField', { name: 'paymentsMethods', value: response.data.settings });
            }
          });
      }
    },
  },
  mutations: {
    setField(state, payload) {
      state[payload.name] = payload.value;
    },
  },
};
