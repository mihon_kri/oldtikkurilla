export default {
  namespaced: true,
  state: {
    eps: null, // текущая выбранная карта ЭПС, для eps-select
    cards: [], // список всех карт ЭПС, доступных для заказа
    orders: [], // заказы ЭПС
    selectedNominal: 0,
    cart: {},
    cartLength: 0,
    cartSummary: 0,
    cartFullSummary: 0,
  },
  mutations: {
    ClearCart(state) {
      state.cart = {};
      state.cartLength = 0;
      state.cartSummary = 0;
    },
    SetCartLength(state, length) {
      state.cartLength = length;
    },
    SetCartSummary(state, summary) {
      state.cartSummary = summary;
    },
    SetCartFullSummary(state, fullSummary) {
      state.cartFullSummary = fullSummary;
    },
    SetCards(state, cards) {
      state.cards = cards;
    },
    SetEpsOrders(state, orders) {
      state.orders = orders;
    },
    SetSelectedCard(state, eps) {
      state.eps = eps;
      state.selectedNominal = 0;
    },
    SetSelectedNominal(state, i) {
      state.selectedNominal = i;
    },
  },
  actions: {
    GetEpsCards({ commit, rootGetters }) {
      rootGetters.ax.post('catalog/api-v3/cards/list')
        .then((response) => {
          commit('SetCards', response.data.cards);
        });
    },
    GetEpsOrders({ commit, rootState, rootGetters }) {
      rootGetters.ax.post('catalog/api-v3/users/orders', { profile_id: rootState.profile_id })
        .then((response) => {
          commit('SetEpsOrders', response.data.orders);
        });
    },
    AddToCart({ state, rootState, dispatch }, info) {
      if (!info.nominal || info.nominal <= 0) {
        window.vueMain.$f7.dialog.alert('Пожалуйста, укажите номинал');
        return;
      }
      if (!info.qty || info.qty <= 0) {
        window.vueMain.$f7.dialog.alert('Пожалуйста, укажите количество сертификатов');
        return;
      }

      console.log(info);
      info.card = state.eps.type;
      info.card_name = state.eps.name;

      const sum = info.qty * info.nominal + state.cartSummary;

      if (sum > rootState.user.balance) {
        window.vueMain.$f7.dialog.alert(`На вашем счету недостаточно средств для добавления товара в корзину.<hr/> Ваш баланс: ${rootState.user.balance}<br/> Сумма корзины: ${sum}`);
        return;
      }

      if (state.cart[info.card]) {
        if (state.cart[info.card][info.nominal]) {
          state.cart[info.card][info.nominal].qty += info.qty;
        } else {
          state.cart[info.card][info.nominal] = info;
        }
      } else {
        state.cart[info.card] = {};
        state.cart[info.card][info.nominal] = info;
      }

      dispatch('CountCart');
      window.vueMain.$f7.dialog.alert('Корзина пополнена!', 'Уведомление', () => {
        window.vueMain.$f7.router.app.views.main.router.navigate('/eps/');
      });
    },
    DeleteCartItem({ state, dispatch }, info) {
      delete state.cart[info.name][info.nom];
      dispatch('CountCart');
    },
    SetEpsAmount({ state, dispatch }, info) {
      state.cart[info.key][info.index].qty = Number(info.amount);
      dispatch('CountCart');
    },
    ProcessEpsOrder({ rootState, rootGetters, state, dispatch }, payload) {
      payload.profile_id = rootState.profile_id;
      rootGetters.axl.post('catalog/api-v3/orders/create', payload)
        .then(() => {
          state.cart = {};
          dispatch('GetProfile', { root: true });
          dispatch('GetEpsOrders');
          dispatch('ClearCart');
          window.vueMain.$f7.dialog.alert('В течение 5 дней сертификаты придут на указанную почту!', 'Уведомление', () => {
            window.vueMain.$f7.router.app.views.main.router.navigate('/eps-orders/');
          });
        })
        .catch((error) => {
          dispatch('HandleError', error, { root: true });
        });
    },
    DeleteEpsOrder({ rootState, rootGetters, dispatch }, order) {
      const payload = { ms_order_id: order.ms_order_id, profile_id: rootState.profile_id };
      rootGetters.axl.post('catalog/api-v3/orders/cancel', payload)
        .then(() => {
          dispatch('GetProfile', { root: true });
          dispatch('GetEpsOrders');
          window.vueMain.$f7.dialog.alert(`Заказ № ${order.ms_order_id} отменен`);
        })
        .catch((error) => {
          dispatch('HandleError', error, { root: true });
        });
    },
    ClearCart({ commit }) {
      commit('ClearCart');
    },
    ClearOrders(state) {
      state.orders = [];
    },
    CountCart({ state, commit }) {
      let cartSummary = 0;
      let cartFullSummary = 0;
      let cartLength = 0;

      Object.keys(state.cart).forEach((cardType) => {
        Object.keys(state.cart[cardType]).forEach((nominal) => {
          cartLength += state.cart[cardType][nominal].qty;
          cartSummary += state.cart[cardType][nominal].qty * parseInt(nominal, 10);
          cartFullSummary += state.cart[cardType][nominal].qty * parseInt(
            state.cart[cardType][nominal].price, 10,
          );
        });
      });

      commit('SetCartLength', cartLength);
      commit('SetCartSummary', cartSummary);
      commit('SetCartFullSummary', cartFullSummary);

      if (cartLength === 0) {
        window.vueMain.$f7.router.app.views.main.router.navigate('/eps/');
      }
    },
  },
};
