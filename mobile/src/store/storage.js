import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import axios from 'axios';
import eps from './modules/eps';
import sales from './modules/sales';
import news from './modules/news';
import mobileNotifications from './modules/mobileNotifications';
import tickets from './modules/tickets';
import register from './modules/register';
import payments from './modules/payments';

Vue.use(Vuex);

const isBrowser = document.URL.indexOf('http://') === 0 || document.URL.indexOf('https://') === 0;
const baseURL = isBrowser ? 'http://tikkurila.f' : 'https://api-tikkurila.msforyou.ru';
const xToken = '83fkWcODoNgGFm0vLl7udIxpCXHd0YfjXidUb7NaBOWYL34e4toNRAZEPmB9ipIz';

export default new Vuex.Store({
  plugins: [createPersistedState({ key: 'promo', ssr: false })],
  state: {
    profile_id: null,
    token: null,
    user: null,
    loading: false,
    routeMeta: {},
    settings: {},
  },
  actions: {
    UserLogin({ commit, dispatch }, payload) {
      commit('SetLogged', payload);
      dispatch('FirebaseRegisterDevice');
      dispatch('FirebaseOnNotification');
    },
    UserLogout({ commit }) {
      commit('Logout');
    },
    GetProfile({ commit, state, getters }) {
      if (state.profile_id) {
        getters.ax.post('profiles/api/auth/get-profile', { profile_id: state.profile_id })
          .then((response) => {
            commit('SetUser', response.data.profile);
          });
      }
    },
    FirebaseRegisterDevice({ state, dispatch }) {
      console.log('> FIREBASE: set interval FirebaseRegisterDevice');

      const interval = setInterval(() => {
        if ('FirebasePlugin' in window && isBrowser === false && state.profile_id) {
          console.log('> FIREBASE: FirebasePlugin in window && isBrowser === false && state.profile_id');
          clearInterval(interval);

          window.FirebasePlugin.hasPermission(
            (data) => {
              if (data.isEnabled) {
                console.log('> FIREBASE: Permission already granted');
              } else {
                window.FirebasePlugin.grantPermission(() => {
                  console.log('Permission granted', data.isEnabled);
                  dispatch('FirebaseGetToken');
                }, (error) => {
                  console.error('> FIREBASE: unable to grant permission', error);
                });
              }
            },
            (error) => {
              console.log('> FIREBASE: hasPermission failed', error);
            },
          );
          dispatch('FirebaseGetToken');
        }
      }, 3 * 1000);
    },
    FirebaseGetToken({ state, getters }) {
      if ('FirebasePlugin' in window) {
        console.log('> FIREBASE: FirebasePlugin.getToken');

        window.FirebasePlugin.getToken((token) => {
          console.log('> FIREBASE: Got token');
          const payload = {
            token,
            platform: device.platform.toLowerCase(),
            profile_id: state.profile_id,
          };
          getters.ax.post('mobile/api/firebase/register-device', payload).then(() => {
            console.log('> FIREBASE: Sent Token');
          });
        });
      }
    },
    FirebaseOnNotification({ state, dispatch }) {
      if (isBrowser) {
        return;
      }
      console.log('> FIREBASE: set interval FirebaseOnNotification');

      const interval = setInterval(() => {
        if ('FirebasePlugin' in window) {
          clearInterval(interval);

          window.FirebasePlugin.onNotificationOpen((data) => {
            console.log('> FIREBASE: notification');
            console.log(data);

            if ('badge' in data && data.badge) {
              cordova.plugins.notification.badge.increase();
            } else {
              if (state.profile_id) {
                dispatch('mobileNotifications/GetNotifications', { root: true });
                if ('GetTickets' in data) {
                  dispatch('tickets/GetTickets', { root: true });
                }
                if ('GetProfile' in data) {
                  dispatch('GetProfile');
                }
              }
              window.vueMain.$f7.notification.create({
                icon: '<i class="fal fa-bell" style="color:#ff141e; font-size:24px; margin-bottom: 10px;"></i>',
                subtitle: data.title,
                text: data.body,
                closeButton: true,
                closeOnClick: true,
                on: {
                  close() {
                    cordova.plugins.notification.badge.clear();
                  },
                },
              }).open();
            }
          });
          console.log('> FIREBASE: Subscribed onNotificationOpen');
        }
      }, 3 * 1000);
    },
    DownloadFile({ getters, dispatch }, info) {
      getters.axf.post(info.url, info.data).then((response) => {
        const blob = new Blob([response.data]);
        if (navigator.msSaveBlob) {
          navigator.msSaveBlob(blob, info.filename);
        } else {
          const url = window.URL.createObjectURL(new Blob([response.data]));
          const link = document.createElement('a');
          link.href = url;
          link.setAttribute('download', info.filename);
          document.body.appendChild(link);
          link.click();
          link.parentNode.removeChild(link);
        }
      }).catch((error) => {
        dispatch('HandleError', error);
      });
    },
    HandleError({}, error) {
      if (error.response) {
        const { data } = error.response;
        let errors = [];
        if ('error' in data) {
          errors = [data.error];
        } else if ('errors' in data) {
          if (Array.isArray(data.errors)) {
            ({ errors } = data);
          } else {
            Object.keys(data.errors).forEach((k) => {
              errors.push(data.errors[k]);
            });
          }
        }
        if (errors.length > 2) {
          errors = errors.slice(0, 2);
        }
        errors.forEach((message) => {
          window.vueMain.$f7.dialog.alert(message);
        });
      } else if (error.request) {
        window.vueMain.$f7.dialog.alert('С вашим подключением не все так просто. Пожалуйста, проверьте доступ к интернету');
      } else {
        window.vueMain.$f7.dialog.alert('Что-то пошло не так');
        console.log(error.config);
      }
    },
    ChangeRouteMeta({ commit }, payload) {
      commit('SetRouteMeta', payload);
    },
    GetSettings({ commit, getters }) {
      getters.ax.post('api/settings')
        .then((response) => {
          commit('SetSettings', response.data.settings);
        });
    },
  },
  mutations: {
    SetRouteMeta(state, payload) {
      state.routeMeta = payload;
    },
    SetLogged(state, payload) {
      state.profile_id = payload.profile_id;
      state.token = payload.token;
    },
    SetUser(state, payload) {
      state.user = payload;
    },
    Logout(state) {
      state.profile_id = null;
      state.user = null;
      state.token = null;
    },
    SetSettings(state, settings) {
      state.settings = settings;
    },
  },
  getters: {
    ax(state) {
      const ax = axios.create({
        baseURL,
        timeout: 60000,
        headers: { 'Content-Type': 'application/json', 'X-Token': xToken },
      });
      ax.interceptors.request.use(
        (config) => {
          if (state.token !== undefined && state.token) {
            config.headers.Authorization = `Bearer ${state.token}`;
          }
          return config;
        },
        error => Promise.reject(error),
      );
      return ax;
    },
    axl(state) {
      const ax = axios.create({
        baseURL,
        timeout: 60000,
        headers: { 'Content-Type': 'application/json', 'X-Token': xToken },
      });
      ax.interceptors.request.use(
        (config) => {
          if (state.token !== undefined && state.token) {
            config.headers.Authorization = `Bearer ${state.token}`;
          }
          window.vueMain.$f7.preloader.show();
          state.loading = true;
          return config;
        },
        error => Promise.reject(error),
      );
      ax.interceptors.response.use(
        (response) => {
          window.vueMain.$f7.preloader.hide();
          state.loading = false;
          return response;
        },
        (error) => {
          window.vueMain.$f7.preloader.hide();
          state.loading = false;
          return Promise.reject(error);
        },
      );
      return ax;
    },
    axf(state) {
      const ax = axios.create({
        baseURL,
        timeout: 60000,
        headers: { 'Content-Type': 'application/json', 'X-Token': xToken },
        responseType: 'blob',
      });
      ax.interceptors.request.use(
        (config) => {
          if (state.token !== undefined && state.token) {
            config.headers.Authorization = `Bearer ${state.token}`;
          }
          window.vueMain.$f7.preloader.show();
          state.loading = true;
          return config;
        },
        error => Promise.reject(error),
      );
      ax.interceptors.response.use(
        (response) => {
          window.vueMain.$f7.preloader.hide();
          state.loading = false;
          return response;
        },
        (error) => {
          window.vueMain.$f7.preloader.hide();
          state.loading = false;
          return Promise.reject(error);
        },
      );
      return ax;
    },
    token(state) {
      return state.token;
    },
    isAuthenticated(state) {
      return state.profile_id !== null && state.profile_id !== undefined
        && state.token !== null && state.token !== undefined;
    },
    isAdminChecked(state) {
      return state.user !== null
        && (state.user.role === 'profclub'
          || (state.user.role === 'regular' && state.user.checked_at !== null));
    },
    hasClubcard(state) {
      return state.user !== null && state.user.role === 'profclub';
    },
  },
  modules: {
    eps,
    sales,
    news,
    mobileNotifications,
    tickets,
    register,
    payments,
  },
});
