<?php

namespace modules\sales\console\controllers;

use console\base\Controller;
use common\components\traits\CommandTrait;
use modules\sales\common\commands\PayBonusesCommand;
use modules\sales\common\commands\CreatePlanBonusesCommand;

class BonusesController extends Controller
{
    use CommandTrait;

    /**
     * Pay bonuses for finished actions
     */
    public function actionPay()
    {
        $this->runInstance(new PayBonusesCommand());
    }

    /**
     * Creates bonuses for completed plans
     */
    public function actionCreatePlanBonuses()
    {
        $this->runInstance(new CreatePlanBonusesCommand());
    }

}