<?php

namespace modules\sales\frontend\forms;

use yii\base\Model;
use modules\sales\common\models\Group;
use modules\sales\common\models\Category;
use modules\sales\common\models\Product;

class ProductListForm extends Model
{
    /**
     * @var integer
     */
    public $group_id;

    /**
     * @var integer
     */
    public $category_id;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['group_id', 'integer'],
            ['group_id', 'exist',
                'targetClass' => Group::class,
                'targetAttribute' => ['group_id' => 'id'],
                'message' => 'Бренд не найдена'
            ],

            ['category_id', 'integer'],
            ['category_id', 'exist',
                'targetClass' => Category::class,
                'targetAttribute' => ['category_id' => 'id'],
                'message' => 'Категория не найдена'
            ],
        ];
    }

    /**
     * @return Product[]|array
     */
    public function search()
    {
        $query = Product::find()
            ->where([
                'enabled' => true,
                'is_show_in_catalog' => true,
            ])
            ->orderBy(['name' => SORT_ASC]);

        $query->andFilterWhere([
            'group_id' => $this->group_id,
            'category_id' => $this->category_id,
        ]);

        return $query->all();
    }


}