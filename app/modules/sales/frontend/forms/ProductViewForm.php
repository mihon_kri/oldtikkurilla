<?php

namespace modules\sales\frontend\forms;

use yii\base\Model;
use modules\sales\common\models\Product;

class ProductViewForm extends Model
{
    /**
     * @var integer
     */
    public $product_id;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['product_id', 'required'],
            ['product_id', 'integer'],
            ['product_id', 'exist',
                'targetClass' => Product::class,
                'targetAttribute' => ['product_id' => 'id'],
                'message' => 'Товар не найдена'
            ],
        ];
    }

    /**
     * @return Product|null
     */
    public function search()
    {
        return Product::findOne(['id' => $this->product_id]);
    }

}