<?php

namespace modules\sales\frontend\controllers\api;

use ms\loyalty\api\frontend\base\ApiController;
use modules\sales\common\models\Category;

class CategoryController extends ApiController
{
    /**
     * @api {post} sales/api/category/list Список категорий
     * @apiDescription Получить список всех категорий
     * @apiName SalesCategoryList
     * @apiGroup Sales
     *
     * @apiSuccess {String} result Статус ответа "OK"
     * @apiSuccess {Object} categories Список категорий
     * @apiSuccess {Integer} categories.id Идентификатор
     * @apiSuccess {String} categories.name Название
     *
     * @apiSuccessExample {json} Пример успешного ответа:
     * {
     *   "result": "OK",
     *   "categories": [
     *     {
     *     "id": 185,
     *      "name": "Адгезионный грунт"
     *     },
     *     {
     *       "id": 171,
     *       "name": "Антисептик кроющий"
     *      }
     *   ]
     * }
     *
     */
    public function actionList()
    {
        $categories = Category::find()
            ->orderBy(['name' => SORT_ASC])
            ->all();

        $this->logResponse = false;

        return $this->ok(['categories' => $categories], 'Получение списка категорий');
    }

}