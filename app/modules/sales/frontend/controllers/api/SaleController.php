<?php

namespace modules\sales\frontend\controllers\api;

use Yii;
use ms\loyalty\api\frontend\base\ApiController;
use modules\profiles\common\models\Profile;
use modules\sales\common\models\Sale;
use modules\sales\frontend\models\ApiSale;

class SaleController extends ApiController
{
    public function actionList()
    {
        $profile = Profile::findOne(['id' => Yii::$app->request->post('profile_id')]);

        if (null === $profile) {
            return $this->error("Не найден участник");
        }

        $sales = Sale::find()
            ->where(['recipient_id' => $profile->id])
            ->orderBy(['created_at' => SORT_DESC])
            ->all();

        $this->logResponse = false;

        return $this->ok(['sales' => $sales], "Получение списка продаж участником");
    }

    public function actionView()
    {
        $profile_id = (int) Yii::$app->request->post('profile_id', null);
        $sale_id = (int) Yii::$app->request->post('sale_id', null);
        $profile = Profile::findOne($profile_id);
        $err = 'Ошибка получения деталей продажи';

        if (null === $profile) {
            return $this->error('Не найден участник', $err);
        }

        $sale = Sale::find()->where(['id' => $sale_id, 'recipient_id' => $profile_id])->one();

        if (null === $sale) {
            return $this->error("У участника не найдена продажа", $err);
        }

        return $this->ok(['sale' => $sale], 'Получение деталей продажи участником');
    }

    /**
     * @api {post} sales/api/sales/create Добавить продажу
     * @apiDescription Добавить новую продажу
     * @apiName SalesCreate
     * @apiGroup Sales
     *
     * @apiParam {Integer} profile_id Индификатор участника
     * @apiParam {Integer} action_id Индификатор акции
     * @apiParam {String} number Номер накладной
     * @apiParam {String} sold_on_local Дата продажи (в формате "dd.mm.yyyy")
     * @apiParam {Object} positions Позиции
     * @apiParam {Integer} positions.product_id Индификатор товара
     * @apiParam {Integer} positions.quantity Количество
     * @apiParam {Integer} positions.bonuses Стоимость
     * @apiParam {Object} documents Документы
     * @apiParam {String} documents.name Название
     * @apiParam {String} documents.base64 Изображение
     *
     * @apiParamExample {json} Пример запроса:
     * {
     *   "profile_id": 1,
     *   "action_id": 1,
     *   "number": "123123",
     *   "sold_on_local": "01.02.2019",
     *   "positions": [
     *     {
     *       "product_id": 1,
     *       "quantity": 1,
     *       "bonuses": 10
     *     },
     *     {
     *       "product_id": 199,
     *       "quantity": 2,
     *       "bonuses": 150
     *     }
     *   ],
     *   "documents": [
     *     {
     *       "name": "burj-khalifa-dubai-03.jpg",
     *       "base64": "data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAZABkAAD="
     *     },
     *     {
     *       "name": "empty.png",
     *       "base64": "data:image/png;base64,iVBORw0KGgo="
     *     }
     *   ]
     * }
     *
     * @apiSuccess {String} result Статус ответа "OK"
     * @apiSuccess {Object} sale Продажа
     * @apiSuccess {Object} sale.id Индификатор
     * @apiSuccess {Object} sale.action_id Индификатор акции
     * @apiSuccess {Object} sale.status Статус
     * @apiSuccess {Object} sale.status_label Описание статуса
     * @apiSuccess {Object} sale.bonuses Сумма бонусов
     * @apiSuccess {Object} sale.sold_on_local Дата продажи
     * @apiSuccess {Object} sale.created_at Дата создания
     * @apiSuccess {Object} sale.number Номер накладной
     * @apiSuccess {Object} sale.documents Документы
     * @apiSuccess {Integer} sale.documents.id Индификатор
     * @apiSuccess {String} sale.documents.name Имя файла
     * @apiSuccess {String} sale.documents.url URL адрес
     * @apiSuccess {Object} sale.positions Список позиций
     * @apiSuccess {Integer} sale.positions.product_id Индификатор товара
     * @apiSuccess {Integer} sale.positions.quantity Количество товаров
     * @apiSuccess {Integer} sale.positions.bonuses Суммарная стоимость
     * @apiSuccess {Object} sale.positions.product Товар
     * @apiSuccess {Integer} sale.positions.product.id Идентификатор товара
     * @apiSuccess {Integer} sale.positions.product.group_id Идентификатор бренда
     * @apiSuccess {Integer} sale.positions.product.category_id Идентификатор категории
     * @apiSuccess {Integer} sale.positions.product.unit_id Идентификатор единицы измерения
     * @apiSuccess {String} sale.positions.product.name Название
     * @apiSuccess {String} sale.positions.product.code Номенклатура
     * @apiSuccess {Integer} sale.positions.product.enabled Признак активности
     * @apiSuccess {Number} sale.positions.product.weight Фасовка
     * @apiSuccess {Object} sale.positions.product.group Бренд товара
     * @apiSuccess {Integer} sale.positions.product.group.id Индификаторв бренда
     * @apiSuccess {String} sale.positions.product.group.name Название бренда
     * @apiSuccess {Object} sale.positions.product.category Категория товара
     * @apiSuccess {Integer} sale.positions.product.category.id Индификаторв категории
     * @apiSuccess {String} sale.positions.product.category.name Название категории
     * @apiSuccess {Object} sale.positions.product.unit Единица измерения
     * @apiSuccess {Integer} sale.positions.product.unit.id Индификатов единицы измерения
     * @apiSuccess {String} sale.positions.product.unit.name Название единицы измерения
     * @apiSuccess {String} sale.positions.product.unit.short_name Короткое название единицы измерения
     * @apiSuccess {Integer} sale.positions.product.unit.quantity_divider Делитель количества
     * @apiSuccess {Object} sale.history История изменения
     * @apiSuccess {Integer} sale.history.id Индификатор
     * @apiSuccess {String} sale.history.created_at Дата создания
     * @apiSuccess {String} sale.history.note Описание
     * @apiSuccess {String} sale.history.comment Коментарий
     * @apiSuccess {Object} sale.action Акция
     * @apiSuccess {Integer} sale.action.id Индификатор акции
     * @apiSuccess {String} sale.action.title Название
     * @apiSuccess {String} sale.action.short_description Короткое описание
     * @apiSuccess {String} sale.action.description Описание
     * @apiSuccess {String} sale.action.start_on Дата начала
     * @apiSuccess {String} sale.action.end_on Дата окончания
     * @apiSuccess {Boolean} sale.action.has_products Отображать или нет выбор продукции
     * @apiSuccess {Object} sale.saleAction Бонус за акцию
     * @apiSuccess {Integer} sale.saleAction.id Индификатор начисления
     * @apiSuccess {Integer} sale.saleAction.bonuses Баллы
     * @apiSuccess {String} sale.saleAction.status Статус (null - баллы начислены или 'paid' - баллы доступны дял трат)
     *
     * @apiSuccessExample {json} Пример успешного ответа:
     * {
     *   "result": "OK",
     *   "sale": {
     *     "id": 32,
     *     "action_id": 1,
     *     "status": "adminReview",
     *     "status_label": "На проверке",
     *     "bonuses": 160,
     *     "sold_on_local": "01.02.2019",
     *     "created_at": "11.02.2019",
     *     "number": "123123",
     *     "documents": [
     *       {
     *         "id": 76,
     *         "name": "32_document_5c618a1d62697.jpg",
     *         "url": "http://api.tikkurila.local/data/sales/32_document_5c618a1d62697.jpg"
     *       },
     *       {
     *         "id": 77,
     *         "name": "32_document_5c618a1d6b258.png",
     *         "url": "http://api.tikkurila.local/data/sales/32_document_5c618a1d6b258.png"
     *       }
     *     ],
     *     "positions": [
     *       {
     *         "product_id": 1,
     *         "quantity": 1,
     *         "bonuses": 10,
     *         "product": {
     *           "id": 1,
     *           "group_id": 1,
     *           "category_id": 1,
     *           "unit_id": 1,
     *           "name": "Антисептик SPILL WOOD бесцв п/гл 0,9л",
     *           "code": "700007255",
     *           "enabled": 1,
     *           "weight": 0.9,
     *           "group": {
     *             "id": 1,
     *             "name": "Finncolor"
     *           },
     *           "category": {
     *             "id": 1,
     *             "name": "Антисептик лессирующий"
     *           },
     *            "unit": {
     *             "id": 1,
     *             "name": "Литр",
     *             "short_name": "л.",
     *             "quantity_divider": 100
     *           }
     *         }
     *       }
     *     ],
     *     "history": [
     *       {
     *         "id": 20,
     *         "created_at": "11.02.2019 17:41",
     *         "note": "Продажа обновлена участником",
     *         "comment": null
     *       }
     *     ],
     *     "action": {
     *       "id": 5,
     *       "title": "Акция для PROFCLUB",
     *       "short_description": "10% от продаж",
     *       "description": "Участники получают 10% от всех подтвержденных продаж",
     *       "start_on": "2019-02-01",
     *       "end_on": "2019-03-10",
     *       "has_products": 1
     *     }
     *     "saleAction": {
     *       "id": 10,
     *       "bonuses": 290,
     *       "status": null
     *     }
     *   }
     * }
     */
    public function actionCreate()
    {
        $model = new ApiSale();
        $model->load(Yii::$app->request->post(), '');

        if ($model->apiSave()) {
            return $this->ok(['sale' => Sale::findOne($model->id)], 'Успешное добавление продажи');
        }

        return $this->error($model->getFirstErrors(), 'Ошибка внесения новой продажи');
    }

    public function actionUpdate()
    {
        $profile_id = (int) Yii::$app->request->post('profile_id');
        $sale_id = (int) Yii::$app->request->post('sale_id');
        $profile = Profile::findOne($profile_id);
        $err = 'Ошибка обновления продажи';

        if (null === $profile) {
            return $this->error('Не найден участник', $err);
        }

        /** @var ApiSale $model */
        $model = ApiSale::find()->where(['id' => $sale_id, 'recipient_id' => $profile_id])->one();

        if (null === $model) {
            return $this->error("У участника не найдена продажа", $err);
        }

        $model->load(Yii::$app->request->post(), '');

        if ($model->apiSave()) {
            return $this->ok(['sale' => Sale::findOne(['id' => $model->id])], 'Успешное обновление продажи');
        }

        return $this->error($model->getFirstErrors(), $err);
    }
}