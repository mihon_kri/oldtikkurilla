<?php

namespace modules\sales\frontend\controllers\api;

use ms\loyalty\api\frontend\base\ApiController;
use modules\sales\common\models\Unit;

class UnitController extends ApiController
{
    /**
     * @api {post} sales/api/unit/list Список ед. измерения
     * @apiDescription Получить список всех единиц измерения
     * @apiName SalesUnitList
     * @apiGroup Sales
     *
     * @apiSuccess {String} result Статус ответа "OK"
     * @apiSuccess {Object} units Список единиц измерения
     * @apiSuccess {Integer} units.id Идентификатор
     * @apiSuccess {String} units.name Название
     * @apiSuccess {String} units.short_name Короткое название
     * @apiSuccess {String} units.quantity_divider Делитель количества
     *
     * @apiSuccessExample {json} Пример успешного ответа:
     * {
     *   "result": "OK",
     *   "units": [
     *     {
     *       "id": 2,
     *       "name": "Килограмм",
     *       "short_name": "кг.",
     *       "quantity_divider": 100
     *     },
     *     {
     *       "id": 1,
     *       "name": "Литр",
     *       "short_name": "л.",
     *       "quantity_divider": 100
     *     }
     *   ]
     * }
     *
     */
    public function actionList()
    {
        $units = Unit::find()
            ->orderBy(['name' => SORT_ASC])
            ->all();

        $this->logResponse = false;

        return $this->ok(['units' => $units], 'Получение списка единиц измерения');
    }

}