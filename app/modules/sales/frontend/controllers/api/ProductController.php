<?php

namespace modules\sales\frontend\controllers\api;

use Yii;
use ms\loyalty\api\frontend\base\ApiController;
use modules\sales\common\models\Product;
use modules\sales\frontend\forms\ProductListForm;
use modules\sales\frontend\forms\ProductViewForm;

/**
 * @apiDefine ProductNotFoundError
 * @apiError ProductNotFound Товар не найден
 * @apiErrorExample {json} Пример ошибки "Товар не найден":
 * HTTP/1.1 400 Bad Request
 * {
 *     "result": "FAIL",
 *     "errors": {
 *         "product_id": "Товар не найден"
 *     }
 * }
 */

class ProductController extends ApiController
{
    /**
     * @api {post} sales/api/product/list Список товаров
     * @apiDescription Получить список всех товаров
     * @apiName SalesProductList
     * @apiGroup Sales
     *
     * @apiParam {Integer} group_id Индификатор бренда (необязательный параметр)
     * @apiParam {Integer} category_id Индификатор категории (необязательный параметр)
     *
     * @apiParamExample {json} Пример запроса:
     * {
     *   "category_id": 185
     * }
     *
     * @apiSuccess {String} result Статус ответа "OK"
     * @apiSuccess {Object} products Список товаров
     * @apiSuccess {Integer} products.id Идентификатор товара
     * @apiSuccess {Integer} products.group_id Идентификатор бренда
     * @apiSuccess {Integer} products.category_id Идентификатор категории
     * @apiSuccess {Integer} products.unit_id Идентификатор единицы измерения
     * @apiSuccess {String} products.name Название
     * @apiSuccess {String} products.code Номенклатура
     * @apiSuccess {Integer} products.enabled Признак активности
     * @apiSuccess {Number} products.weight Фасовка
     * @apiSuccess {Object} products.group Бренд товара
     * @apiSuccess {Integer} products.group.id Индификаторв бренда
     * @apiSuccess {String} products.group.name Название бренда
     * @apiSuccess {Object} products.category Категория товара
     * @apiSuccess {Integer} products.category.id Индификаторв категории
     * @apiSuccess {String} products.category.name Название категории
     * @apiSuccess {Object} products.unit Единица измерения
     * @apiSuccess {Integer} products.unit.id Индификатов единицы измерения
     * @apiSuccess {String} products.unit.name Название единицы измерения
     * @apiSuccess {String} products.unit.short_name Короткое название единицы измерения
     * @apiSuccess {Integer} products.unit.quantity_divider Делитель количества
     *
     * @apiSuccessExample {json} Пример успешного ответа:
     * {
     *   "result": "OK",
     *   "products": [
     *     {
     *       "id": 6859,
     *       "group_id": 3,
     *       "category_id": 2,
     *       "unit_id": 1,
     *       "name": "Адгезионный грунт MINERAL GRUND RPA 2,7л",
     *       "code": "700010658",
     *       "enabled": 1,
     *       "weight": 2.7,
     *       "group": {
     *         "id": 21,
     *         "name": "Finncolor"
     *       },
     *       "category": {
     *         "id": 185,
     *         "name": "Адгезионный грунт"
     *       },
     *       "unit": {
     *         "id": 1,
     *         "name": "Литр",
     *         "short_name": "л.",
     *         "quantity_divider": 100
     *       }
     *     },
     *     {
     *       "id": 6860,
     *       "name": "Адгезионный грунт MINERAL GRUND RPA 9л",
     *       "code": "700010659",
     *       "enabled": 1,
     *       "weight": 9,
     *       "group": {
     *         "id": 21,
     *         "name": "Finncolor"
     *       },
     *       "category": {
     *         "id": 185,
     *         "name": "Адгезионный грунт"
     *       },
     *       "unit": {
     *         "id": 1,
     *         "name": "Литр",
     *         "short_name": "л.",
     *         "quantity_divider": 100
     *       }
     *     }
     *   ]
     * }
     *
     */
    public function actionList()
    {
        $form = new ProductListForm;
        $form->load(Yii::$app->request->post(), '');

        if ($form->validate()) {
            $this->logResponse = false;

            return $this->ok(['products' => $form->search()], 'Получение списка продуктов');
        }

        return $this->error($form->getFirstErrors(), 'Ошибка при получении списка продуктов');
    }

    /**
     * @api {post} sales/api/product/view Просмотр товара
     * @apiDescription Получить товар по его индификатору
     * @apiName SalesProductView
     * @apiGroup Sales
     *
     * @apiParam {Integer} product_id Индификатор товара
     *
     * @apiParamExample {json} Пример запроса:
     * {
     *   "product_id": 185
     * }
     *
     * @apiSuccess {String} result Статус ответа "OK"
     * @apiSuccess {Object} product Объект товара
     * @apiSuccess {Integer} product.id Идентификатор товара
     * @apiSuccess {Integer} product.group_id Идентификатор бренда
     * @apiSuccess {Integer} product.category_id Идентификатор категории
     * @apiSuccess {Integer} product.unit_id Идентификатор единицы измерения
     * @apiSuccess {Integer} product.name Название
     * @apiSuccess {Integer} product.code Номенклатура
     * @apiSuccess {Integer} product.enabled Признак активности
     * @apiSuccess {Number} product.weight Фасовка
     * @apiSuccess {Object} product.group Бренд товара
     * @apiSuccess {Integer} product.group.id Индификаторв бренда
     * @apiSuccess {String} product.group.name Название бренда
     * @apiSuccess {Object} product.category Категория товара
     * @apiSuccess {Integer} product.category.id Индификаторв категории
     * @apiSuccess {String} product.category.name Название категории
     * @apiSuccess {Object} product.unit Единица измерения
     * @apiSuccess {Integer} product.unit.id Индификатов единицы измерения
     * @apiSuccess {String} product.unit.name Название единицы измерения
     * @apiSuccess {String} product.unit.short_name Короткое название единицы измерения
     * @apiSuccess {Integer} product.unit.quantity_divider Делитель количества
     *
     * @apiSuccessExample {json} Пример успешного ответа:
     * {
     *   "result": "OK",
     *   "product": {
     *     "id": 6859,
     *     "group_id": 3,
     *     "category_id": 2,
     *     "unit_id": 1,
     *     "name": "Адгезионный грунт MINERAL GRUND RPA 2,7л",
     *     "code": "700010658",
     *     "enabled": 1,
     *     "weight": 2.7,
     *     "group": {
     *       "id": 21,
     *       "name": "Finncolor"
     *     },
     *     "category": {
     *       "id": 185,
     *       "name": "Адгезионный грунт"
     *     },
     *     "unit": {
     *       "id": 1,
     *       "name": "Литр",
     *       "short_name": "л.",
     *       "quantity_divider": 100
     *     }
     *   }
     * }
     *
     * @apiUse ProductNotFoundError
     */
    public function actionView()
    {
        $form = new ProductViewForm;
        $form->load(Yii::$app->request->post(), '');

        if ($form->validate()) {
            return $this->ok(['product' => $form->search()], 'Получение конкретного товара');
        }

        return $this->error($form->getFirstErrors(), 'Ошибка при получении конкретного товара');
    }
}