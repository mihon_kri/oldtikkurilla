<?php

namespace modules\sales\frontend\controllers\api;

use ms\loyalty\api\frontend\base\ApiController;
use modules\sales\common\models\Group;

class GroupController extends ApiController
{
    /**
     * @api {post} sales/api/group/list Список брендов
     * @apiDescription Получить список всех брендов
     * @apiName SalesGroupList
     * @apiGroup Sales
     *
     * @apiSuccess {String} result Статус ответа "OK"
     * @apiSuccess {Object} groups Список брендов
     * @apiSuccess {Integer} groups.id Идентификатор
     * @apiSuccess {String} groups.name Название
     *
     * @apiSuccessExample {json} Пример успешного ответа:
     * {
     *   "result": "OK",
     *   "groups": [
     *     {
     *     "id": 21,
     *      "name": "Finncolor"
     *     },
     *     {
     *       "id": 21,
     *       "name": "TEKS"
     *      }
     *   ]
     * }
     *
     */
    public function actionList()
    {
        $groups = Group::find()
            ->orderBy(['name' => SORT_ASC])
            ->all();

        $this->logResponse = false;

        return $this->ok(['groups' => $groups], 'Получение списка брендов');
    }

}