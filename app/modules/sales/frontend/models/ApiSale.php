<?php

namespace modules\sales\frontend\models;

use Yii;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use modules\sales\common\models\Sale;
use modules\sales\common\models\SaleHistory;
use modules\sales\common\models\SalePosition;
use modules\sales\common\sales\statuses\Statuses;
use modules\actions\common\models\Action;
use modules\actions\common\validators\ActionAllowValidator;
use modules\profiles\common\validators\ProfileExistsValidator;
use modules\actions\common\validators\ActionSaleLimitsValidator;
use modules\profiles\common\models\Profile;

class ApiSale extends Sale
{
    /**
     * @var integer
     */
    public $profile_id;

    /**
     * @var array
     */
    public $documents;

    /**
     * @var array
     */
    public $positions;

    /**
     * @var Profile
     */
    public $profile;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['sold_on_local', 'required', 'skipOnEmpty' => false],

            ['profile_id', 'required'],
            ['profile_id', ProfileExistsValidator::class, 'loadAttribute' => 'profile'],

            ['action_id', 'required'],
            ['action_id', 'integer'],
            ['action_id', 'exist',
                'targetClass' => Action::class,
                'targetAttribute' => ['action_id' => 'id'],
                'message' => 'Акция не найдена'
            ],
            ['action_id', ActionAllowValidator::class, 'profileIdAttribute' => 'profile_id'],
            ['action_id', ActionSaleLimitsValidator::class],

            ['positions', 'safe'],

            ['documents', 'checkDocuments', 'skipOnEmpty' => false],
        ]);
    }

    /**
     * @return bool
     */
    public function checkDocuments()
    {
        $this->uploadFiles($this->documents, Sale::DATA_DIR, Sale::class);

        if (empty($this->uploadedFiles)) {
            $this->addError('documents', 'Необходимо прикрепить хотя бы один документ (изображение или PDF-файл)');

            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function apiSave()
    {
        if (false === $this->validate()) {
            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();

        try {
            $this->status = Statuses::ADMIN_REVIEW;

            if ($this->isNewRecord) {
                $this->recipient_id = $this->profile->id;
                $this->save(false);
                $this->refresh();
                $this->saveFiles();
            } else {
                $this->save(false);
                $this->saveFiles();
                $this->deleteMissingFiles();
                $this->deletePositions();
            }

            $this->savePositions();
            $this->updateBonuses();
            $this->saveHistory();

            $this->actionManager->verify();

            $transaction->commit();

            return true;

        } catch (Exception $e) {
            $transaction->rollBack();

            return false;
        }
    }

    private function deletePositions()
    {
        $positions = $this->getPositions()->all();
        foreach ($positions as $position) {
            $position->delete();
        }
    }

    private function savePositions()
    {
        if (empty($this->positions)) {
            $this->addError('positions', 'Не указаны позиции');
            throw new Exception();
        }

        foreach ($this->positions as $position) {
            $productId = (int) ArrayHelper::getValue($position, 'product_id');
            $quantity = (int) ArrayHelper::getValue($position, 'quantity');
            $bonuses = (int) ArrayHelper::getValue($position, 'bonuses');

            $salePosition = new SalePosition();
            $salePosition->sale_id = $this->id;
            $salePosition->product_id = $productId;
            $salePosition->quantity = $quantity;
            $salePosition->bonuses = $bonuses;

            if (false === $salePosition->save()) {
                $this->addError('positions', implode(', ', $salePosition->getFirstErrors()));
                throw new Exception();
            }
        }
    }

    private function saveHistory()
    {
        $history = new SaleHistory();
        $history->sale_id = $this->id;
        $history->status_old = empty($this->status) ? null : $this->status;
        $history->status_new = Statuses::ADMIN_REVIEW;
        $history->note = $this->isNewRecord ? 'Продажа внесена участником' : 'Продажа обновлена участником';
        $history->comment = null;
        $history->admin_id = null;
        $history->type = $this->isNewRecord ? SaleHistory::TYPE_CREATE : SaleHistory::TYPE_UPDATE;

        if (false === $history->save(false)) {
            $this->addError('history', implode(', ', $history->getFirstErrors()));
            throw new Exception();
        }

        return true;
    }
}