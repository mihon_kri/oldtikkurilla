<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yz\icons\Icons;

/**
 * @var \yii\web\View $this
 */

$this->title = 'Импорт каталога';
$this->params['header'] = $this->title;
?>

<div class="row">
    <div class="col-sm-offset-2 col-sm-8">
        <a href="/media/catalog_example.xlsx">
            <i class="fa fa-file-excel"></i>
            Скачать пример файл для загрузки каталога
        </a>
    </div>
</div>