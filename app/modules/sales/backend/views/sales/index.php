<?php

use yii\helpers\Html;
use yii\bootstrap\ButtonDropdown;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;
use yz\admin\widgets\Box;
use yz\icons\Icons;
use modules\sales\common\models\Sale;
use modules\sales\common\sales\statuses\Statuses;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\sales\backend\models\SaleSearch $searchModel
 * @var array $columns
 * @var integer $bonuses
 */

$this->title = modules\sales\common\models\Sale::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;

?>

<?php $box = Box::begin(['cssClass' => 'sale-index box-primary']) ?>
<div class="text-right">
    <?php echo ActionButtons::widget([
        'order' => [['export', 'export-all', 'approve', 'create', 'delete', 'return']],
        'gridId' => 'sale-grid',
        'searchModel' => $searchModel,
        'modelClass' => 'modules\sales\common\models\Sale',
        'buttons' => [
            'export-all' => function () {
                return Html::a(Icons::p('file-excel-o') . 'Быстрый экспорт всех', ['export-all'], [
                    'class' => 'btn btn-default',
                    'title' => 'Быстрый экспорт всех',
                ]);
            },
            'approve' => function() {
                   return ButtonDropdown::widget([
                    'tagName' => 'span',
                    'label' => 'Изменить статус у выбранных',
                    'encodeLabel' => false,
                    'split' => false,
                    'dropdown' => [
                        'encodeLabels' => false,
                        'items' => [
                            [
                                'label' => 'Одобрить выбранные',
                                'url' => ['change-status-selected', 'status' => Statuses::APPROVED],
                                'linkOptions' => [
                                    'data' => [
                                        'grid' => 'sale-grid',
                                        'grid-bind' => 'selection',
                                        'grid-param' => 'ids',
                                        'confirm' => 'Вы уверены, что хотите одобрить выбранные записи?',
                                    ],
                                ]
                            ],
                            [
                                'label' => 'На согласование',
                                'url' => ['change-status-selected', 'status' => Statuses::ADMIN_REVIEW],
                                'linkOptions' => [
                                    'data' => [
                                        'grid' => 'sale-grid',
                                        'grid-bind' => 'selection',
                                        'grid-param' => 'ids',
                                        'confirm' => 'Вы уверены, что хотите согласовать выбранные записи?',
                                    ],
                                ]
                            ],
                            [
                                'label' => 'Отправить на доработку',
                                'url' => ['change-status-selected', 'status' => Statuses::DRAFT],
                                'linkOptions' => [
                                    'data' => [
                                        'grid' => 'sale-grid',
                                        'grid-bind' => 'selection',
                                        'grid-param' => 'ids',
                                        'confirm' => 'Вы уверены, что хотите отправить выбранные записи на доработку?',
                                    ],
                                ]
                            ],
                            [
                                'label' => 'Отклонить продажу',
                                'url' => ['change-status-selected', 'status' => Statuses::DECLINED],
                                'linkOptions' => [
                                    'data' => [
                                        'grid' => 'sale-grid',
                                        'grid-bind' => 'selection',
                                        'grid-param' => 'ids',
                                        'confirm' => 'Вы уверены, что хотите отклонить выбранные записи?',
                                    ],
                                ]
                            ],
                        ],
                    ],
                    'options' => [
                        'class' => 'btn btn-default',
                    ],
                ]);
            }
        ]
    ]) ?>
</div>

<h3>Суммарно: <?= $bonuses ?></h3>

<?= GridView::widget([
    'id' => 'sale-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => array_merge([
        ['class' => 'yii\grid\CheckboxColumn'],
    ], [
        [
            'class' => 'yz\admin\widgets\ActionColumn',
            'template' => '{view} {delete}',
            'buttons' => [
                'delete' => function ($url, Sale $model, $key) {
                    if ($model->statusManager->canBeDeleted() == false) {
                        return '';
                    }

                    return Html::a(Icons::i('trash-o fa-lg'), $url, [
                        'title' => Yii::t('admin/t', 'Delete'),
                        'data-confirm' => Yii::t('admin/t', 'Are you sure to delete this item?'),
                        'data-method' => 'post',
                        'class' => 'btn btn-danger btn-sm',
                        'data-pjax' => '0',
                    ]);
                }
            ]
        ],
    ], $columns),
]); ?>
<?php Box::end() ?>

