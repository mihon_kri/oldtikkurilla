<?php

use yii\helpers\Html;
use yii\helpers\Url;
use modules\sales\common\models\Sale;

/** @var Sale $sale */

?>

<?php if ($sale->positions): ?>

    <?php foreach ($sale->positions as $position): ?>

        <?php if ($product = $position->product): ?>

        <div class="sale-details">
            <div><b><?= $position->product->name ?></b></div>
            <div>
                <?php if ($group = $product->group): ?>
                    <span><?= $group->name ?>,</span>
                <?php endif; ?>

                <?php if ($category = $product->category): ?>
                    <span><?= $category->name ?></span>
                <?php endif; ?>

                <?= $product->bonuses_formula ?>
                <?php if ($product->weight || $product->unit): ?>
                    , <?= $product->weight ?>
                    <?= $product->unit ? $product->unit->short_name : '' ?>
                <?php endif; ?>

                <?php if ($position->cost): ?>
                    , стоимость = <?= $position->cost_real ?>
                <?php endif; ?>

                <span class="sale-details__bonuses">
                    <?= $position->quantity ?>x = <?= $position->bonuses ?> р.
                </span>
            </div>
        </div>

        <?php endif; ?>

    <?php endforeach ?>

<?php endif; ?>