<?php

namespace modules\sales\backend\forms;

use modules\sales\common\models\Product;

class ProductCreateForm extends Product
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['category_id', 'required'],

            ['group_id', 'required'],
        ]);
    }

}