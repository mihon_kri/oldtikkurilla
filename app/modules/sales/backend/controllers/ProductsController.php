<?php

namespace modules\sales\backend\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yz\admin\actions\ExportAction;
use yz\admin\traits\CrudTrait;
use yz\admin\actions\ToggleAction;
use yz\admin\grid\columns\ToggleColumn;
use backend\base\Controller;
use modules\sales\backend\models\ProductSearch;
use modules\sales\common\models\Category;
use modules\sales\common\models\Group;
use modules\sales\common\models\Product;
use modules\sales\common\models\Unit;
use modules\sales\backend\forms\ProductCreateForm;

/**
 * ProductsController implements the CRUD actions for Product model.
 */
class ProductsController extends Controller
{
    use CrudTrait;

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::class,
                'dataProvider' => function ($params) {
                    $searchModel = new ProductSearch;
                    $dataProvider = $searchModel->search($params);
                    return $dataProvider;
                },
            ],
            'toggle' => [
                'class' => ToggleAction::class,
                'modelClass' => Product::class,
                'attributes' => ['enabled', 'is_show_in_catalog'],
            ]
        ]);
    }

    /**
     * Lists all Product models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns(),
        ]);
    }

    public function getGridColumns()
    {
        return [
            [
                'attribute' => 'id',
            ],
            [
                'attribute' => 'group_id',
                'filter' => Group::getOptions(),
                'titles' => Group::getOptions(),
            ],
            [
                'attribute' => 'category_id',
                'filter' => Category::getOptions(),
                'titles' => Category::getOptions(),
            ],
            'name',
            'code',
            'weight_real',
            [
                'attribute' => 'unit_id',
                'filter' => Unit::getList(),
                'titles' => Unit::getList(),
            ],
            [
                'attribute' => 'enabled',
                'class' => ToggleColumn::class,
            ],
            [
                'class' => ToggleColumn::class,
                'attribute' => 'is_show_in_catalog',
            ],
        ];
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionCreate()
    {
        $model = new ProductCreateForm;

        if (null === $model->enabled) {
            $model->enabled = true;
        }

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully created'));
            return $this->getCreateUpdateResponse($model);
        }
        else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, \Yii::t('admin/t', 'Record was successfully updated'));
            return $this->getCreateUpdateResponse($model);
        }
        else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete(array $id)
    {
        $message = is_array($id) ?
            \Yii::t('admin/t', 'Records were successfully deleted') : \Yii::t('admin/t', 'Record was successfully deleted');
        $id = (array) $id;

        foreach ($id as $id_) {
            $model = $this->findModel($id_);
            try {
                $model->delete();
                \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, $message);
            }
            catch (\Exception $e) {
                $model->updateAttributes(['enabled' => false]);
                \Yii::$app->session->setFlash(\yz\Yz::FLASH_INFO, 'Запись не может быть удалена, так как этот товар фигурирует в покупках. Товар был отключен из списков.');
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductCreateForm::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
