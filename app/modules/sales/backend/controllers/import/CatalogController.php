<?php

namespace modules\sales\backend\controllers\import;

use yii\helpers\Html;
use yz\admin\import\BatchImportAction;
use yz\admin\import\ImportForm;
use yz\admin\import\InterruptImportException;
use yz\admin\import\SkipRowException;
use backend\base\Controller;
use modules\sales\common\models\Group;
use modules\sales\common\models\Category;
use modules\sales\common\models\Product;
use modules\sales\common\models\Unit;
use modules\sales\backend\forms\ProductCreateForm;

class CatalogController extends Controller
{
    const FIELD_CODE = 'Номенклатура';
    const FIELD_GROUP = 'Бренд';
    const FIELD_CATEGORY = 'Наименование продукта (Trade Name)';
    const FIELD_PRODUCT = 'Продукт';
    const FIELD_WEIGHT = 'Фасовка';

    /**
     * @var array
     */
    private $units;

    public function actions()
    {
        return [
            'index' => [
                'class' => BatchImportAction::class,
                'extraView' => '@modules/sales/backend/views/import/catalog.php',
                'importConfig' => [
                    'availableFields' => [
                        self::FIELD_CODE => 'Номенклатура Аксапта 2009',
                        self::FIELD_GROUP => 'Бренд',
                        self::FIELD_CATEGORY => 'Наименование продукта (Trade Name)',
                        self::FIELD_PRODUCT => 'Наименование SKU',
                        self::FIELD_WEIGHT => 'Фасовка (л/кг)',
                    ],
                    'rowImport' => [$this, 'rowImport'],
                    'skipFirstLine' => true,
                ]
            ]
        ];
    }

    public function init()
    {
        ini_set('memory_limit', '-1');

        $units = [];

        foreach (Unit::find()->all() as $unit) {
            /* @var Unit $unit */
            $key = str_replace(".", "", $unit->short_name);
            $units[$key] = $unit->id;
        }

        $this->units = $units;
    }

    public function rowImport(ImportForm $form, array $row)
    {
        $row = array_map('trim', $row);

        $group = $this->importBrand($row);
        $category = $this->importCategory($row);
        $product = $this->importProduct($row, $group, $category);
    }

    /**
     * @param array $row
     * @return Group
     * @throws InterruptImportException
     */
    private function importBrand($row)
    {
        $group = Group::findOne(['name' => $row[self::FIELD_GROUP]]);

        if (null === $group) {
            $group = new Group();
            $group->name = $row[self::FIELD_GROUP];

            if ($group->save() === false) {
                throw new InterruptImportException('Не удалось добавить бренд: ' . implode(', ', $group->getFirstErrors()), $row);
            }
        }

        return $group;
    }

    /**
     * @param array $row
     * @return Category
     * @throws InterruptImportException
     */
    private function importCategory($row)
    {
        $category = Category::findOne(['name' => $row[self::FIELD_CATEGORY]]);

        if (null === $category) {
            $category = new Category();
            $category->name = $row[self::FIELD_CATEGORY];

            if ($category->save() === false) {
                throw new InterruptImportException('Ошибка при импорте категории: ' . implode(', ', $category->getFirstErrors()), $row);
            }
        }

        return $category;
    }

    /**
     * @param array $row
     * @param group $group
     * @param Category $category
     * @return Product
     * @throws InterruptImportException
     * @throws SkipRowException
     */
    private function importProduct($row, Group $group, Category $category)
    {
        $product = Product::findOne([
            'category_id' => $category->id,
            'name' => $row[self::FIELD_PRODUCT]
        ]);

        if ($product) {
            throw new SkipRowException("Товар уже существует: " . $product->name);
        }

        $weight = str_replace(',', '.', $row[self::FIELD_WEIGHT]);
        $weight = trim(preg_replace('/[^0-9.]/', '', $weight));
        $unit = trim(preg_replace('/[0-9,]/', '', $row[self::FIELD_WEIGHT]));

        if (isset($this->units[$unit]) === false) {
            throw new InterruptImportException("Единица изменения не найдена: " . Html::encode($unit), $row);
        }

        if (is_numeric($weight) === false) {
            throw new InterruptImportException("Значение не является числом: " . Html::encode($weight), $row);
        }

        $product = new ProductCreateForm();
        $product->code = $row[self::FIELD_CODE];
        $product->name = $row[self::FIELD_PRODUCT];
        $product->group_id = $group->id;
        $product->category_id = $category->id;
        $product->unit_id = $this->units[$unit];
        $product->weight_real = (float) $weight;

        if ($product->save() === false) {
            throw new InterruptImportException('Ошибка при импорте товара: ' . implode(', ', $product->getFirstErrors()), $row);
        }

        return $product;
    }
}