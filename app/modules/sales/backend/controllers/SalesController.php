<?php

namespace modules\sales\backend\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\helpers\Html;
use yz\admin\actions\ExportAction;
use yz\admin\grid\columns\DataColumn;
use yz\admin\grid\filters\DateRangeFilter;
use yz\Yz;
use backend\base\Controller;
use modules\profiles\common\models\Region;
use modules\profiles\common\models\City;
use modules\sales\backend\models\SaleSearch;
use modules\sales\common\actions\DownloadDocumentAction;
use modules\sales\common\actions\UploadImagesAction;
use modules\sales\common\models\Sale;
use modules\sales\common\sales\statuses\Statuses;
use modules\actions\common\models\Action;
use modules\profiles\common\models\Leader;

/**
 * SalesController implements the CRUD actions for Sale model.
 */
class SalesController extends Controller
{
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::class,
                'dataProvider' => function ($params) {
                    $searchModel = Yii::createObject(SaleSearch::class);
                    $dataProvider = $searchModel->search($params);

                    return $dataProvider;
                },
            ],
            'download-document' => [
                'class' => DownloadDocumentAction::class,
            ],
            'download-small-document' => [
                'class' => DownloadDocumentAction::class,
                'format' => DownloadDocumentAction::TYPE_SMALL,
            ],
            'upload-images' => UploadImagesAction::class,
        ]);
    }

    /**
     * Lists all Sale models.
     *
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex()
    {
        $searchModel = Yii::createObject(SaleSearch::class);
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        $bonuses = $dataProvider->query->sum('{{%sales}}.bonuses');

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns(),
            'bonuses' => $bonuses,
        ]);
    }

    public function actionExportAll()
    {
        $data = (new \DateTime('now'))->format('d.m.Y');
        Yii::$app->response->sendFile(Yii::getAlias('@data/sales.xlsx'), "sales_$data.xlsx", ['inline' => false])->send();
    }

    public function getGridColumns()
    {
        $isExport = \Yii::$app->request instanceof \yii\console\Request
            || Yii::$app->request->getPathInfo() == 'sales/sales/export';

        if ($isExport) {
            set_time_limit(600);
            ini_set('memory_limit', '-1');
        }

        $leader = Leader::getLeaderByIdentity();

        $regions = Region::getOptions($leader);
        $cities = City::getTitleOptions($leader);

        return [
            [
                'attribute' => 'id',
            ],
            [
                'attribute' => 'status',
                'titles' => Sale::getStatusValues(),
                'filter' => Sale::getStatusValues(),
                'labels' => [
                    Statuses::DRAFT => DataColumn::LABEL_DEFAULT,
                    Statuses::ADMIN_REVIEW => DataColumn::LABEL_INFO,
                    Statuses::APPROVED => DataColumn::LABEL_SUCCESS,
                    Statuses::PAID => DataColumn::LABEL_SUCCESS,
                    Statuses::DECLINED => DataColumn::LABEL_DANGER,
                ],
            ],
            [
                'attribute' => 'full_name',
                'format' => 'raw',
                'value' => function (SaleSearch $model) {
                    $profile = $model->profile;
                    return $profile
                        ? Html::a($profile->full_name, ['/profiles/profiles/index', 'ProfileSearch[full_name]' => $profile->full_name])
                        : null;
                }
            ],
            [
                'attribute' => 'phone_mobile',
                'value' => function (SaleSearch $model) {
                    return $model->profile->phone_mobile ?? null;
                }
            ],
            [
                'attribute' => 'region_id',
                'filter' => $regions,
                'value' => function (SaleSearch $model) {
                    return $model->profile->city->region->title ?? null;
                }
            ],
            [
                'attribute' => 'city_id',
                'filter' => $cities,
                'value' => function (SaleSearch $model) {
                    return $model->profile->city->title ?? null;
                }
            ],
            [
                'attribute' => 'action_id',
                'filter' => Action::getList(),
                'format' => 'raw',
                'value' => function (SaleSearch $model) {
                    $action = $model->action;
                    return $action
                        ? Html::a($action->title, ['/actions/action/index', 'ActionSearch[id]' => $action->id], [
                            'title' => Html::encode($action->short_description)
                        ])
                        : null;
                }
            ],
            'bonuses',
            [
                'attribute' => 'sale_action_bonuses',
                'value' => function (SaleSearch $model) {
                    return $model->saleAction->bonuses ?? 0;
                }
            ],
            [
                'label' => 'Состав продажи',
                'contentOptions' => ['style' => 'width:400px; font-size:11px;'],
                'format' => 'raw',
                'value' => function (SaleSearch $model) use ($isExport) {
                    return $isExport
                        ? $this->renderPartial('@modules/sales/backend/views/sales/_positions_export', ['sale' => $model])
                        : $this->renderPartial('@modules/sales/backend/views/sales/_positions', ['sale' => $model]);
                }
            ],
            [
                'attribute' => 'sold_on',
                'filter' => DateRangeFilter::instance(),
                'value' => function ($model) {
                    return (new \DateTime($model->sold_on))->format('d.m.Y');
                }
            ],
            [
                'attribute' => 'created_at',
                'filter' => DateRangeFilter::instance(),
                'value' => function (Sale $model) {
                    return (new \DateTime($model->created_at))->format('d.m.Y H:i');
                }
            ],
        ];
    }

    /**
     * Updates an existing Sale model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionApp($id)
    {
        $model = $this->findModel($id);

        if ($model->statusManager->adminCanEdit() == false) {
            return $this->redirect(['view', 'id' => $id]);
        }

        return $this->render('app', compact('model'));
    }

    public function actionEdit($id)
    {
        $model = $this->findModel($id);

        if ($model->statusManager->adminCanEdit() == false) {
            return $this->redirect(['view', 'id' => $id]);
        }

        return $this->render('edit', compact('model'));
    }

//    public function actionEditSale($id)
//    {
//        Yii::$app->response->format = Response::FORMAT_JSON;
//        Yii::$app->response->statusCode = 200;
//        $data = Yii::$app->request->post();
//
//        $model = new SaleEditForm();
//        $model->sale_id = $id;
//        $model->frontend = false;
//
//        if ($model->load(['SaleEditForm' => $data]) && $model->process()) {
//            Yii::$app->session->setFlash('success', 'Покупка успешно обновлена!');
//            return ['result' => 'OK', 'redirect' => Url::to(['/sales/sales/view', 'id' => $model->sale_id])];
//        }
//        else {
//            Yii::$app->response->statusCode = 400;
//            return ['result' => 'FAIL', 'errors' => array_values($model->getFirstErrors())];
//        }
//    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash(\yz\Yz::FLASH_SUCCESS, 'Сохранено!');
        }

        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionDelete(array $id)
    {
        $status = Yz::FLASH_SUCCESS;

        $message = count($id) > 1
            ? Yii::t('admin/t', 'Records were successfully deleted')
            : Yii::t('admin/t', 'Record was successfully deleted');

        foreach ($id as $_id) {
            $model = $this->findModel($_id);

            if ($model->statusManager->canBeDeleted()) {
                $this->findModel($_id)->delete();
            } else {
                $status = Yz::FLASH_WARNING;
                $message = 'Некоторые (или все) из выбранных продаж невозможно удалить, т.к. они уже подтверждены или начислены.';
            }
        }

        Yii::$app->session->setFlash($status, $message);

        return $this->redirect(['index']);
    }

    public function actionChangeStatus(int $id, string $status, string $comment = '')
    {
        $model = $this->findModel($id);

        if ($model->statusManager->adminCanSetStatus($status)) {
            $model->statusManager->changeStatus($status, $comment);
            $model->statusManager->addNitifications($status, $id, $model->recipient_id, $comment);
        } else {
            Yii::$app->session->setFlash(Yz::FLASH_ERROR, 'Статус не может быть изменен');
        }

        return $this->redirect(Yii::$app->request->referrer ?: Url::home());
    }

    public function actionChangeStatusSelected(array $ids, string $status)
    {
        $transaction = Sale::getDb()->beginTransaction();

        foreach ($ids as $id) {
            $model = $this->findModel($id);
            if ($model->statusManager->adminCanSetStatus($status)) {
                $model->statusManager->changeStatus($status);
                $model->statusManager->addNitifications($status, $id, $model->recipient_id);
            } else {
                Yii::$app->session->setFlash(Yz::FLASH_ERROR, 'У ряда закупок статус не может быть изменен. Все изменения отменены');
                $transaction->rollBack();

                return $this->redirect(Yii::$app->request->referrer ?: Url::home());
            }
        }

        $transaction->commit();

        Yii::$app->session->setFlash(Yz::FLASH_SUCCESS, 'Статус успешно изменен');

        return $this->redirect(Yii::$app->request->referrer ?: Url::home());
    }

    /**
     * Finds the Sale model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     * @return Sale the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sale::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionFileCatalog()
    {
        $path = Yii::getAlias('@modules/sales/common/files/catalog.xlsx');

        if (file_exists($path)) {
            return Yii::$app->response->sendFile($path);
        }

        throw new NotFoundHttpException();
    }

    public function actionFileSales()
    {
        $path = Yii::getAlias('@modules/sales/common/files/sales.xlsx');

        if (file_exists($path)) {
            return Yii::$app->response->sendFile($path);
        }

        throw new NotFoundHttpException();
    }

}
