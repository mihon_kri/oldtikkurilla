<?php

namespace modules\sales\backend;

use yz\icons\Icons;

class Module extends \modules\sales\common\Module
{
    public function getName()
    {
        return 'Бонусы за покупку';
    }

    public function getAdminMenu()
    {
        return [
            [
                'label' => 'Продукция и продажи',
                'icon' => Icons::o('shopping-bag'),
                'items' => [
                    [
                        'route' => ['/sales/sales/index'],
                        'label' => 'Список продаж',
                        'icon' => Icons::o('shopping-bag'),
                    ],
                    [
                        'route' => ['/sales/groups/index'],
                        'label' => 'Бренды',
                        'icon' => Icons::o('bank'),
                    ],
                    [
                        'route' => ['/sales/categories/index'],
                        'label' => 'Наименование продукта (Trade Name)',
                        'icon' => Icons::o('cube'),
                    ],
                    [
                        'route' => ['/sales/products/index'],
                        'label' => 'Продукция',
                        'icon' => Icons::o('cubes'),
                    ],
                    [
                        'route' => ['/sales/units/index'],
                        'label' => 'Единицы измерения',
                        'icon' => Icons::o('superscript'),
                    ],
                    [
                        'route' => ['/sales/import/catalog/index'],
                        'label' => 'Импорт каталога',
                        'icon' => Icons::o('upload'),
                    ],
                ]
            ],
        ];
    }

}