<?php

namespace modules\sales\backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use modules\sales\common\models\Product;

/**
 * ProductSearch represents the model behind the search form about `modules\sales\common\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'group_id', 'price', 'weight', 'unit_id'], 'integer'],
            [['weight_real'], 'double'],
            [['name','bonuses_formula', 'name_html', 'code', 'title', 'description', 'url', 'enabled'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $query->joinWith([
            'unit',
            'category'
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'name' => SORT_ASC
                ]
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if ($this->weight_real) {
            $this->weight = $this->weight_real * self::MULTIPLIER;
            $query->andFilterWhere(['{{%sales_products}}.weight' => $this->weight]);
        }

        $query->andFilterWhere([
            '{{%sales_products}}.id' => $this->id,
            '{{%sales_products}}.category_id' => $this->category_id,
            '{{%sales_products}}.group_id' => $this->group_id,
            '{{%sales_products}}.unit_id' => $this->unit_id,
            '{{%sales_products}}.enabled' => $this->enabled,
        ]);

        $query->andFilterWhere(['LIKE', '{{%sales_products}}.name', $this->name]);
        $query->andFilterWhere(['LIKE', '{{%sales_products}}.code', $this->code]);

        return $dataProvider;
    }
}
