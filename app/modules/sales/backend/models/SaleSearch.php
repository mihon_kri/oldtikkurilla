<?php

namespace modules\sales\backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yz\admin\behaviors\DateRangeFilteringBehavior;
use yz\admin\search\SearchModelEvent;
use yz\admin\search\SearchModelInterface;
use modules\sales\common\models\Sale;
use modules\profiles\common\models\Leader;

/**
 * SaleSearch represents the model behind the search form about `modules\sales\common\models\Sale`.
 */
class SaleSearch extends Sale implements SearchModelInterface
{
    /**
     * @var string
     */
    public $full_name;

    /**
     * @var string
     */
    public $phone_mobile;

    /**
     * @var integer
     */
    public $region_id;

    /**
     * @var integer
     */
    public $city_id;

    /**
     * @var integer
     */
    public $sale_action_bonuses;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['recipient_id', 'bonuses', 'id', 'total_cost', 'region_id', 'city_id', 'action_id', 'sale_action_bonuses'], 'integer'],
            [['status', 'created_at', 'updated_at', 'sold_on', 'approved_by_admin_at', 'bonuses_paid_at',
                'full_name', 'phone_mobile'], 'safe'],
            [['created_at_range', 'updated_at_range', 'sold_on_range', 'number', 'place'], 'safe'],
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'full_name' => 'ФИО',
            'phone_mobile' => 'Телефон',
            'region_id' => 'Регион',
            'city_id' => 'Город',
            'sale_action_bonuses' => 'Бонусы за акцию',
        ]);
    }

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            [
                'class' => DateRangeFilteringBehavior::class,
                'attributes' => [
                    'created_at' => 'sale.created_at',
                    'updated_at' => 'sale.updated_at',
                    'sold_on' => 'sale.sold_on',
                ]
            ]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->prepareQuery();
        $this->trigger(self::EVENT_AFTER_PREPARE_QUERY, new SearchModelEvent([
            'query' => $query,
        ]));

        $dataProvider = $this->prepareDataProvider($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_DATA_PROVIDER, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        $this->load($params);

        $this->prepareFilters($query);
        $this->trigger(self::EVENT_AFTER_PREPARE_FILTERS, new SearchModelEvent([
            'query' => $query,
            'dataProvider' => $dataProvider,
        ]));

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    protected function prepareQuery()
    {
        $query = self::find()
            ->joinWith([
                'action',
                'saleAction',
                'positions',
                'profile' => function (ActiveQuery $query) {
                    $query->joinWith([
                        'city' => function (ActiveQuery $query) {
                            $query->joinWith([
                                'region'
                            ]);
                        }
                    ]);
                }
            ]);

        $leader = Leader::getLeaderByIdentity();

        if ($leader) {
            if ($leader->roleManager->isAdminRegional()) {
                $query->andWhere(['{{%cities}}.region_id' => $leader->region_id]);
            }

            if ($leader->roleManager->isAdminOp()) {
                $query->andWhere(['IN', '{{%profiles}}.id', $leader->getProfiles()->column()]);
            }
        }


        return $query;
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider($query)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
            ]
        ]);
        return $dataProvider;
    }

    /**
     * @param ActiveQuery $query
     */
    protected function prepareFilters($query)
    {
        $query->andFilterWhere([
            '{{%sales}}.id' => $this->id,
            '{{%sales}}.recipient_id' => $this->recipient_id,
            '{{%sales}}.bonuses' => $this->bonuses,
            '{{%sales}}.total_cost' => $this->total_cost,
            '{{%sales}}.number' => $this->number,
            '{{%sales}}.action_id' => $this->action_id,
            '{{%cities}}.id' => $this->city_id,
            '{{%regions}}.id' => $this->region_id,
            '{{%sales_actions}}.bonuses' => $this->sale_action_bonuses,

        ]);

        $query->andFilterWhere(['LIKE', '{{%sales}}.status', $this->status]);
        $query->andFilterWhere(['LIKE', '{{%profiles}}.full_name', $this->full_name]);
        $query->andFilterWhere(['LIKE', '{{%profiles}}.phone_mobile', $this->phone_mobile]);

//        $query->andFilterWhere(['LIKE', '{{%sales}}.sale_products', empty($this->sale_products) ? null : ':' . $this->sale_products . ':']);
//        $query->andFilterWhere(['LIKE', '{{%sales}}.sale_groups', empty($this->sale_groups) ? null : ':' . $this->sale_groups . ':']);
//        $query->andFilterWhere(['LIKE', '{{%sales}}.sale_qty', empty($this->sale_qty) ? null : ':' . $this->sale_qty . ':']);

    }
}
