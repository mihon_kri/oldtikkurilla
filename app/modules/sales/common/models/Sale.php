<?php

namespace modules\sales\common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\interfaces\ModelInfoInterface;
use marketingsolutions\datetime\DateTimeBehavior;
use ms\files\attachments\common\traits\FileAttachmentsTrait;
use modules\profiles\common\models\Profile;
use modules\sales\common\sales\statuses\Statuses;
use modules\sales\common\sales\statuses\StatusManager;
use modules\actions\common\models\Action;
use modules\sales\common\managers\ActionManager;

/**
 * This is the model class for table "yz_sales".
 *
 * @property integer $id
 * @property integer $action_id
 * @property string $status
 * @property integer $recipient_id
 * @property integer $bonuses
 * @property integer $total_cost
 * @property string $created_at
 * @property string $updated_at
 * @property string $sold_on
 * @property string $bonuses_paid_at
 * @property string $approved_by_admin_at
 * @property string $review_comment
 * @property string $number
 * @property string $sale_products
 * @property string $sale_groups
 * @property string $sale_qty
 * @property string $place
 *
 * @property SalePosition[] $positions
 * @property StatusManager $statusManager
 * @property ActionManager $actionManager
 * @property SaleDocument[] $documents
 * @property SaleHistory[] $history
 * @property string $status_label
 * @property string $sold_on_local
 * @property Profile $profile
 * @property Action $action
 * @property Action[] $actions
 */
class Sale extends \yz\db\ActiveRecord implements ModelInfoInterface
{
    use FileAttachmentsTrait;

    const DATA_DIR = 'sales';

    /**
     * @var StatusManager
     */
    private $_statusManager;

    /**
     * @var ActionManager
     */
    private $_actionManager;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sales}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Покупки';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Покупки продукции';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
            'datetime' => [
                'class' => DateTimeBehavior::class,
                'attributes' => [
                    'sold_on' => [
                        'targetAttribute' => 'sold_on_local',
                        'originalFormat' => ['date', 'yyyy-MM-dd'],
                        'targetFormat' => ['date', 'dd.MM.yyyy'],
                    ]
                ]
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['action_id', 'required'],
            ['action_id', 'integer'],

            ['sold_on_local', 'required'],

            ['status', 'string'],

            ['recipient_id', 'integer'],

            ['bonuses', 'integer'],

            ['total_cost', 'integer'],

            ['sale_products', 'string'],

            ['sale_groups', 'string'],

            ['number', 'string'],

            ['sale_qty', 'string'],

            ['place', 'string'],

            [['created_at', 'updated_at', 'sold_on', 'bonuses_paid_at', 'approved_by_admin_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'action_id' => 'Акция',
            'status' => 'Статус',
            'recipient_id' => 'Получатель приза',
            'total_cost' => 'Стоимость материалов',
            'bonuses' => 'Сумма продажи',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
            'sold_on' => 'Дата покупки',
            'bonuses_paid_at' => 'Дата начисления бонусов',
            'approved_by_admin_at' => 'Дата одобрения администратором',
            'review_comment' => 'Комментарий',
            'number' => 'Номер',
            'place' => 'Место покупки',
            'sale_products' => 'Продукция',
            'sale_groups' => 'Бренды',
            'sale_qty' => 'Количество',
        ];
    }

    public function fields()
    {
        return [
            'id',
            'action_id',
            'status',
            'status_label',
            'bonuses',
            'sold_on_local',
            'created_at' => function (Sale $model) {
                return $model->created_at ? (new \DateTime($model->created_at))->format('d.m.Y') : null;
            },
            'number',
            'documents' => function (Sale $model) {
                return $model->getAttachedFiles()->orderBy('id')->all();
            },
            'positions',
            'history',
            'action',
            'saleAction',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHistory()
    {
        return $this->hasMany(SaleHistory::class, ['sale_id' => 'id'])->orderBy(['id' => SORT_DESC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositions()
    {
        return $this->hasMany(SalePosition::class, ['sale_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(Action::class, ['id' => 'action_id']);
    }

    public function getSaleAction()
    {
        return $this->hasOne(SaleAction::class, ['sale_id' => 'id']);
    }

    public static function getStatusValues()
    {
        return Statuses::statusesValues();
    }

    public function getStatusText()
    {
        $statuses = self::getStatusValues();

        return empty($statuses[$this->status]) ? null : $statuses[$this->status];
    }

    public static function getStatusLabel($status)
    {
        $statuses = self::getStatusValues();

        return empty($statuses[$status]) ? null : $statuses[$status];
    }

    public function getStatus_label()
    {
        $statuses = self::getStatusValues();

        return empty($statuses[$this->status]) ? null : $statuses[$this->status];
    }

    public function getProducts()
    {
        return $this->hasOne(Product::class, ['id' => 'product_id'])->via('positions');
    }

    /**
     * @param boolean $save
     * @return integer
     */
    public function updateBonuses($save = true)
    {
        $bonuses = 0;

        foreach ($this->getPositions()->each() as $position) {
            /* @var SalePosition $position */
            $bonuses += $position->bonuses * $position->quantity;
        }

        if ($save) {
            $this->updateAttributes(['bonuses' => $bonuses]);
        }

        return $bonuses;
    }

    public function updateTotalCost()
    {
        $this->total_cost = $this->getPositions()->sum('cost');

        return $this->updateAttributes(['total_cost']);
    }

    /**
     * @return StatusManager
     */
    public function getStatusManager()
    {
        if (null === $this->_statusManager) {
            $this->_statusManager = new StatusManager($this);
        }
        return $this->_statusManager;
    }

    /**
     * @return ActionManager
     */
    public function getActionManager()
    {
        if (null === $this->_actionManager) {
            $this->_actionManager = new ActionManager($this);
        }

        return $this->_actionManager;
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        SaleHistory::deleteAll(['sale_id' => $this->id]);
        SalePosition::deleteAll(['sale_id' => $this->id]);
        SaleAction::deleteAll(['sale_id' => $this->id]);

        return true;
    }

    public function getProfile()
    {
        return $this->getRecipient();
    }

    public function getRecipient()
    {
        return $this->hasOne(Profile::class, ['id' => 'recipient_id']);
    }

    /**
     * @return SaleHistory[]
     */
    public function getOrderedHistory()
    {
        return SaleHistory::find()
            ->where(['sale_id' => $this->id])
            ->orderBy(['created_at' => SORT_DESC])
            ->all();
    }

    /**
     * @return SalePosition[]
     */
    public function getOrderedPositions()
    {
        return SalePosition::find()
            ->where(['sale_id' => $this->id])
            ->orderBy(['id' => SORT_ASC])
            ->all();
    }

    /**
     * @return bool
     */
    public function isDraft()
    {
        return $this->status == Statuses::DRAFT;
    }

    /**
     * @return bool
     */
    public function userCanEdit()
    {
        return $this->status == Statuses::DRAFT || $this->status == Statuses::ADMIN_REVIEW;
    }

    /**
     * @param string $oldStatus
     * @param string $type
     * @param string $comment
     * @return bool
     */
    public function addHistory(string $oldStatus, string $type, string $comment = '')
    {
        $note = SaleHistory::getTypesList()[$type] ?? '';

        $history = new SaleHistory();
        $history->sale_id = $this->id;
        $history->status_old = $oldStatus;
        $history->status_new = $type;
        $history->note = $note;
        $history->comment = $comment;
        $history->admin_id = Yii::$app->user->identity->getId(); // TODO: fix for leaders
        $history->type = $type;

        return $history->save(false);
    }
}
