<?php

namespace modules\sales\common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_sales_categories".
 *
 * @property integer $id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Product[] $products
 */
class Category extends \yz\db\ActiveRecord implements ModelInfoInterface
{
    const DUMMY_PLAN_CATEGORY_NAME = "Выполнение плана";

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sales_categories}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Категория';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Наименование продукта (Trade Name)';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'string', 'max' => 255],
            ['name', 'required'],
            ['name', 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата изменения',
        ];
    }

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'id',
            'name',
        ];
    }

    public function beforeDelete()
    {
        Product::updateAll(
            ['category_id' => null],
            ['category_id' => $this->id]
        );

        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::class, ['category_id' => 'id']);
    }

    /**
     * @return array
     */
    public static function getOptions()
    {
        return Category::find()
            ->select('name, id')
            ->orderBy(['name' => SORT_ASC])
            ->indexBy('id')
            ->column();
    }
}
