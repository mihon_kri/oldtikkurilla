<?php

namespace modules\sales\common\models;

use modules\profiles\common\models\Dealer;
use modules\profiles\common\models\Profile;
use modules\sales\common\sales\bonuses\BonusesCalculator;
use modules\sales\common\sales\bonuses\FormulaValidator;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_sales_products".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $group_id
 * @property string $name
 * @property string $name_html
 * @property string $code
 * @property string $title
 * @property string $description
 * @property string $url
 * @property string $url_shop
 * @property string $photo_name
 * @property integer $price
 * @property string $weight
 * @property integer $unit_id
 * @property string $bonuses_formula
 * @property boolean $enabled
 * @property boolean $is_show_on_main
 * @property boolean $is_show_in_catalog
 *
 * @property float $weight_real
 * @property Category $category
 * @property Group $group
 * @property Unit $unit
 * @property BonusesCalculator $bonusesCalculator
 * @property string $photo_url
 */
class Product extends \yz\db\ActiveRecord implements ModelInfoInterface
{
    const MULTIPLIER = 100;

    /**
     * @var float
     */
    public $weight_real;

    public $photo;

    /**
     * @var BonusesCalculator
     */
    private $bonusesCalculator;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sales_products}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Товар';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Продукция';
    }

    public static function getUnitIdValues()
    {
        return Unit::find()->select('name, id')->indexBy('id')->column();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required'],
            ['name', 'string', 'max' => 255],

            ['name_html', 'string', 'max' => 500],

            ['code', 'filter', 'filter' => 'trim'],
            ['code', 'required'],
            ['code', 'string', 'max' => 255],
            ['code', 'unique'],

            ['title', 'string', 'max' => 500],

            ['description', 'string'],

            ['photo_name', 'string', 'max' => 255],

            ['bonuses_formula', 'string', 'max' => 255],
            ['bonuses_formula', FormulaValidator::class],

            ['url', 'url'],
            ['url', 'string', 'max' => 500],

            ['url_shop', 'url'],
            ['url_shop', 'string', 'max' => 500],

            ['category_id', 'safe'],

            ['group_id', 'integer'],

            ['unit_id', 'required'],
            ['unit_id', 'integer'],

            ['price', 'integer'],

            ['weight', 'required'],
            ['weight', 'integer'],

            ['weight_real', 'required'],
            ['weight_real', 'double'],

            ['is_show_on_main', 'boolean'],
            ['is_show_on_main', 'default', 'value' => false],

            ['is_show_in_catalog', 'boolean'],
            ['is_show_in_catalog', 'default', 'value' => true],

            ['enabled', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Наименование продукта (Trade Name)',
            'group_id' => 'Бренд',
            'name' => 'Название',
            'name_html' => 'Название HTML',
            'code' => 'Номенклатура',
            'title' => 'Описание',
            'description' => 'Описание полное',
            'url' => 'Ссылка на описание продукции',
            'url_shop' => 'Ссылка на магазин продукции',
            'weight' => 'Фасовка',
            'weight_real' => 'Фасовка',
            'price' => 'Цена',
            'photo' => 'Изображение',
            'photo_name' => 'Изображение',
            'unit_id' => 'Единица измерения',
            'bonuses_formula' => 'Формула для бонусов',
            'is_show_on_main' => 'Показать в слайдере на главной странице',
            'enabled' => 'Доступен',
            'is_show_in_catalog' => 'Доступен участникам'
        ];
    }

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'id',
            'group_id',
            'category_id',
            'unit_id',
            'name',
            'code',
            'enabled',
            'weight' => function (Product $model) {
                return $model->weight_real;
            },
            'group',
            'category',
            'unit',
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $this->weight = (int) round( ((float) $this->weight_real) * self::MULTIPLIER);

        return parent::beforeValidate();
    }

    public static function getCategoryId($category_id)
    {
        $isCategory = Category::findOne(['id' => $category_id]);
        if($isCategory){
            return $isCategory->name;
        }
        return "";
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            $isCategory = Category::findOne(['name' => $this->category_id]);
            if(!$isCategory){
                $category = new Category();
                $category->name = $this->category_id;
                $category->save();
                $this->category_id = $category->id;
            }
            else{
                $this->category_id = $isCategory->id;
            }

            return true;
        }
        return false;
    }

    public function afterFind()
    {
        parent::afterFind();

        $this->weight_real = $this->weight / self::MULTIPLIER;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::class, ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(Unit::class, ['id' => 'unit_id']);
    }

    public function getBonusesCalculator()
    {
        if ($this->bonusesCalculator === null) {
            $this->bonusesCalculator = new BonusesCalculator($this);
        }
        return $this->bonusesCalculator;
    }

    public static function getOptions()
    {
        return self::find()->indexBy('id')->select('name, id')->orderBy(['name' => SORT_ASC])->column();
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $this->upload();
    }

    public function getPhotoPath()
    {
        return Yii::getAlias('@frontendWebroot/data/products/' . $this->photo_name);
    }

    public function getPhoto_url()
    {
        return Yii::getAlias('@frontendWeb/data/products/' . $this->photo_name);
    }

    private function upload()
    {
        $dir = Yii::getAlias('@data/products');
        FileHelper::createDirectory($dir, $mode = 0775, $recursive = true);

        $file = UploadedFile::getInstance($this, 'photo');
        $unique = uniqid();

        if ($file instanceof UploadedFile) {
            $name = "{$this->id}_photo_{$unique}.{$file->extension}";
            $path = $dir . DIRECTORY_SEPARATOR . $name;
            $file->saveAs($path, false);

            if ($this->photo_name) {
                @unlink($dir . DIRECTORY_SEPARATOR . $this->photo_name);
            }

            $this->photo_name = $name;
            $this->updateAttributes(['photo_name']);
        }

        if (getenv('YII_ENV') == 'dev') {
            $frontendDir = Yii::getAlias("@frontendWebroot/data/products");
            FileHelper::copyDirectory($dir, $frontendDir);
            $backendDir = Yii::getAlias("@backendWebroot/data/products");
            FileHelper::copyDirectory($dir, $backendDir);
        }
    }
}
