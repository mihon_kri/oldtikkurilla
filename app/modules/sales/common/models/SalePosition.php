<?php

namespace modules\sales\common\models;

use Yii;
use yii\db\ActiveQuery;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_sales_positions".
 *
 * @property integer $id
 * @property integer $sale_id
 * @property integer $product_id
 * @property integer $serial_number_id
 * @property integer $custom_serial
 * @property integer $quantity
 * @property integer $bonuses
 * @property integer $bonuses_primary
 * @property integer $cost
 * @property string $validation_method
 *
 * @property string $cost_real
 * @property Sale $sale
 * @property Product $product
 * @property Unit $unit
 * @property Category category
 * @property SaleDocument[] $documents
 */
class SalePosition extends \yz\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sales_positions}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Товарная позиция';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Товарные позиции';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['product_id', 'required', 'message' => 'Не выбран товар'],
            ['product_id', 'in',
                'range' => array_keys(self::getProductIdValues()),
                'message' => 'Выбранный товар не доступен'
            ],

            ['bonuses', 'integer'],
            ['bonuses', 'required', 'message' => 'Необходимо указать стоимость'],
            ['bonuses', 'compare',
                'compareValue' => 0,
                'type' => 'number',
                'operator' => '>',
                'message' => 'Стоимость должа быть больше нуля'
            ],

            ['quantity', 'integer'],
            ['quantity', 'required', 'message' => 'Необходимо указать количество'],
            ['quantity', 'compare',
                'compareValue' => 0,
                'type' => 'number',
                'operator' => '>',
                'message' => 'Количество должо быть больше нуля'
            ],

            ['cost', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sale_id' => 'ID покупки',
            'product_id' => 'Продукция',
            'quantity' => 'Количество',
            'bonuses' => 'Бонусы',
            'bonuses_primary' => 'Бонусы за 1ед на момент оформления',
            'validation_method' => 'Способ подтверждения',
            'cost' => 'Стоимость',
        ];
    }

    public function fields()
    {
        return [
            'product_id',
            'quantity',
            'bonuses',
            'product',
        ];
    }

    public static function getProductIdValues()
    {
        return Product::find()->select('name, id')->indexBy('id')->column();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(Unit::class, ['id' => 'unit_id'])->via('product');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSale()
    {
        return $this->hasOne(Sale::className(), ['id' => 'sale_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id'])->via('product');
    }

    /**
     * @return ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(SaleDocument::class, ['sale_position_id' => 'id']);
    }

    public function getCost_real()
    {
        return empty($this->cost) ? null : str_replace(',', '.', ($this->cost / 100) . '');
    }
}
