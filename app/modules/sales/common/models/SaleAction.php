<?php

namespace modules\sales\common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\interfaces\ModelInfoInterface;
use modules\actions\common\models\Action;
use modules\sales\common\models\Sale;

/**
 * This is the model class for table "yz_sales_actions".
 *
 * @property integer $id
 * @property integer $sale_id
 * @property integer $action_id
 * @property integer $bonuses
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $paid_at
 *
 * @property Action $action
 * @property Sale $sale
 */
class SaleAction extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sales_actions}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Sale Action';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Sale Actions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['sale_id', 'integer'],

            ['action_id', 'integer'],

            ['bonuses', 'integer'],

            ['status', 'string'],

            ['created_at', 'safe'],
            ['updated_at', 'safe'],
            ['paid_at', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sale_id' => 'Продажа',
            'action_id' => 'Акция',
            'bonuses' => 'Bonuses',
            'status' => 'Статус',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'paid_at' => 'Paid At'
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        return [
            'id',
            'bonuses',
            'status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(Action::class, ['id' => 'action_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSale()
    {
        return $this->hasOne(Sale::class, ['id' => 'sale_id']);
    }
}
