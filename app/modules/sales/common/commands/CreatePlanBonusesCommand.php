<?php

namespace modules\sales\common\commands;

use modules\actions\common\models\ActionGrowthBonus;
use modules\actions\common\models\ActionProfile;
use modules\actions\common\types\PlanCompletePersonalActionType;
use Yii;
use yii\base\Component;
use yii\base\Exception;
use common\components\interfaces\CommandInterface;
use common\components\traits\CommandTrait;
use modules\profiles\common\models\Profile;
use modules\sales\common\sales\statuses\Statuses;
use modules\sales\common\models\Sale;
use modules\sales\common\models\SaleAction;
use modules\actions\common\models\Action;
use modules\actions\common\types\PlanCompleteActionType;
use modules\actions\common\calculators\BonusesFormulaCalculator;

/**
 * Creates bonuses (SaleAction's) for completed plans (Sales in Action with type PlanCompleteActionType)
 */
class CreatePlanBonusesCommand extends Component implements CommandInterface
{
    use CommandTrait;

    public function handle()
    {
        $transaction = Yii::$app->db->beginTransaction();

        try {

            $actions = $this->getRunningActions();

            foreach ($actions->each() as $action) {
                /* @var Action $action */
                if($action->plan_amount){
                    $this->consoleOutput("\nAction #{$action->id} - plan is {$action->plan_amount}\n");
                }else{
                    $this->consoleOutput("\nAction #{$action->id} - plan is individual\n");
                }


                $this->handleProfiles($action);
            }

            $transaction->commit();
            $this->consoleOutput("\nSuccess");

            return true;

        } catch (\Throwable $e) {

            $transaction->rollBack();
            $this->consoleOutput("\nError\n" . $e->getMessage());

            return false;
        }
    }

    /**
     * @param Action $action
     * @return bool
     * @throws Exception
     */
    private function handleProfiles(Action $action)
    {
        $plan = $action->plan_amount;

        $profiles = $this->getActionProfiles($action);
        $this->consoleOutput(" Profiles count: " . $profiles->count());

        foreach ($profiles->each() as $profile) {
            /* @var Profile $profile */
            $this->consoleOutput("\n  #$profile->id $profile->full_name");

            if ($this->isProfileHasBonuses($action, $profile)) {
                $this->consoleOutput("  ... SKIP: already has bonuses");
                continue;
            }

            $sum = $this->getActionSalesSumByProfile($action, $profile);

            if(!$plan){
                $plan = $this->getIndividualPlan($profile->id, $action->id);
            }

            //Если бонусная формула пуста, то прилетает массив с приростами и бонусами,
            // если непуста, то сумма вычисляется по формуле
            if(!is_array($plan)){
                if ($sum < $plan) {
                    $this->consoleOutput("  ... SKIP: $sum < $plan");
                    continue;
                }
            }else{
                $sum = $this->getPlanByGrowth($sum, $plan);
            }




            $lastSale = $this->getLastActionSaleByProfile($action, $profile);
//            if($action->id == 10) {
////                print_r($lastSale->action->id);
////                print_r('!!!!!!!!!!!!!!!!!!!');
////                print_r($lastSale->bonuses);
////                print_r('!!!!!!!!!!!!!!!!!!!');
////                print_r($this->createSaleAction($lastSale));
//                $calculator = new BonusesFormulaCalculator($lastSale->action);
//                $bonuses = $calculator->calculate($lastSale->bonuses);
//                print_r($bonuses);
//                exit;
//            }
            $saleAction = $this->createSaleAction($lastSale, $sum);
            $this->consoleOutput("  ... OK: added #$saleAction->id $saleAction->bonuses to sale #$lastSale->id");
        }

        return true;
    }

    public function getPlanByGrowth($sum, $plan)
    {
        if(!is_array($plan)){
            return 0;
        }
        foreach ($plan as $param){
            if($sum >= $param['growth_from'] && $sum <= $param['growth_to']){
                return $param['bonus'];
            }elseif($sum > $param['growth_from'] && !$param['growth_to']){
                return $param['bonus'];
            }
        }
    }

    public function getIndividualPlan($profileId, $actionId)
    {
        $actionProfile = ActionProfile::findOne(['action_id' => $actionId, 'profile_id' => $profileId]);
        if($actionProfile){
            $action = Action::findOne($actionId);
            if(!$action){
                return 0;
            }
            //Если есть формула рассчета персонального плана, то берем её
            $formula = $action->personal_plan_formula;
            //Если она пуста, то перем таблицу с приростами в виде массива
            if (!$formula){
                return ActionGrowthBonus::find()->where(['action_id' => $action->id])->asArray()->all();
            }
            if(stristr($formula, 'plan') === FALSE){
                return (int)$formula;
            }
            if(!$formula){
                $formula=1;
            }

            return $actionProfile->last_year_plan * $this->parseToFloat($formula);
        }
        return 0;
    }

    /**
     * @param $a
     * @return float
     */
    public static function parseToFloat($a, $text='plan')
    {
        $b = str_replace(',', '.', $a);
        $b = str_replace($text, '', $b);
        $b = str_replace('*', '', $b);
        $b = str_replace(' ', '', $b);
        $b = trim($b);
        return (double)$b;
    }

    /**
     * Returns running actions with the type PlanCompleteActionType
     * @return \modules\actions\common\models\ActionQuery
     * @throws \yii\base\InvalidConfigException
     */
    private function getRunningActions()
    {
        $query = Action::find()
            ->innerJoinWith(['type'])
            ->runningNow()
            ->andWhere(['or',
                ['{{%actions_types}}.className' => PlanCompleteActionType::class],
                ['{{%actions_types}}.className' => PlanCompletePersonalActionType::class]
            ]);

        return $query;
    }

    /**
     * @param Action $action
     * @return \yii\db\ActiveQuery
     */
    private function getActionProfiles(Action $action)
    {
        $query = Profile::find()
            ->joinWith(['sales'], false)
            ->where([
                'AND',
                ['{{%sales}}.action_id' => $action->id],
                ['IN', '{{%sales}}.status', [Statuses::APPROVED, Statuses::PAID]]
            ])
            ->orderBy(['{{%profiles}}.full_name' => SORT_ASC])
            ->groupBy('{{%profiles}}.id');

        return $query;
    }


    /**
     * @param Action $action
     * @param Profile $profile
     * @return bool
     */
    private function isProfileHasBonuses(Action $action, Profile $profile)
    {
        $query = SaleAction::find()
            ->innerJoinWith(['sale'], false)
            ->where([
                'AND',
                ['{{%sales}}.recipient_id' => $profile->id],
                ['{{%sales_actions}}.action_id' => $action->id],
                ['IN', '{{%sales}}.status', [Statuses::APPROVED, Statuses::PAID]],
            ]);

        return $query->exists();
    }

    /**
     * @param Action $action
     * @param Profile $profile
     * @return int
     */
    private function getActionSalesSumByProfile(Action $action, Profile $profile)
    {
        $query = Sale::find()
            ->where([
                'AND',
                ['recipient_id' => $profile->id],
                ['action_id' => $action->id],
                ['IN', 'status', [Statuses::APPROVED, Statuses::PAID]]
            ]);
        if($action->pay_type == Action::PAY_TYPE_COMPLETED && $action->status != Action::STATUS_FINISHED){
            return 0;
        }

        return (int) $query->sum('bonuses');
    }

    /**
     * @param Action $action
     * @param Profile $profile
     * @return array|Sale|null
     * @throws Exception
     */
    private function getLastActionSaleByProfile(Action $action, Profile $profile)
    {
        $sale = Sale::find()
            ->where([
                'AND',
                ['recipient_id' => $profile->id],
                ['action_id' => $action->id],
                ['IN', 'status', [Statuses::APPROVED, Statuses::PAID]]
            ])
            ->orderBy(['created_at' => SORT_DESC])
            ->limit(1)
            ->one();

        if (null === $sale) {
            throw new Exception("Can't find last sale");
        }

        return $sale;
    }

    /**
     * @param Sale $sale
     * @return SaleAction
     * @throws Exception
     */
    public function createSaleAction(Sale $sale, $sum)
    {
        /*Старый калькулятор*/
        //$calculator = new BonusesFormulaCalculator($sale->action);
        //$bonuses = $calculator->calculate($sale->bonuses);

        /*Новый калькулятор*/
        //Все типы идут через эту бонусную формулу


        if(stristr($sale->action->bonuses_formula, 'bonuses') === FALSE){
            $bonuses = (int) $sale->action->bonuses_formula;
        }
        else {

            $bonuses = $this->parseToFloat($sale->action->bonuses_formula, 'bonuses') * $sum;
        }
        print_r($this->parseToFloat($sale->action->bonuses_formula, 'bonuses'));
        print_r('!!!!!!!!');
        print_r($sum);

        //Если есть ограничение по баллам, то используем его вместо суммы бонусов
        if($sale->action->limit_bonuses && $bonuses > $sale->action->limit_bonuses){
            $bonuses = $sale->action->limit_bonuses;
        }

        $model = new SaleAction;
        $model->sale_id = $sale->id;
        $model->action_id = $sale->action->id;
        $model->bonuses = $bonuses;

        if (false === $model->save(false)) {
            throw new Exception("Can't create bonuses: " . implode(', ', $model->getFirstErrors()));
        }

        return $model;
    }


}