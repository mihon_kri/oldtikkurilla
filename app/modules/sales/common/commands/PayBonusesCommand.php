<?php

namespace modules\sales\common\commands;

use modules\sales\common\models\SaleAction;
use Yii;
use yii\base\Component;
use yii\base\Exception;
use yii\db\Expression;
use marketingsolutions\finance\models\Transaction;
use common\components\interfaces\CommandInterface;
use common\components\traits\CommandTrait;
use modules\sales\common\models\Sale;
use modules\sales\common\sales\statuses\Statuses;
use modules\sales\common\finances\SalePartner;
use modules\actions\common\models\Action;

/**
 * Pay bonuses for finished actions
 */
class PayBonusesCommand extends Component implements CommandInterface
{
    use CommandTrait;

    public function handle()
    {
        $transaction = Yii::$app->db->beginTransaction();

        $sales = $this->getSales();

        $this->consoleOutput("Sales count: " . $sales->count());

        try {

            foreach ($sales->each() as $sale) {
                /* @var Sale $sale */
                $this->payBonuses($sale);
            }

            $transaction->commit();
            $this->consoleOutput("Success");

            return true;

        } catch (\Throwable $e) {

            $transaction->rollBack();
            $this->consoleOutput("Error\n" . $e->getMessage());

            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    private function getSales()
    {
        $query = Sale::find()
            ->joinWith(['saleAction'])
            ->where([
                'AND',
                ['{{%sales}}.status' => Statuses::APPROVED],
                ['IS NOT', '{{%sales_actions}}.id', new Expression('NULL')]
            ]);

        return $query;
    }

    /**
     * @param Sale $sale
     * @return bool
     * @throws Exception
     */
    private function payBonuses(Sale $sale)
    {
        if (Statuses::PAID === $sale->saleAction->status) {
            return false;
        }

        if (Action::PAY_TYPE_COMPLETED === $sale->action->pay_type) {
            return $this->payCompleteActionBonuses($sale);
        }

        if (Action::PAY_TYPE_THRESHOLD === $sale->action->pay_type) {
            return $this->payThresholdActionBonuses($sale);
        }

        return false;
    }

    /**
     * @param Sale $sale
     * @return bool
     * @throws Exception
     */
    private function payCompleteActionBonuses(Sale $sale)
    {
        if ($sale->action->statusManager->isFinished()) {
            return $this->createTransaction($sale);
        }

        $this->consoleOutput("Sale #$sale->id - Action #{$sale->action->id} {$sale->action->title} - NOT finished");

        return false;
    }

    /**
     * @param Sale $sale
     * @return bool
     * @throws Exception
     */
    private function payThresholdActionBonuses(Sale $sale)
    {
        $actionBonusesSum = SaleAction::find()
            ->joinWith(['sale'])
            ->where([
                'AND',
                ['{{%sales_actions}}.action_id' => $sale->action->id],
                ['IN', '{{%sales}}.status', [Statuses::APPROVED, Statuses::PAID]]
            ])
            ->sum('{{%sales_actions}}.bonuses');

        if ($actionBonusesSum >= $sale->action->pay_threshold) {
            return $this->createTransaction($sale);
        }

        $this->consoleOutput("Sale #$sale->id - Action #{$sale->action->id} {$sale->action->title} - NOT reach threshold ({$sale->action->pay_threshold} > $actionBonusesSum)");

        return false;
    }

    /**
     * @param Sale $sale
     * @return bool
     * @throws Exception
     */
    private function createTransaction(Sale $sale)
    {
        $result = $sale->profile->purse->addTransaction(Transaction::factory(
            Transaction::INCOMING,
            $sale->saleAction->bonuses,
            new SalePartner(['id' => $sale->id]),
            "Начисление баллов за продажу #$sale->id"
        ));

        if (false == $result) {
            throw new Exception("Can't create transaction");
        }

        $result = $sale->updateAttributes([
            'status' => Statuses::PAID,
            'bonuses_paid_at' => new Expression('NOW()'),
        ]);

        if (false == $result) {
            throw new Exception("Can't update sale status");
        }

        $result = $sale->saleAction->updateAttributes([
            'status' => Statuses::PAID,
            'paid_at' => new Expression('NOW()'),
        ]);

        if (false == $result) {
            throw new Exception("Can't update saleAction status");
        }

        $this->consoleOutput("Sale #$sale->id - Action #{$sale->action->id} {$sale->action->title} - OK {$sale->saleAction->bonuses}");

        return true;
    }

}