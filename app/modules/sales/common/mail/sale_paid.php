<?php

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \yii\mail\BaseMessage $message instance of newly created mail message
 * @var \modules\sales\common\models\Sale $sale
 * @var \modules\profiles\common\models\Profile $profile
 */
$message->setSubject('Покупка одобрена, баллы начислены');
?>

<p><?= $profile->isMale() ? 'Уважаемый' : 'Уважаемая' ?> <?= $profile->full_name ?>, добрый день!<br/>
	Мы подтвердили чек № <?= $sale->id ?>, который Вы загрузили в Профи клубе Сен-Гобен.<br/>
	Вам начислено <?= $sale->bonuses ?> баллов.</p>

<?= $this->renderFile('@modules/sales/common/mail/_noreply.php'); ?>

