<?php

namespace modules\sales\common\sales\statuses;

use ms\loyalty\mobile\common\models\MobileNotification;
use Yii;
use yii\base\InvalidCallException;
use yii\base\BaseObject;
use yii\db\Expression;
use yz\Yz;
use modules\sales\common\models\Sale;
use modules\sales\common\models\SaleHistory;

/**
 * Class StatusManager
 */
class StatusManager extends BaseObject
{
    /**
     * @var Sale
     */
    private $sale;

    public function __construct(Sale $sale, $config = [])
    {
        $this->sale = $sale;
        parent::__construct($config);
    }

    public function adminCanSetStatus($status)
    {
        switch ($this->sale->status) {
            case Statuses::ADMIN_REVIEW:
                return in_array($status, [Statuses::DRAFT, Statuses::APPROVED, Statuses::DECLINED]);
            case Statuses::APPROVED:
                return in_array($status, [Statuses::DRAFT, Statuses::ADMIN_REVIEW, Statuses::DECLINED]);
            case Statuses::DECLINED:
                return in_array($status, [Statuses::DRAFT, Statuses::ADMIN_REVIEW, Statuses::APPROVED]);
            case Statuses::DRAFT:
                return in_array($status, [Statuses::ADMIN_REVIEW, Statuses::APPROVED, Statuses::DECLINED]);
        }

        return false;
    }

    public function recipientCanSetStatus($status)
    {
        switch ($this->sale->status) {
            case Statuses::DRAFT:
                return in_array($status, [Statuses::ADMIN_REVIEW]);
        }
        return false;
    }


    public function addNitifications($status, $saleId, $profileId, $comment='')
    {
        $message = "<div>Покупка&nbsp;№".$saleId."&nbsp;переведена&nbsp;в&nbsp;статус&nbsp;<b>".Statuses::statusesValues()[$status]."</b>.</div>";
        if($comment){
            $message .= "<div>Комментарий&nbsp;модератора:&nbsp;".$comment."</div>";
        }

        MobileNotification::createPush('Изменение статуса зарегистрированной покупки', $message, $profileId);

        return true;
    }
    /**
     * @param string $status
     * @param string $comment
     * @return bool
     */
    public function changeStatus($status, $comment = '')
    {
        if ($this->sale->isNewRecord) {
            throw new InvalidCallException('Can not change status of the new sale');
        }

        $oldStatus = $this->sale->status;

        switch ($status) {

            case Statuses::DRAFT:
                Yii::$app->session->setFlash(Yz::FLASH_SUCCESS, 'Статус успешно изменен, продажа возвращена на доработку участнику');

                $this->sale->updateAttributes([
                    'status' => $status,
                    'approved_by_admin_at' => null,
                    'updated_at' => new Expression('NOW()'),
                ]);

                $this->sale->addHistory($oldStatus, SaleHistory::TYPE_DRAFT, $comment);

                break;

            case Statuses::ADMIN_REVIEW:
                Yii::$app->session->setFlash(Yz::FLASH_SUCCESS, 'Статус успешно изменен, продажа на рассмотрении у администратора');

                $this->sale->updateAttributes([
                    'status' => $status,
                    'approved_by_admin_at' => null,
                    'updated_at' => new Expression('NOW()'),
                ]);

                $this->sale->addHistory($oldStatus, SaleHistory::TYPE_ADMIN_REVIEW, $comment);

                break;

            case Statuses::APPROVED:
                Yii::$app->session->setFlash(Yz::FLASH_SUCCESS, 'Продажа подтверждена, баллы за продажу подсчитаны');

                $this->sale->updateAttributes([
                    'status' => $status,
                    'approved_by_admin_at' => new Expression('NOW()'),
                    'updated_at' => new Expression('NOW()'),
                ]);

                $this->sale->addHistory($oldStatus, SaleHistory::TYPE_APPROVE, $comment);

                break;

            case Statuses::DECLINED:
                Yii::$app->session->setFlash(Yz::FLASH_SUCCESS, 'Продажа отклонена');

                $this->sale->updateAttributes([
                    'status' => $status,
                    'approved_by_admin_at' => null,
                    'updated_at' => new Expression('NOW()'),
                ]);

                $this->sale->addHistory($oldStatus, SaleHistory::TYPE_DECLINE, $comment);

                break;

            default:
                return false;

        }

        return true;
    }

    /**
     * @return bool
     */
    public function canBeDeleted()
    {
        return $this->sale->status !== Statuses::PAID;
    }

    /**
     * Is user can edit this sale
     * @return bool
     */
    public function recipientCanEdit()
    {
        return $this->sale->status == Statuses::DRAFT;
    }

    /**
     * Is administrator can edit this sale
     * @return bool
     */
    public function adminCanEdit()
    {
        return true;
        # return $this->sale->status == Statuses::ADMIN_REVIEW || $this->sale->status == Statuses::DRAFT;
    }
}