<?php

namespace modules\sales\common\managers;

use Yii;
use modules\sales\common\models\Sale;
use modules\actions\common\models\ActionType;
use modules\actions\common\types\ActionTypeInterface;

class ActionManager
{
    /**
     * @var Sale
     */
    private $_sale;

    public function __construct(Sale $sale)
    {
        $this->_sale = $sale;
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function verify()
    {
        /* @var ActionType $actionType */
        /* @var ActionTypeInterface $actionClass */

        $actionType = $this->_sale->action->type;
        $actionClass = Yii::createObject($actionType->className, [$this->_sale]);

        return $actionClass->verify();
    }

}