<?php

use yii\db\Migration;

use modules\sales\common\models\Product;

/**
 * Class m190226_074945_sales_products_remove_dummy_product
 */
class m190226_074945_sales_products_remove_dummy_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        Product::deleteAll(['code' => '999999999']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return false;
    }

}
