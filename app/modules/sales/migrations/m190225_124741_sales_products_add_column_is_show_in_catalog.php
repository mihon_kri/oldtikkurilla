<?php

use yii\db\Migration;

/**
 * Class m190225_124741_sales_products_add_column_is_show_in_catalog
 */
class m190225_124741_sales_products_add_column_is_show_in_catalog extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%sales_products}}', 'is_show_in_catalog', $this->boolean()->defaultValue(true));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%sales_products}}', 'is_show_in_catalog');
    }
}
