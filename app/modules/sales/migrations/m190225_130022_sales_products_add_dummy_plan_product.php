<?php

use yii\db\Migration;

use yii\helpers\Console;
use modules\sales\common\models\Product;
use modules\sales\common\models\Unit;

/**
 * Class m190225_130022_sales_products_add_dummy_plan_product
 */
class m190225_130022_sales_products_add_dummy_plan_product extends Migration
{
    const UNIT_NAME = 'Штуки / Упаковка';
    const DUMMY_PRODUCT_NAME = 'Выполнение плана';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $product = Product::findOne(['code' => Product::DUMMY_PLAN_PRODUCT_CODE]);

        if ($product) {
            return true;
        }

        $unit = Unit::findOne(['name' => self::UNIT_NAME]);

        if (null === $unit) {
            Console::output("Unit with name '" . self::UNIT_NAME ."' not found");

            return false;
        }

        $product = new Product();
//        $product->category_id = '';
//        $product->group_id = '';
        $product->name = self::DUMMY_PRODUCT_NAME;
        $product->code = Product::DUMMY_PLAN_PRODUCT_CODE;
        $product->enabled = true;
        $product->is_show_in_catalog = false;
        $product->unit_id = $unit->id;
        $product->weight_real = 1;

        if (false === $product->save()) {
            Console::output("Save error: " . implode(', ', $product->getFirstErrors()));

            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        Product::deleteAll(['code' => Product::DUMMY_PLAN_PRODUCT_CODE]);
    }

}
