<?php

use yii\db\Migration;

/**
 * Class m190211_112459_sales_add_column_action_id
 */
class m190211_112459_sales_add_column_action_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%sales}}', 'action_id', $this->integer()->after('id'));

        $this->addForeignKey('{{%fk-sales-actions}}',
            '{{%sales}}', 'action_id',
            '{{%actions}}', 'id',
            'RESTRICT', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%fk-sales-actions}}', '{{%sales}}');

        $this->dropColumn('{{%sales}}', 'action_id');
    }
}
