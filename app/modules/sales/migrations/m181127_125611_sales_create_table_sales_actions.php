<?php

use yii\db\Migration;

class m181127_125611_sales_create_table_sales_actions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%sales_actions}}', [
            'id' => $this->primaryKey(),
            'sale_id' => $this->integer(),
            'action_id' => $this->integer(),
            'bonuses' => $this->integer(),
            'status' => $this->string(16),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'paid_at' => $this->dateTime(),
        ], $tableOptions);

        $this->addForeignKey('{{%fk-sales_actions-sales}}',
            '{{%sales_actions}}', 'sale_id',
            '{{%sales}}', 'id',
            'RESTRICT', 'CASCADE');

        $this->addForeignKey('{{%fk-sales_actions-actions}}',
            '{{%sales_actions}}', 'action_id',
            '{{%actions}}', 'id',
            'RESTRICT', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%fk-sales_actions-sales}}', '{{%sales_actions}}');
        $this->dropForeignKey('{{%fk-sales_actions-actions}}', '{{%sales_actions}}');

        $this->dropTable('{{%sales_actions}}');
    }
}
