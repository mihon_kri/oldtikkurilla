<?php

namespace modules\actions\backend\controllers;


use modules\actions\common\models\ActionGrowthBonus;
use modules\actions\common\types\PlanCompletePersonalActionType;
use modules\profiles\backend\rbac\Rbac;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yz\admin\actions\ExportAction;
use yz\admin\contracts\AccessControlInterface;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\traits\CrudTrait;
use yz\Yz;
use modules\actions\backend\models\ActionSearch;
use modules\actions\common\models\ActionType;
use modules\actions\backend\forms\ActionForm;
use modules\actions\common\models\Action;
use modules\profiles\common\models\Leader;
use modules\profiles\common\models\Region;
use modules\profiles\common\models\City;
use modules\profiles\common\managers\RoleManager;
use modules\profiles\common\models\Profile;
use yii\helpers\Html;
use yii\helpers\Url;

class ActionController extends Controller implements AccessControlInterface
{
    use CrudTrait, CheckAccessTrait;

    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::class,
                'searchModel' => function ($params) {
                    /** @var ActionSearch $searchModel */
                    return Yii::createObject(ActionSearch::class);
                },
                'dataProvider' => function ($params, ActionSearch $searchModel) {
                    $dataProvider = $searchModel->search($params);
                    return $dataProvider;
                },
            ]
        ]);
    }

    /**
     * Lists all Action models.
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex()
    {
        /** @var ActionSearch $searchModel */
        $searchModel = Yii::createObject(ActionSearch::class);
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns($searchModel),
        ]);
    }

    public function getGridColumns(ActionSearch $searchModel)
    {
        return [
            'id',
            'code',
            'start_on:date',
            'end_on:date',
            [
                'attribute' => 'admin_id',
                'label' => 'Создал',
                'value' => function (ActionSearch $model) {
                    return $model->admin ? $model->admin->name : '';
                }
            ],
            [
                'attribute' => 'role',
                'filter' => RoleManager::getList(),
                'titles' => RoleManager::getList(),
            ],
            [
                'attribute' => 'type_id',
                'filter' => ActionType::getList(),
                'titles' => ActionType::getList(),
            ],
            'title',
            'short_description',

            'bonuses_formula',
            'bonuses_amount',
            'plan_amount',
            [
                'attribute' => 'pay_type',
                'filter' => Action::getPayTypesList(),
                'titles' => Action::getPayTypesList(),
                'value' => function (ActionSearch $model) {
                    $title = Action::getPayTypesList()[$model->pay_type];

                    if (Action::PAY_TYPE_THRESHOLD === $model->pay_type) {
                        $title .= " ($model->pay_threshold)";
                    }

                    return $title;
                }
            ],

            [
                'attribute' => 'status',
                'filter' => Action::getStatusesList(),
                'format' => 'raw',
                'value' => function (ActionSearch $model) {
                    return $this->renderPartial('_statuses', ['model' => $model]);
                }
            ],
            [
                'attribute' => 'id',
                'label' => 'Загрузка участников акции из XLS',
                'format' => 'html',
                'value' => function($model){
                    $actionType = ActionType::findOne($model->type_id);
                    if($model->status != Action::STATUS_FINISHED && $actionType && $actionType->className == PlanCompletePersonalActionType::class) {
                        return Html::a('<img src="/images/xls_icon.png">', Url::to('/actions/download-actions-user?id=' . $model->id));
                    }
                    return "";
                }
            ],
            [
                'attribute' => 'id',
                'label' => 'Загрузка бонусной формулы в зависимости от прироста',
                'format' => 'html',
                'contentOptions' => ['style' => 'min-width: 200px;'],
                'value' => function($model){
                    $actionType = ActionType::findOne($model->type_id);
                    $growthBonus = ActionGrowthBonus::getGrowthBonus($model->id);
                    if ($actionType && $actionType->className == PlanCompletePersonalActionType::class) {
                        if (!empty($growthBonus)) {
                            $growth = "<table align='center' class='growth'><tr><td align='center' class='growth'><b>Прирост</b></td><td align='center' class='growth'><b>Бонусы</b></td></tr>";
                            foreach ($growthBonus as $item) {
                                $to = $item['growth_to'] ? $item['growth_to'] : "и выше";
                                $growth .= "<tr><td align='center' class='growth'>" . $item['growth_from'] . "-" . $to . "</td><td class='growth'>" . $item['bonus'] . "</td></tr>";
                            }
                            $growth .= "</table><br/>";

                            if ($model->status != Action::STATUS_FINISHED){
                                $growth .= "<p align='center'><a target='_blank' class='label label-warning' href='/actions/action-growth-bonus/index?action_id=" . $model->id . "'>редактировать</a></p>";
                            }
                            return $growth;
                        }else{
                            return Html::a('<img src="/images/xls_icon.png">', Url::to('/actions/download-actions-growth?action_id=' . $model->id));
                        }
                    }
                    return null;
                }
            ],
            [
                'label' => 'Планы/участники (для акций с индивидуальными планами)',
                'format' => 'html',
                'value' => function($model){
                    $actionType = ActionType::findOne($model->type_id);

                    if($actionType && $actionType->className == PlanCompletePersonalActionType::class){
                        return '<a class="btn btn-success" href="/actions/action-profile/index?action='.$model->id.'">просмотр</a>';
                    }
                    return '';
                }
            ],
            'created_at:datetime',
        ];
    }

    /**
     * Creates a new Action model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param $forRole
     * @return mixed
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionCreate()
    {
        $model = new ActionForm();
        $model->has_products = true;
        $model->status = ActionForm::STATUS_NEW;

        if ($model->load(Yii::$app->request->post()) && $model->process()) {
            Yii::$app->session->setFlash(Yz::FLASH_SUCCESS, 'Акция успешно создана');

            return $this->getCreateUpdateResponse($model);
        }

        $leader = Leader::getLeaderByIdentity();

        return $this->render('create', [
            'model' => $model,
            'actionTypes' => ActionType::find()->all(),
            'regions' => Region::getOptions($leader),
            'cities' => City::getTitleOptions($leader),
            'profiles' => Profile::getOptions($leader),
        ]);
    }

    /**
     * Updates an existing Action model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws \yii\web\BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findAction($id);

        if ($model->load(Yii::$app->request->post()) && $model->process()) {
            Yii::$app->session->setFlash(Yz::FLASH_SUCCESS, 'Акция успешно изменена');

            return $this->getCreateUpdateResponse($model);
        }

        $leader = Leader::getLeaderByIdentity();

        return $this->render('update', [
            'model' => $model,
            'actionTypes' => ActionType::find()->all(),
            'regions' => Region::getOptions($leader),
            'cities' => City::getTitleOptions($leader),
            'profiles' => Profile::getOptions($leader),
        ]);
    }

    /**
     * Deletes an existing Action model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete(int $id)
    {
        $model = $this->findAction($id);

        if ($model->statusManager->canBeDeleted()) {
            $model->delete();
            Yii::$app->session->setFlash(Yz::FLASH_SUCCESS, "Акция успешно удалена");
        } else {
            Yii::$app->session->setFlash(Yz::FLASH_WARNING, "Невозможно удалить акцию, так как акция завершена или в акцию уже добавлены продажи");
        }

        return $this->redirect(['index']);
    }

    public function actionSetStatus(int $id, string $value)
    {
        $model = $this->findAction($id);
        $model->status = $value;

        if (false === isset(Action::getStatusesList()[$value])) {
            Yii::$app->session->setFlash(Yz::FLASH_WARNING, "Неверное значение статуса");

            return $this->redirect('index');
        }

        $result = false;
        if ($model->statusManager->canChangeStatus()) {
            $result = $model->updateAttributes(['status' => $value]);
        }

        $result
            ? Yii::$app->session->setFlash(Yz::FLASH_SUCCESS, "Статус акции успешно изменен")
            : Yii::$app->session->setFlash(Yz::FLASH_WARNING, "Не удалось изменить статус акции");

        return $this->redirect('index');
    }

    /**
     * @param $id
     * @return ActionForm
     * @throws NotFoundHttpException
     */
    protected function findAction($id)
    {
        if (($model = ActionForm::findOne(['id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
