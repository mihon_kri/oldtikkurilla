<?php

namespace modules\actions\common\models;

use modules\actions\common\types\PlanCompletePersonalActionType;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yz\interfaces\ModelInfoInterface;
use yz\admin\models\AuthItem;
use yz\admin\models\User;
use modules\profiles\common\models\Profile;
use modules\profiles\common\models\Dealer;
use modules\sales\common\models\Group;
use modules\sales\common\models\Category;
use modules\sales\common\models\Product;
use modules\sales\common\models\Sale;
use modules\sales\common\models\SaleAction;
use modules\actions\common\models\ActionType;
use modules\actions\common\models\ActionGroup;
use modules\actions\common\models\ActionCategory;
use modules\actions\common\models\ActionProduct;
use modules\actions\common\models\ActionRegion;
use modules\actions\common\models\ActionCity;
use modules\actions\common\models\ActionDealer;
use modules\actions\common\models\ActionProfile;
use modules\actions\common\models\ActionAdmin;
use modules\actions\common\types\ActionTypeInterface;
use modules\actions\common\managers\Validator;
use modules\profiles\common\models\Region;
use modules\profiles\common\models\City;
use modules\profiles\common\managers\RoleManager;
use modules\profiles\common\models\Leader;
use modules\actions\common\managers\StatusManager;

/**
 * This is the model class for table "yz_actions".
 *
 * @property integer $id
 * @property integer $admin_id
 * @property integer $type_id
 * @property string $start_on
 * @property string $code
 * @property string $end_on
 * @property string $title
 * @property string $personal_plan_formula
 * @property string $short_description
 * @property string $description
 * @property string $bonuses_formula
 * @property integer $bonuses_amount
 * @property integer $plan_amount
 * @property string $pay_type
 * @property integer $pay_threshold
 * @property integer $limit_bonuses
 * @property integer $limit_qty
 * @property integer $has_products
 * @property string $role
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Validator $validator
 * @property StatusManager $statusManager
 * @property ActionGroup[] $actionGroups
 * @property Group[] $groups
 * @property ActionCategory[] $actionCategories
 * @property Category[] $categories
 * @property ActionProduct[] $actionProducts
 * @property Product[] $products
 * @property ActionRegion[] $actionRegions
 * @property Region[] $regions
 * @property ActionCity[] $actionCities
 * @property City[] $cities
 * @property ActionDealer[] $actionDealers
 * @property Dealer[] $dealers
 * @property ActionProfile[] $actionProfiles
 * @property Profile[] $profiles
 * @property ActionAdmin[] $actionAdmins
 * @property AuthItem[] $admins
 * @property ActionType $type
 * @property Sale[] $sales
 * @property User $admin
 * @property Leader $leader
 */
class Action extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    const STATUS_NEW = 'new';
    const STATUS_ACTIVE = 'active';
    const STATUS_FINISHED = 'finished';

    /**
     * Type of bonus payments at the end of the action
     */
    const PAY_TYPE_COMPLETED = 'completed';

    /**
     * Type of bonus payments after reaching the threshold
     */
    const PAY_TYPE_THRESHOLD = 'threshold';

    /**
     * @var Validator
     */
    private $_validator;

    /**
     * @var StatusManager
     */
    private $_statusManager;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%actions}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Акция';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Акции';
    }

    /**
     * @return array
     */
    public static function getStatusesList()
    {
        return [
            self::STATUS_NEW => 'Новая',
            self::STATUS_ACTIVE => 'Активна',
            self::STATUS_FINISHED => 'Завершена',
        ];
    }

    /**
     * @return array
     */
    public static function getPayTypesList()
    {
        return [
            self::PAY_TYPE_COMPLETED => 'По окончанию акции',
            self::PAY_TYPE_THRESHOLD => 'По достижению количества баллов',
        ];
    }

    public static function getList()
    {
        return self::find()
            ->select(['title', 'id'])
            ->orderBy(['title' => SORT_ASC])
            ->indexBy('id')
            ->column();
    }

    /**
     * @return ActionQuery
     * @throws \yii\base\InvalidConfigException
     */
    public static function find()
    {
        return Yii::createObject(ActionQuery::class, [get_called_class()]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['admin_id', 'integer'],

            ['type_id', 'required'],
            ['type_id', 'integer'],

            ['start_on', 'required'],
            ['start_on', 'safe'],

            ['end_on', 'required'],
            ['end_on', 'safe'],

            ['title', 'filter', 'filter' => 'trim'],
            ['title', 'required'],
            ['title', 'string', 'max' => 255],

            ['short_description', 'string', 'max' => 255],

            ['description', 'string'],
            ['personal_plan_formula', 'string'],
            ['code', 'string'],

            ['bonuses_formula', 'required'],
            ['bonuses_formula', 'string', 'max' => 255],

            ['bonuses_amount', 'integer'],

            ['plan_amount', 'integer'],

            ['pay_type', 'required'],
            ['pay_type', 'string', 'max' => 16],
            ['pay_type', 'in', 'range' => array_keys(self::getPayTypesList())],

            ['pay_threshold', 'integer'],

            ['limit_bonuses', 'integer'],

            ['limit_qty', 'integer'],

            ['has_products', 'boolean'],
            ['has_products', 'default', 'value' => true],

            ['role', 'required'],
            ['role', 'in', 'range' => array_keys(RoleManager::getList())],

            ['status', 'string', 'max' => 32],
            ['status', 'default', 'value' => self::STATUS_NEW],
            ['status', 'in', 'range' => array_keys(self::getStatusesList())],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'admin_id' => 'Администратор',
            'code' => 'Код акции',
            'start_on' => 'Дата начала',
            'end_on' => 'Дата завершения',
            'type_id' => 'Тип акции',
            'title' => 'Название',
            'short_description' => 'Краткое описание',
            'description' => 'Описание',
            'bonuses_formula' => 'Бонусная формула',
            'bonuses_amount' => 'Сумма разовой закупки',
            'personal_plan_formula' => 'Формула расчета индивидуального плана',
            'plan_amount' => 'План',
            'pay_type' => 'Условие начисление баллов',
            'pay_threshold' => 'Порог начисления баллов',
            'limit_bonuses' => 'Ограничение по начисленным баллам для участника',
            'limit_qty' => 'Ограничение по количеству позиций для участника',
            'has_products' => 'Отображать выбор продукции',
            'role' => 'Роль участников',
            'status' => 'Статус',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    public function beforeDelete()
    {
        ActionGroup::deleteAll(['action_id' => $this->id]);
        ActionCategory::deleteAll(['action_id' => $this->id]);
        ActionProduct::deleteAll(['action_id' => $this->id]);
        ActionDealer::deleteAll(['action_id' => $this->id]);
        ActionProfile::deleteAll(['action_id' => $this->id]);
        ActionRegion::deleteAll(['action_id' => $this->id]);
        ActionCity::deleteAll(['action_id' => $this->id]);
        ActionAdmin::deleteAll(['action_id' => $this->id]);
        SaleAction::deleteAll(['action_id' => $this->id]);

        return parent::beforeDelete();
    }

    public function fields()
    {
        return [
            'id',
            'title',
            'short_description',
            'description',
            'planForAction',
            'start_on',
            'end_on',
            'has_products',
        ];
    }

    /**
     * @return Validator
     */
    public function getValidator()
    {
        if (null === $this->_validator) {
            $this->_validator = new Validator($this);
        }

        return $this->_validator;
    }

    public function getPlanForAction()
    {
        if($this->plan_amount){
            return $this->plan_amount;
        }
        if($this->type_id == 3){
            $profieId = Yii::$app->request->post('profile_id');
            $individualPlan = ActionProfile::findOne(['profile_id' => $profieId, 'action_id' => $this->id]);
            if($individualPlan){
                $pos = strpos($this->personal_plan_formula, 'plan');

                if($pos === false && $this->personal_plan_formula){
                    return $this->personal_plan_formula;
                }else{
                    $coef = str_replace('plan', '', $this->personal_plan_formula);
                    $coef = str_replace('*', '', $coef);
                    $coef = trim($coef);
                    return $individualPlan->last_year_plan*(int)$coef;
                }
            }
        }
        return [];
    }

    /**
     * @return StatusManager
     */
    public function getStatusManager()
    {
        if (null === $this->_statusManager) {
            $this->_statusManager = new StatusManager($this);
        }

        return $this->_statusManager;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActionGroups()
    {
        return $this->hasMany(ActionGroup::class, ['action_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Group::class, ['id' => 'group_id'])
            ->via('actionGroups');
    }

    /**
     * @return array
     */
    public function getActionGroupIds()
    {
        return $this->getActionGroups()->select('group_id')->column();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActionCategories()
    {
        return $this->hasMany(ActionCategory::class, ['action_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::class, ['id' => 'category_id'])
            ->via('actionCategories');
    }

    /**
     * @return array
     */
    public function getActionCategoryIds()
    {
        return $this->getActionCategories()->select('category_id')->column();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActionProducts()
    {
        return $this->hasMany(ActionProduct::class, ['action_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::class, ['id' => 'product_id'])
            ->via('actionProducts');
    }

    /**
     * @return array
     */
    public function getActionProductIds()
    {
        return $this->getActionProducts()->select('product_id')->column();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActionRegions()
    {
        return $this->hasMany(ActionRegion::class, ['action_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegions()
    {
        return $this->hasMany(Region::class, ['id' => 'region_id'])
            ->via('actionRegions');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActionCities()
    {
        return $this->hasMany(ActionCity::class, ['action_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::class, ['id' => 'city_id'])
            ->via('actionCities');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActionDealers()
    {
        return $this->hasMany(ActionDealer::class, ['action_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDealers()
    {
        return $this->hasMany(Dealer::class, ['id' => 'dealer_id'])
            ->via('actionDealers');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActionProfiles()
    {
        return $this->hasMany(ActionProfile::class, ['action_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfiles()
    {
        return $this->hasMany(Profile::class, ['id' => 'profile_id'])
            ->via('actionProfiles');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActionAdmins()
    {
        return $this->hasMany(ActionAdmin::class, ['action_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmins()
    {
        return $this->hasMany(AuthItem::class, ['name' => 'auth_item_name'])
            ->via('actionAdmins');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(ActionType::class, ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSales()
    {
        return $this->hasMany(Sale::class, ['action_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmin()
    {
        return $this->hasOne(User::class, ['id' => 'admin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeader()
    {
        return $this->hasOne(Leader::class, ['identity_id' => 'id'])
            ->via('admin');
    }

    public static function getActionName($action_id = null)
    {
        if(!$action_id){
            return "";
        }

        $action = self::findOne($action_id);

        if($action){
            return $action->title;
        }

        return "";
    }

    public static function getIndividualAction()
    {
        $arrReturn = [];

        $actions = self::find()->where(['type_id' => 3])->asArray()->all();
        foreach ($actions as $action){
            $arrReturn[$action['id']] = $action['title'];
        }
        return $arrReturn;
    }
}
