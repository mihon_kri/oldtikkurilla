<?php

namespace modules\actions\common\types;

class PlanCompletePersonalActionType implements ActionTypeInterface
{
    use ActionTypeTrait;

    public function title()
    {
        return "Бонус за выполнение индивидуального плана";
    }

    public function shortDescription()
    {
        return "Продавец или покупатель получает бонусные баллы за выполнение индивидуального поставленного плана";
    }

    /**
     * NOTICE
     * Bonuses for a this action type is created by CronJob from approved sales
     * @see \modules\sales\common\commands\CreatePlanBonusesCommand
     *
     * @return bool
     */
    public function validate()
    {
        return false;
    }
}