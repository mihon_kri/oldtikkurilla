<?php

namespace modules\actions\common\types;

use yii\base\Exception;
use modules\sales\common\models\Sale;
use modules\sales\common\models\SaleAction;
use modules\actions\common\calculators\BonusesFormulaCalculator;

trait ActionTypeTrait
{
    /**
     * @var Sale
     */
    protected $sale;

    /**
     * @param Sale $sale
     */
    public function __construct(Sale $sale = null)
    {
        $this->sale = $sale;
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function verify()
    {
        if (false === $this->validate()) {
            return false;
        }

        $this->createSaleAction();

        return true;
    }

    /**
     * @return integer
     */
    public function calculateBonuses()
    {
        $calculator = new BonusesFormulaCalculator($this->sale->action);
        $bonuses = $calculator->calculate($this->sale->bonuses);

        return $bonuses;
    }

    /**
     * @return SaleAction|null
     * @throws Exception
     */
    public function createSaleAction()
    {
        $action = $this->sale->action;

        $model = new SaleAction;
        $model->sale_id = $this->sale->id;
        $model->action_id = $action->id;
        $model->bonuses = $this->calculateBonuses();

        if (false === $model->save(false)) {
            throw new Exception('Не удалось сохранить акцию: ' . implode(', ', $model->getFirstErrors()));
        }

        return $model;
    }
}