<?php

use yii\db\Migration;

/**
 * Class m190418_084401_action_add_action_type_data
 */
class m190418_084401_action_add_action_type_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $className = \modules\actions\common\types\PlanCompletePersonalActionType::class;

        Yii::$app->db->createCommand()->batchInsert('{{%actions_types}}', ['id', 'title', 'short_description', 'className', 'created_at'], [
            [
                '3',
                'Бонус за выполнение индивидуального  плана',
                'Продавец или покупатель получает бонусные баллы за выполнение индивидуального поставленного плана',
                $className,
                date("Y-m-d H:i:s")
            ],
        ])->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
