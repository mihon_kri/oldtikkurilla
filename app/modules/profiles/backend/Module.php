<?php

namespace modules\profiles\backend;

use modules\profiles\backend\rbac\Rbac;
use yz\icons\Icons;

/**
 * Class Module
 */
class Module extends \modules\profiles\common\Module
{
    public function getName()
    {
        return 'Профили участников';
    }

    public function getAdminMenu()
    {
        return [
            [
                'label' => 'Участники программы',
                'icon' => Icons::o('user'),
                'items' => [
                    [
                        'route' => ['/profiles/profiles/index'],
                        'label' => 'Профили участников',
                        'icon' => Icons::o('group'),
                    ],
                    [
                        'route' => ['/profiles/leaders/index'],
                        'label' => 'Администраторы',
                        'icon' => Icons::o('user-secret'),
                    ],
                    [
                        'route' => ['/profiles/profile-transactions/index'],
                        'label' => 'Движение баллов участников',
                        'icon' => Icons::o('rub'),
                    ],
//                    [
//                        'route' => ['/profiles/dealers/index'],
//                        'label' => 'Точки продаж',
//                        'icon' => Icons::o('flag'),
//                    ],
                    [
                        'route' => ['/profiles/cities/index'],
                        'label' => 'Города',
                        'icon' => Icons::o('globe'),
                    ],
                    [
                        'route' => ['/profiles/regions/index'],
                        'label' => 'Регионы',
                        'icon' => Icons::o('globe'),
                    ],
                ]
            ],
        ];
    }

    public function getAuthItems()
    {
        return array_merge(parent::getAuthItems(), Rbac::dependencies());
    }
}