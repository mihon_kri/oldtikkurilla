<?php

namespace modules\profiles\backend\controllers;

use Yii;
use yz\admin\actions\ExportAction;
use yz\admin\grid\columns\DataColumn;
use yz\admin\grid\filters\DateRangeFilter;
use backend\base\Controller;
use modules\profiles\backend\models\ProfileTransaction;
use modules\profiles\backend\models\ProfileTransactionSearch;
use marketingsolutions\finance\models\Transaction;

/**
 * Class ProfileTransactionsController
 */
class ProfileTransactionsController extends Controller
{
    public function actions()
    {
        return array_merge(parent::actions(), [
            'export' => [
                'class' => ExportAction::class,
                'dataProvider' => function ($params) {
                    $searchModel = Yii::createObject(ProfileTransactionSearch::class);
                    $dataProvider = $searchModel->search($params);
                    return $dataProvider;
                },
            ]
        ]);
    }

    public function actionIndex()
    {
        /** @var ProfileTransactionSearch $searchModel */
        $searchModel = Yii::createObject(ProfileTransactionSearch::class);
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $dataProvider->setSort([
            'defaultOrder' => ['id' => SORT_DESC],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'columns' => $this->getGridColumns(),
        ]);
    }

    public function getGridColumns()
    {
        return [
            'id',
            [
                'attribute' => 'type',
                'filter' => Transaction::getTypeValues(),
                'titles' => Transaction::getTypeValues(),
                'labels' => [
                    Transaction::INCOMING => DataColumn::LABEL_SUCCESS,
                    Transaction::OUTBOUND => DataColumn::LABEL_DANGER,
                ]
            ],
            'profile__full_name',
            'profile__phone_mobile',
            'amount',
            [
                'attribute' => 'balance_before',
                'value' => function (Transaction $model) {
                    return $model->balance_before ? $model->balance_before : '';
                }
            ],
            [
                'attribute' => 'balance_after',
                'value' => function (Transaction $model) {
                    return $model->balance_after ? $model->balance_after : '';
                }
            ],
            'title',
            'comment',
            [
                'attribute' => 'created_at',
                'contentOptions' => ['style' => 'text-align:center; width: 180px;'],
                'filter' => DateRangeFilter::instance(),
                'value' => function (ProfileTransaction $model) {
                    return (new \DateTime($model->created_at))->format('d.m.Y H:i');
                }
            ],
        ];
    }
}