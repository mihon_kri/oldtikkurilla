<?php

namespace modules\profiles\backend\controllers\import;

use yii\web\Controller;
use yii\helpers\Html;
use yz\admin\import\BatchImportAction;
use yz\admin\import\ImportForm;
use yz\admin\import\InterruptImportException;
use yz\admin\traits\CheckAccessTrait;
use yz\admin\contracts\AccessControlInterface;
use modules\profiles\common\models\Leader;
use modules\profiles\backend\rbac\Rbac;
use modules\profiles\backend\forms\LeaderForm;
use modules\profiles\common\models\Profile;
use modules\profiles\common\models\Region;

class LeadersController extends Controller implements AccessControlInterface
{
    use CheckAccessTrait;

    const FIELD_FULLNAME = 'ФИО';
    const FIELD_ROLE = 'Роль';
    const FIELD_REGION = 'Регион';
    const FIELD_PHONE = 'Телефон';
    const FIELD_EMAIL = 'Email';
    const FIELD_LOGIN = 'Логин';
    const FIELD_PASSWORD = 'Пароль';
    const FIELD_ADMIN_REGION = 'Администратор региона';
    const FIELD_PROFILES = 'Участники';

    public function actions()
    {
        return [
            'index' => [
                'class' => BatchImportAction::class,
                'extraView' => '@modules/profiles/backend/views/leaders/import/leaders.php',
                'importConfig' => [
                    'availableFields' => [
                        self::FIELD_FULLNAME => 'ФИО',
                        self::FIELD_ROLE => 'Роль',
                        self::FIELD_REGION => 'Навазние региона',
                        self::FIELD_PHONE => 'Телефон',
                        self::FIELD_EMAIL => 'Email',
                        self::FIELD_LOGIN => 'Логин',
                        self::FIELD_PASSWORD => 'Пароль',
                        self::FIELD_ADMIN_REGION => 'Администратор региона (ФИО)',
                        self::FIELD_PROFILES => 'Участники (список ФИО через ",")',
                    ],
                    'rowImport' => [$this, 'rowImport'],
                ]
            ]
        ];
    }

    /**
     * @param ImportForm $form
     * @param array $row
     * @throws InterruptImportException
     */
    public function rowImport(ImportForm $form, array $row)
    {
        $row = array_map('trim', $row);

        $this->importLeader($row);
    }

    /**
     * @param $row
     * @return Leader
     * @throws InterruptImportException
     */
    private function importLeader(array $row)
    {
        if (($row[self::FIELD_LOGIN] && $row[self::FIELD_PASSWORD]) === false) {
            throw new InterruptImportException('Не указан логин или пароль', $row);
        }

        $role = array_search($row[self::FIELD_ROLE], Rbac::getRolesList());
        $parts = explode(' ', $row[self::FIELD_FULLNAME]);
        $profileIds = [];

        if ($role === Rbac::ROLE_ADMIN_REGION) {
            $region = Region::findOne(['title' => $row[self::FIELD_REGION]]);

            if (null === $region) {
                throw new InterruptImportException('Регион не найден', $row);
            }
        }

        if ($role === Rbac::ROLE_ADMIN_OP) {
            $adminRegion = Leader::findOne(['full_name' => $row[self::FIELD_ADMIN_REGION]]);

            if (null === $adminRegion) {
                throw new InterruptImportException('Администратор региона не найден', $row);
            }

            $region = $adminRegion->region;

            $profileIds = $this->getProfileIds($row);
        }

        $leader = new LeaderForm;
        $leader->role = $role;
        $leader->first_name = $parts[1] ?? null;
        $leader->last_name = $parts[0] ?? null;
        $leader->middle_name = $parts[2] ?? null;
        $leader->phone_mobile_local = $row[self::FIELD_PHONE];
        $leader->email = $row[self::FIELD_EMAIL];
        $leader->region_id = $region->id ?? null;
        $leader->leader_id = $adminRegion->id ?? null;
        $leader->profileIds = $profileIds;
        $leader->login = $row[self::FIELD_LOGIN];
        $leader->password = $leader->passwordCompare = $row[self::FIELD_PASSWORD];

        if (false == $leader->process()) {
            throw new InterruptImportException('Ошибка при импорте администратора: ' . implode(', ', $leader->getFirstErrors()), $row);
        }

        return $leader;
    }

    /**
     * @param array $row
     * @return array
     * @throws InterruptImportException
     */
    private function getProfileIds(array $row)
    {
        $profileNames = array_filter(array_map('trim', explode(',', $row[self::FIELD_PROFILES])));
        $profileIds = [];

        if (empty($profileNames)) {
            return $profileIds;
        }

        foreach ($profileNames as $profileName) {
            $profile = Profile::findOne(['full_name' => $profileName]);

            if (null === $profile) {
                throw new InterruptImportException("Участник '" . Html::encode($profileName) . "' не найден", $row);
            }

            $profileIds[] = $profile->id;
        }

        return $profileIds;
    }
}