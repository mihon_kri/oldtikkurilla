<?php

namespace modules\profiles\backend\controllers\import;

use backend\base\Controller;
use yz\admin\import\BatchImportAction;
use yz\admin\import\ImportForm;
use yz\admin\import\InterruptImportException;
use modules\profiles\backend\forms\ProfileRegularImportForm;
use modules\profiles\common\managers\RoleManager;
use modules\profiles\common\models\City;
use modules\profiles\common\models\Dealer;

class ProfilesRegularController extends Controller
{
    const FIELD_FULLNAME = 'фио';
    const FIELD_CITY = 'город';
    const FIELD_PHONE = 'телефон';
    const FIELD_EMAIL = 'email';
   // const FIELD_DEALER = 'место работы';

    public function actions()
    {
        return [
            'index' => [
                'class' => BatchImportAction::class,
                'extraView' => '@modules/profiles/backend/views/profiles/import/regular.php',
                'importConfig' => [
                    'availableFields' => [
                        self::FIELD_FULLNAME => 'ФИО участника',
                        self::FIELD_CITY => 'Город фактического проживания',
                        self::FIELD_PHONE => 'Номер телефона',
                        self::FIELD_EMAIL => 'Электронная почта участника',
                       // self::FIELD_DEALER => 'Место работы'
                    ],
                    'rowImport' => [$this, 'rowImport'],
                    'skipFirstLine' => true,
                ]
            ]
        ];
    }

    public function rowImport(ImportForm $form, array $row)
    {
        $row = array_map('trim', $row);

        $this->importProfile($row);
    }

    /**
     * @param $row
     * @return ProfileRegularImportForm
     * @throws InterruptImportException
     */
    private function importProfile($row)
    {
        $city = City::findOne(['title' => $row[self::FIELD_CITY]]);

        if (null === $city) {
            throw new InterruptImportException('Город не найден', $row);
        }

       // $dealer = Dealer::findOne(['name' => $row[self::FIELD_DEALER]]);

//        if (null === $dealer) {
//            $dealer = new Dealer();
//            $dealer->name = $row[self::FIELD_DEALER];
//
//            if ($dealer->save() === false) {
//                throw new InterruptImportException('Не удалось сохранить торговую точку: ' . implode(', ', $dealer->getFirstErrors()), $row);
//            }
//        }

        $parts = explode(' ', $row[self::FIELD_FULLNAME]);

        $profile = new ProfileRegularImportForm;
        $profile->role = RoleManager::ROLE_REGULAR;
        $profile->first_name = $parts[1] ?? null;
        $profile->last_name = $parts[0] ?? null;
        $profile->middle_name = $parts[2] ?? null;
        $profile->phone_mobile_local = $row[self::FIELD_PHONE];
        $profile->email = $row[self::FIELD_EMAIL];
        $profile->city_id = $city->id;
        //$profile->dealer_id = $dealer->id;
        $profile->checked_at = (new \DateTime())->format('Y-m-d H:i:s');

        if ($profile->save() === false) {
            throw new InterruptImportException('Ошибка при импорте участника: ' . implode(', ', $profile->getFirstErrors()), $row);
        }

        return $profile;
    }
}