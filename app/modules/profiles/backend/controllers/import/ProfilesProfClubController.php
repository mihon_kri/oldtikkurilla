<?php

namespace modules\profiles\backend\controllers\import;

use backend\base\Controller;
use yz\admin\import\BatchImportAction;
use yz\admin\import\ImportForm;
use yz\admin\import\InterruptImportException;
use modules\profiles\backend\forms\ProfileProfClubImportForm;
use modules\profiles\common\managers\RoleManager;
use modules\profiles\common\models\City;

class ProfilesProfClubController extends Controller
{
    const FIELD_FULLNAME = 'фио';
    const FIELD_CITY = 'город';
    const FIELD_PHONE = 'телефон';
    const FIELD_EMAIL = 'email';

    public function actions()
    {
        return [
            'index' => [
                'class' => BatchImportAction::class,
                'extraView' => '@modules/profiles/backend/views/profiles/import/profclub.php',
                'importConfig' => [
                    'availableFields' => [
                        self::FIELD_FULLNAME => 'ФИО участника',
                        self::FIELD_CITY => 'Город фактического проживания',
                        self::FIELD_PHONE => 'Номер телефона',
                        self::FIELD_EMAIL => 'Электронная почта участника',
                    ],
                    'rowImport' => [$this, 'rowImport'],
                    'skipFirstLine' => true,
                ]
            ]
        ];
    }

    public function rowImport(ImportForm $form, array $row)
    {
        $row = array_map('trim', $row);

        $this->importProfile($row);
    }

    /**
     * @param $row
     * @return ProfileProfClubImportForm
     * @throws InterruptImportException
     */
    private function importProfile($row)
    {
        $city = City::findOne(['title' => $row[self::FIELD_CITY]]);

        if (null === $city) {
            throw new InterruptImportException('Город не найден', $row);
        }

        $parts = explode(' ', $row[self::FIELD_FULLNAME]);

        $profile = new ProfileProfClubImportForm;
        $profile->role = RoleManager::ROLE_PROFCLUB;
        $profile->first_name = $parts[1] ?? null;
        $profile->last_name = $parts[0] ?? null;
        $profile->middle_name = $parts[2] ?? null;
        $profile->phone_mobile_local = $row[self::FIELD_PHONE];
        $profile->email = $row[self::FIELD_EMAIL];
        $profile->city_id = $city->id;
        $profile->checked_at = (new \DateTime())->format('Y-m-d H:i:s');

        if ($profile->save() === false) {
            throw new InterruptImportException('Ошибка при импорте участника: ' . implode(', ', $profile->getFirstErrors()), $row);
        }

        return $profile;
    }
}