<?php

/* @var $this yii\web\View */

$this->title = 'Импорт участников регулярных акций';
$this->params['header'] = $this->title;

?>

<div class="row">
    <div class="col-sm-offset-2 col-sm-8">
        <a href="/media/profiles_regular_example.xlsx">
            <i class="fa fa-file-excel"></i>
            Пример файла для импорта
        </a>
    </div>
</div>