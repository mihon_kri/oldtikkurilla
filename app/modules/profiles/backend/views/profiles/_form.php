<?php

use yii\helpers\FileHelper;
use yz\admin\helpers\AdminHtml;
use yz\admin\widgets\ActiveForm;
use yz\admin\widgets\Box;
use yz\admin\widgets\FormBox;
use yii\helpers\Url;
use modules\profiles\common\managers\RoleManager;
use modules\profiles\common\models\City;
use modules\profiles\common\models\Dealer;
use modules\profiles\backend\forms\DivisionForm;

/**
 * @var yii\web\View $this
 * @var modules\profiles\common\models\Profile $model
 * @var yz\admin\widgets\ActiveForm $form
 */

?>

<div class="row">
	<div class="col-md-6">
        <?php $box = FormBox::begin(['cssClass' => 'profile-form box-primary', 'title' => '']) ?>

        <?php $form = ActiveForm::begin([
            'options' => [
                'class' => 'js-profile-form'
            ]
        ]); ?>

        <?php $box->beginBody() ?>

            <?= $form->field($model, 'role')->dropDownList(RoleManager::getList(), [
                'prompt' => 'Выбрать роль...',
                'class' => 'form-control js-profile-role-select',
            ]) ?>

            <?php if ($model->isNewRecord === false && $model->roleManager->isProfclub()) : ?>
                <?= $form->field($model, 'clubcard', ['options' => ['class' => 'form-group js-profile-clubcard-field hidden']])->staticControl([
                    'disabled' => 'disabled'
                ]) ?>
            <?php endif; ?>

            <?= $form->field($model, 'first_name')->textInput(['maxlength' => 32]) ?>
            <?= $form->field($model, 'last_name')->textInput(['maxlength' => 32]) ?>
            <?= $form->field($model, 'middle_name')->textInput(['maxlength' => 32]) ?>
            <?= $form->field($model, 'phone_mobile_local')->widget(\yii\widgets\MaskedInput::class, [
                'mask' => '+7 999 999-99-99',
            ]) ?>
            <?= $form->field($model, 'email')->textInput(['maxlength' => 64]) ?>
            <?= $form->field($model, 'city_id')->select2(City::getOptions(), ['prompt' => 'Выбрать ...']) ?>

            <?= $form->field($model, 'work_place', ['options' => ['class' => 'form-group js-profile-dealer-field hidden']])
                ->textInput(['maxlength' => 255]) ?>



        <?php $box->endBody() ?>

        <?php $box->actions([
            AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_STAY, $model->isNewRecord),
            AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_LEAVE, $model->isNewRecord),
            AdminHtml::actionButton(AdminHtml::ACTION_SAVE_AND_CREATE, $model->isNewRecord),
        ]) ?><?php ActiveForm::end(); ?>

        <?php FormBox::end() ?>

        <?php if ($model->isNewRecord == false): ?>
			<!--ДОП ИНФО УЧАСТНИКА-->
            <?php Box::begin(['title' => null]) ?>
            <?= \yii\widgets\DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'created_at:datetime',
                    'registered_at:datetime',
                    'blocked_at:datetime',
                    'banned_at:datetime',
                ],
            ]) ?>

            <?php Box::end() ?>

			<!--НДФЛ АНКЕТА УЧАСТНИКА-->
            <?php Box::begin(['title' => 'Анкета НДФЛ']) ?>
            <?= \modules\profiles\common\widgets\ProfileTaxWidget::widget(['profile' => $model]) ?>

            <?php Box::end() ?>

        <?php endif; ?>
	</div>
	<div class="col-md-6">
		<!--УЧАСТНИК-->
        <?php if ($model->isNewRecord == false): ?>

			<!--БЛОКИРОВКА УЧАСТНИКА-->
            <?php Box::begin(['title' => null]) ?>
			<div style="margin-top:-5px">
                <?php if (!$model->blocked_at): ?><span class="label label-success">активен</span><?php endif; ?>
                <?php if ($model->blocked_at): ?><span class="label label-danger">заблокирован</span><?php endif; ?>
				<b>Блокировка участника</b>
			</div>
			<hr/>
            <?php if ($model->blocked_at): ?>
				<div class="row">
					<div class="col-md-8">
						<p><?= $model->blocked_reason ?></p>
					</div>
					<div class="col-md-4">
						<a href="<?= Url::to(['/profiles/profiles/unblock', 'id' => $model->id]) ?>" type="submit"
						   class="btn btn-success btn-block pull-right">
							Разблокировать участника
						</a>
					</div>
				</div>

            <?php else: ?>

                <?php $form1 = ActiveForm::begin(['action' => ['block', 'id' => $model->id]]); ?>
				<div class="row">
					<div class="col-md-8">
                        <?= \yii\bootstrap\Html::textarea('reason', '', [
                            'class' => 'form-control',
                            'placeholder' => 'Причина блокировки'
                        ]) ?>
					</div>
					<div class="col-md-4">
						<button type="submit" class="btn btn-danger btn-block pull-right">
							Заблокировать
						</button>
					</div>
				</div>
                <?php ActiveForm::end(); ?>

            <?php endif ?>

            <?php Box::end() ?>

			<!--БАН УЧАСТНИКА-->
            <?php Box::begin(['title' => null]) ?>
			<div style="margin-top:-5px">
                <?php if (!$model->banned_at): ?><span class="label label-success">активен</span><?php endif; ?>
                <?php if ($model->banned_at): ?><span class="label label-danger">забанен</span><?php endif; ?>
				<b>Бан участника</b>
			</div>
			<hr/>
            <?php if ($model->banned_at): ?>
				<div class="row">
					<div class="col-md-8">
						<p><?= $model->banned_reason ?></p>
					</div>
					<div class="col-md-4">
						<a href="<?= Url::to(['/profiles/profiles/unban', 'id' => $model->id]) ?>" type="submit"
						   class="btn btn-success btn-block">
							Разбанить участника
						</a>
					</div>
				</div>

            <?php else: ?>

                <?php $form2 = ActiveForm::begin(['action' => ['ban', 'id' => $model->id]]); ?>
				<div class="row">
					<div class="col-md-8">
                        <?= \yii\bootstrap\Html::textarea('reason', '', [
                            'class' => 'form-control',
                            'placeholder' => 'Причина бана'
                        ]) ?>
					</div>
					<div class="col-md-4">
						<button type="submit" class="btn btn-danger btn-block pull-right">
							Забанить
						</button>
					</div>
				</div>
                <?php ActiveForm::end(); ?>

            <?php endif ?>

            <?php Box::end() ?>
            <!--Задать Дивизион участника-->
            <?php Box::begin(['title' => 'Дивизион для региона у участника']) ?>
            <?php $formDivision = ActiveForm::begin(['action' => ['add-division', 'city_id' => $model->city_id]]); ?>
            <div class="row">
                <div class="col-md-8">
                    <?php $division->division_name = DivisionForm::getDivision($model->city_id); ?>
                    <?= $formDivision->field($division, 'division_name') ?>
                </div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-danger btn-block pull-right">Задать дивизион</button>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
            <?php Box::end() ?>


            <!--ТРАНЗАКЦИИ УЧАСТНИКА-->

            <?php Box::begin(['title' => 'Баланс участника']) ?>
            <?= \modules\profiles\common\widgets\ProfilePurseWidget::widget(['profile' => $model]) ?>

            <?php Box::end() ?>

			<!--ЗАКАЗЫ УЧАСТНИКА-->
            <?php Box::begin(['title' => 'Заказы участника']) ?>
            <?= \modules\profiles\common\widgets\ProfileOrdersWidget::widget(['profile' => $model]) ?>

            <?php Box::end() ?>

        <?php endif ?>

        <?= $this->render('partials/profile-sidebar', compact('model')) ?>
	</div>
</div>
