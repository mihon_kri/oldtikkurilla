<?php

use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\bootstrap\ButtonDropdown;
use yz\admin\grid\GridView;
use yz\admin\widgets\ActionButtons;
use yz\admin\widgets\Box;
use yz\icons\Icons;
use modules\profiles\common\models\Profile;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var modules\profiles\backend\models\ProfileSearch $searchModel
 * @var array $columns
 */

$this->title = modules\profiles\common\models\Profile::modelTitlePlural();
$this->params['breadcrumbs'][] = $this->title;
$this->params['header'] = $this->title;

# CSS
$css = <<<CSS
	.purse-balance {
		background: gold;
    	padding: 3px 5px;
    	border-radius: 50%;
    	border: 1px solid orange;
    	box-shadow: 0 0 5px rgba(0,0,0,0.5);
	}
CSS;
$this->registerCss($css);
?>

<?php $box = Box::begin(['cssClass' => 'profile-index box-primary']) ?>
<div class="text-right">
    <?php echo ActionButtons::widget([
        'order' => [['export', 'import', 'create', 'delete', 'return']],
        'gridId' => 'profile-grid',
        'searchModel' => $searchModel,
        'modelClass' => Profile::class,
        'buttons' => [
            'import' => function () {
                return ButtonDropdown::widget([
                    'tagName' => 'span',
                    'label' => 'Импорт',
                    'encodeLabel' => false,
                    'split' => false,
                    'dropdown' => [
                        'encodeLabels' => false,
                        'items' => [
                            [
                                'label' => 'Импорт участников ProfClub',
                                'url' => ['/profiles/import/profiles-prof-club'],
                                'linkOptions' => [
                                    'data' => [
                                        'grid' => 'profile-grid',
                                        //'grid-bind' => 'selection',
                                       // 'grid-param' => 'ids',
                                    ],
                                ]
                            ],
                            [
                                'label' => 'Импорт участников регулярных акций',
                                'url' => ['/profiles/import/profiles-regular'],
                                'linkOptions' => [
                                    'data' => [
                                        'grid' => 'profile-grid',
                                        //'grid-bind' => 'selection',
                                        //'grid-param' => 'ids',
                                    ],
                                ]
                            ],
                        ],
                    ],
                    'options' => [
                        'class' => 'btn btn-default',
                    ],
                ]);
            }
        ]
    ]) ?>
</div>

<?= GridView::widget([
    'id' => 'profile-grid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => array_merge([
        ['class' => 'yii\grid\CheckboxColumn'],
    ], $columns, [
        [
            'class' => 'yz\admin\widgets\ActionColumn',
            'template' => '{bon} {login} {update} {delete}',
            'buttons' => [
                'bon' => function ($url, Profile $model) {
                    return Html::a(Icons::i('dollar'), Url::to(['/manual/manage-bonuses/index', 'id' => $model->id]), [
                        'title' => 'Добавить бонус',
                        'data-method' => 'post',
                        'class' => 'btn btn-info btn-sm',
                        'data-pjax' => '0',
                    ]);
                },
                'login' => function ($url, Profile $model) {
                    return Html::a(Icons::i('key'), '/profiles/profiles/login?id=' . $model->id, [
                        'target' => '_blank',
                        'title' => 'Войти под участником',
                        'data-method' => 'post',
                        'class' => 'btn btn-warning btn-sm',
                        'data-pjax' => '0',
                    ]);
                },
            ],
        ],
    ]),
]); ?>
<?php Box::end() ?>


