<?php

namespace modules\profiles\backend\models;

use modules\profiles\common\models\City;
use modules\profiles\common\models\Province;
use modules\profiles\common\models\Region;
use yz\admin\search\WithExtraColumns;


/**
 * Class CityWithDataSearch
 */
class CityWithDataSearch extends CitySearch
{
    use WithExtraColumns;

	public function rules()
	{
		return array_merge(parent::rules(), [
			[self::extraColumns(), 'safe']
		]);
	}

    protected static function extraColumns()
    {
        return [
            'region__title',
            'region__name',
        ];
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), static::extraColumns());
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'region__title' => 'Регион',
            'region__name' => 'Название региона',
        ]);
    }

	protected function prepareQuery()
	{
		return static::find()
			->select(self::selectWithExtraColumns(['city.*']))
			->from(['city' => City::tableName()])
			->leftJoin(['region' => Region::tableName()], 'region.id = city.region_id');
	}

	protected function prepareFilters($query)
	{
		parent::prepareFilters($query);

		self::filtersForExtraColumns($query);
	}
}