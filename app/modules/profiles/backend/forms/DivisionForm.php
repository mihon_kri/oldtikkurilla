<?php
namespace modules\profiles\backend\forms;

use modules\profiles\common\models\City;
use modules\profiles\common\models\Region;
use Yii;
use yii\base\Exception;
use yii\base\Model;

class DivisionForm extends Model
{
    public $division_name;

    public function rules()
    {
         return [
             ['division_name', 'string', 'max' => 255],
         ];
    }

    public function attributeLabels()
    {
        return [
            'division_name' => 'Дивизион'
        ];
    }

    public static function getDivision($cityId)
    {
        $model = City::find()
            ->select('{{%regions}}.division_name as division')
            ->leftJoin('{{%regions}}', '{{%regions}}.id={{%cities}}.region_id')
            ->where(['{{%cities}}.id' => $cityId])
            ->asArray()
            ->one();
        if(!$model){
            return false;
        }
        return $model['division'];
    }

}