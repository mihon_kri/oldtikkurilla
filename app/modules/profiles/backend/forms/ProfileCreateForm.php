<?php

namespace modules\profiles\backend\forms;

use modules\profiles\common\managers\RoleManager;
use modules\profiles\common\models\Profile;

class ProfileCreateForm extends Profile
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['role', 'required'],

            ['first_name', 'required'],

            ['last_name', 'required'],

            ['middle_name', 'required'],

            ['city_id', 'required'],

            ['email', 'required'],

            ['dealer_id', 'safe',
                'when' => function(ProfileCreateForm $model) {
                    return $model->roleManager->isRegular();
                },
                'whenClient' => "function(attribute, value) {
                    return $('#profilecreateform-role').val() == '".RoleManager::ROLE_REGULAR."';
                }",
            ],
            ['work_place', 'safe'],
        ]);
    }

}