<?php

namespace modules\profiles\backend\forms;

use modules\profiles\common\models\Profile;

class ProfileProfClubImportForm extends Profile
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['first_name', 'required'],

            ['last_name', 'required'],

            ['middle_name', 'required'],

            ['city_id', 'required'],

            ['email', 'required'],
        ]);
    }
}