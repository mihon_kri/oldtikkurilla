<?php

use yii\db\Migration;

/**
 * Class m190320_102914_profiles_add_column_work_place_to_profiles_table
 */
class m190320_102914_profiles_add_column_work_place_to_profiles_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profiles}}', 'work_place', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profiles}}', 'work_place');
    }
}
