<?php

use yii\db\Migration;

/**
 * Class m181229_140755_profiles_add_pers_at_column
 */
class m181229_140755_profiles_add_pers_at_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%profiles}}', 'pers_at', $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%profiles}}', 'pers_at');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181229_140755_profiles_add_pers_at_column cannot be reverted.\n";

        return false;
    }
    */
}
