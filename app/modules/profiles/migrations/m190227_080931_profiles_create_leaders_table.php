<?php

use yii\db\Migration;

/**
 * Class m190227_080931_profiles_create_leaders_table
 */
class m190227_080931_profiles_create_leaders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%leaders}}', [
            'id' => $this->primaryKey(),
            'identity_id' => $this->integer(),
            'role' => $this->string(16),
            'region_id' => $this->integer(),
            'leader_id' => $this->integer(),
            'first_name' => $this->string(32),
            'last_name' => $this->string(32),
            'middle_name' => $this->string(32),
            'full_name' => $this->string(),
            'phone_mobile' =>$this->string(16),
            'email' => $this->string(128),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);

        $this->addForeignKey('{{%fk-leaders-admin_users}}',
            '{{%leaders}}', 'identity_id',
            '{{%admin_users}}', 'id',
            'RESTRICT', 'CASCADE');

        $this->addForeignKey('{{%fk-leaders-regions}}',
            '{{%leaders}}', 'region_id',
            '{{%regions}}', 'id',
            'RESTRICT', 'CASCADE');

        $this->addForeignKey('{{%fk-leaders-leaders}}',
            '{{%leaders}}', 'leader_id',
            '{{%leaders}}', 'id',
            'RESTRICT', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%fk-leaders-leaders}}', '{{%leaders}}');
        $this->dropForeignKey('{{%fk-leaders-regions}}', '{{%leaders}}');
        $this->dropForeignKey('{{%fk-leaders-admin_users}}', '{{%leaders}}');

        $this->dropTable('{{%leaders}}');
    }
}
