<?php

use yii\db\Migration;

/**
 * Class m190523_095555_profiles_add_admin_region_relation_table
 */
class m190523_095555_profiles_add_admin_region_relation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }
        $this->createTable('{{%leader_admin_region}}', [
            'id' => $this->primaryKey(),
            'leader_id' => $this->integer(),
            'region_id' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('leader_id', '{{%leader_admin_region}}', 'leader_id');
        $this->createIndex('region_id', '{{%leader_admin_region}}', 'region_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('leader_id', '{{%leader_admin_region}}');
        $this->dropIndex('region_id', '{{%leader_admin_region}}');
        $this->dropTable('{{%leader_admin_region}}');
    }
}
