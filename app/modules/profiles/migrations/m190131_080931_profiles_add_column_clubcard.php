<?php

use yii\db\Migration;

/**
 * Class m190131_080931_profiles_add_column_clubcard
 */
class m190131_080931_profiles_add_column_clubcard extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn("{{%profiles}}", 'clubcard', $this->integer(5)->unique());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn("{{%profiles}}", 'clubcard');
    }
}
