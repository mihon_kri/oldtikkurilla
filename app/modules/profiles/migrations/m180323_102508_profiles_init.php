<?php

use yii\db\Migration;

/**
 * Class m180323_102508_profiles_init
 */
class m180323_102508_profiles_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%profiles}}', [
            'id' => $this->primaryKey(),
            'passhash' => $this->string(),
            'first_name' => $this->string(32),
            'last_name' => $this->string(32),
            'middle_name' => $this->string(32),
            'full_name' => $this->string(),
            'phone_mobile' => $this->string(16)->unique(),
            'email' => $this->string(64),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'birthday_on' => $this->date(),
            'dealer_id' => $this->integer(),
            'city_id' => $this->integer(),
            'region_id' => $this->integer(),
            'avatar' => $this->string(),
            'gender' => $this->string(),
            'role' => $this->string(),
            'specialty' => $this->string(),
            'document' => $this->string(500),
            'blocked_at' => $this->dateTime(),
            'blocked_reason' => $this->text(),
            'banned_at' => $this->dateTime(),
            'banned_reason' => $this->text(),
            'phone_confirmed_at' => $this->dateTime(),
            'email_confirmed_at' => $this->dateTime(),
            'uniqid' => $this->string(50),
            'registered_at' => $this->dateTime(),
            'checked_at' => $this->dateTime(),
            'external_id' => $this->integer(),
            'external_token' => $this->string(),
        ], $tableOptions);
        $this->createIndex('dealer_id', '{{%profiles}}', 'dealer_id');
        $this->createIndex('city_id', '{{%profiles}}', 'city_id');
        $this->createIndex('region_id', '{{%profiles}}', 'region_id');

        $this->createTable('{{%cities}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'region_id' => $this->integer(),
            'country_id' => $this->integer(),
        ], $tableOptions);
        $this->createIndex('region_id', '{{%cities}}', 'region_id');
        $this->createIndex('country_id', '{{%cities}}', 'country_id');

        $this->createTable('{{%regions}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'name' => $this->string(),
            'country_id' => $this->integer(),
        ], $tableOptions);
        $this->createIndex('country_id', '{{%regions}}', 'country_id');

        $this->createTable('{{%countries}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'shortTitle' => $this->string(),
        ], $tableOptions);

        $this->createTable('{{%dealers}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'address' => $this->string(),
            'leader_id' => $this->integer(),
            'city_id' => $this->integer(),
            'region_id' => $this->integer(),
            'code' => $this->string(),
            'document' => $this->string(500),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);
        $this->createIndex('leader_id', '{{%dealers}}', 'leader_id');
        $this->createIndex('city_id', '{{%dealers}}', 'city_id');
        $this->createIndex('region_id', '{{%dealers}}', 'region_id');

        //------------------------------------------------------------------

        $this->addForeignKey('fk-cities-region-id',
            '{{%cities}}', 'region_id',
            '{{%regions}}', 'id',
            'RESTRICT', 'CASCADE'
        );

        $this->addForeignKey('fk-cities-country-id',
            '{{%cities}}', 'country_id',
            '{{%countries}}', 'id',
            'RESTRICT', 'CASCADE'
        );

        $this->addForeignKey('fk-regions-country-id',
            '{{%regions}}', 'country_id',
            '{{%countries}}', 'id',
            'RESTRICT', 'CASCADE'
        );

        $this->addForeignKey('fk-dealers-city-id',
            '{{%dealers}}', 'city_id',
            '{{%cities}}', 'id',
            'RESTRICT', 'CASCADE'
        );

        $this->addForeignKey('fk-dealers-region-id',
            '{{%dealers}}', 'region_id',
            '{{%regions}}', 'id',
            'RESTRICT', 'CASCADE'
        );

        $this->addForeignKey('fk-profiles-dealer-id',
            '{{%profiles}}', 'dealer_id',
            '{{%dealers}}', 'id',
            'RESTRICT', 'CASCADE'
        );

        $this->addForeignKey('fk-profiles-city-id',
            '{{%profiles}}', 'city_id',
            '{{%cities}}', 'id',
            'RESTRICT', 'CASCADE'
        );

        $this->addForeignKey('fk-profiles-regions-id',
            '{{%profiles}}', 'region_id',
            '{{%regions}}', 'id',
            'RESTRICT', 'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        @$this->dropTable('{{%profiles}}');
        @$this->dropTable('{{%dealers}}');
        @$this->dropTable('{{%cities}}');
        @$this->dropTable('{{%regions}}');
        @$this->dropTable('{{%countries}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180323_102508_profiles_init cannot be reverted.\n";

        return false;
    }
    */
}
