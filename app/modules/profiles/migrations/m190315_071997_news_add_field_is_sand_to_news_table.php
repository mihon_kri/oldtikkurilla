<?php

use yii\db\Migration;

/**
 * Class m190315_071406_profiles_add_division_to_region_table
 */
class m190315_071997_news_add_field_is_sand_to_news_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%news}}', 'is_send', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%news}}', 'is_send');
    }

}
