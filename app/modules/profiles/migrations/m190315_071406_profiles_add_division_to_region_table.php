<?php

use yii\db\Migration;

/**
 * Class m190315_071406_profiles_add_division_to_region_table
 */
class m190315_071406_profiles_add_division_to_region_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%regions}}', 'division_name' , $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('{{%regions}}', 'division_name');
    }

}
