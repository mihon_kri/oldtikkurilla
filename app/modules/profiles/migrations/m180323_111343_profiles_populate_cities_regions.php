<?php

use yii\db\Migration;

/**
 * Class m180323_111343_profiles_populate_cities_regions
 */
class m180323_111343_profiles_populate_cities_regions extends Migration
{

    public function up()
    {
        $this->cleanData();
        $this->execute(file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'cities_ru.sql'));
    }

    public function down()
    {
        $this->cleanData();
    }

    private function cleanData()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $this->execute('delete from yz_cities');
        $this->execute('delete from yz_regions');
        $this->execute('delete from yz_countries');
    }
}
