<?php

use yii\db\Migration;

/**
 * Class m190227_115659_profiles_create_table_leaders_profiles
 */
class m190227_115659_profiles_create_table_leaders_profiles extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'ENGINE=InnoDB CHARSET=utf8';
        }

        $this->createTable('{{%leaders_profiles}}', [
            'id' => $this->primaryKey(),
            'leader_id' => $this->integer(),
            'profile_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);

        $this->addForeignKey('{{%fk-leaders_profiles-leaders}}',
            '{{%leaders_profiles}}', 'leader_id',
            '{{%leaders}}', 'id',
            'RESTRICT', 'CASCADE');

        $this->addForeignKey('{{%fk-leaders_profiles-profiles}}',
            '{{%leaders_profiles}}', 'profile_id',
            '{{%profiles}}', 'id',
            'RESTRICT', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('{{%fk-leaders_profiles-profiles}}', '{{%leaders_profiles}}');
        $this->dropForeignKey('{{%fk-leaders_profiles-leaders}}', '{{%leaders_profiles}}');

        $this->dropTable('{{%leaders_profiles}}');
    }
}
