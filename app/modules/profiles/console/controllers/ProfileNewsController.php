<?php
namespace modules\profiles\console\controllers;

use console\base\Controller;
use modules\profiles\common\models\Profile;
use ms\loyalty\news\common\models\News;
use yz\admin\mailer\common\lists\ManualMailList;
use yz\admin\mailer\common\models\Mail;

class ProfileNewsController extends Controller
{
    /*Отправка уведомлений на e-mail о публикации новости*/
    public function actionIndex(){
        $noSendNews = News::find()->where(['is_send' => false])-> asArray()->all();
        $profileToSend = Profile::find()
            ->select('email')
            ->where(['not', ['checked_at' => null]])
            ->andWhere(['is', 'blocked_at' ,  null])
            ->asArray()
            ->all();
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $mailArr = [];
            foreach ($profileToSend as $mail) {
                $mailArr[] = $mail['email'];
            }
            if(!empty($noSendNews)) {
                foreach ($noSendNews as $new) {
                    $futerMail = "<p>Для просмотра данной новости пройдите по <a href='" . getenv("FRONTEND_SPA") . "/publication/" . $new['id'] . "'>данной ссылке</a></p>";
                    $adminMails = new Mail();
                    $adminMails->status = Mail::STATUS_WAITING;
                    $adminMails->receivers_provider = ManualMailList::className();
                    $adminMails->receivers_provider_data = '{"to": "' . implode(",", $mailArr) . '"}';
                    $adminMails->subject = $new['title'];
                    $adminMails->body_html = $new['content'] . $futerMail;
                    $adminMails->created_at = date("Y-m-d H:i:s");
                    $adminMails->save(false);
                    $sendingNews = News::findOne($new['id']);
                    $sendingNews->is_send = true;
                    $sendingNews->update(false);
                    $adminMails->updateAttributes(['status' => Mail::STATUS_WAITING]);
                }
            }
            $transaction->commit();
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $e->getMessage();
        }

    }
}