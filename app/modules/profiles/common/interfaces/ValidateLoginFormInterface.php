<?php

namespace modules\profiles\common\interfaces;

use modules\profiles\common\models\Profile;

interface ValidateLoginFormInterface
{
    /**
     * @param $form
     * @return Profile|string|boolean Profile if correct, string if custom error text, false if wrong login/password
     */
    public static function validateLoginPassword($form);
}