<?php

namespace modules\profiles\common\traits;

use Yii;
use yii\db\ActiveRecord;
use libphonenumber\PhoneNumberFormat;
use marketingsolutions\phonenumbers\PhoneNumber;
use modules\profiles\frontend\forms\LoginForm;
use modules\profiles\common\managers\RoleManager;

trait JwtLoginPhoneOrClubcardPasshashTrait
{
    /**
     * @param LoginForm $form
     * @return bool|string|ActiveRecord
     * @throws \Exception
     */
    public static function validateLoginPassword($form)
    {
        $login = $form->login;
        $password = $form->password;

        if (false == is_subclass_of(self::class, ActiveRecord::class)) {
            throw new \Exception('Class have to extend ActiveRecord');
        }

        if ($form->role === RoleManager::ROLE_REGULAR) {
            if (PhoneNumber::validate($login, 'RU') == false) {
                return 'Неверно указан номер телефона';
            }

            $model = self::findOne([
                'phone_mobile' => PhoneNumber::format($login, PhoneNumberFormat::E164, 'RU'),
                'role' => RoleManager::ROLE_REGULAR,
            ]);

            if (null === $model) {
                return "Номер $login не участвует в программе";
            }
        }

        if ($form->role === RoleManager::ROLE_PROFCLUB) {
            $model = self::findOne([
                'clubcard' => $form->login,
                'role' => RoleManager::ROLE_PROFCLUB,
            ]);

            if (null === $model) {
                return "Клубная карта указана неверно";
            }
        }

        /** @var ActiveRecord $model */
        if ($model->hasAttribute('registered_at') && $model->registered_at == null) {
            return 'Вы должны сперва зарегистрироваться в программе';
        }

        if (false == $model->hasAttribute('passhash')) {
            throw new \Exception('"passhash" field is missing');
        }

        if (Yii::$app->getSecurity()->validatePassword($password, $model->passhash)) {
            return $model;
        }

        return null;
    }
}