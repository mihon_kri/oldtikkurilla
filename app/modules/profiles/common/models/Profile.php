<?php

namespace modules\profiles\common\models;

use modules\profiles\backend\rbac\Rbac;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use yz\admin\mailer\common\lists\ManualMailList;
use yz\admin\mailer\common\models\Mail;
use yz\admin\models\AuthAssignment;
use yz\admin\models\User;
use yz\interfaces\ModelInfoInterface;
use yii\web\IdentityInterface;
use yii\db\Query;
use libphonenumber\PhoneNumberFormat;
use marketingsolutions\datetime\DateTimeBehavior;
use marketingsolutions\phonenumbers\PhoneNumber;
use marketingsolutions\finance\models\Purse;
use marketingsolutions\finance\models\PurseInterface;
use marketingsolutions\finance\models\PurseOwnerInterface;
use marketingsolutions\finance\models\PurseOwnerTrait;
use marketingsolutions\phonenumbers\PhoneNumberBehavior;
use ms\loyalty\api\common\interfaces\SetPasswordInterface;
use ms\loyalty\api\common\models\Jwt;
use ms\loyalty\api\common\models\Log;
use ms\loyalty\api\common\traits\JwtProfileTrait;
use ms\loyalty\api\common\traits\SetPasswordTrait;
use ms\loyalty\contracts\identities\IdentityRoleInterface;
use ms\loyalty\contracts\prizes\PrizeRecipientInterface;
use ms\loyalty\contracts\profiles\HasEmail;
use ms\loyalty\contracts\profiles\HasEmailInterface;
use ms\loyalty\contracts\profiles\HasPhoneMobile;
use ms\loyalty\contracts\profiles\HasPhoneMobileInterface;
use ms\loyalty\contracts\profiles\ProfileInterface;
use ms\loyalty\contracts\profiles\UserName;
use ms\loyalty\contracts\profiles\UserNameInterface;
use ms\loyalty\taxes\common\models\Account;
use ms\loyalty\taxes\common\models\AccountProfile;
use modules\profiles\common\managers\RoleManager;
use modules\profiles\common\interfaces\ValidateLoginFormInterface;
use modules\profiles\common\traits\JwtLoginPhoneOrClubcardPasshashTrait;
use modules\sales\common\models\Sale;

/**
 * This is the model class for table "yz_profiles".
 *
 * @property integer $id
 * @property string $passhash
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $full_name
 * @property string $phone_mobile
 * @property string $email
 * @property string $created_at
 * @property string $updated_at
 * @property string $registered_at
 * @property string $birthday_on
 * @property string $role
 * @property string $specialty
 * @property string $gender
 * @property string $work_place
 * @property string $avatar
 * @property string $document
 * @property string $blocked_at
 * @property string $blocked_reason Reason of the user blocked
 * @property string $banned_at
 * @property string $banned_reason Reason of the user banned
 * @property integer $dealer_id
 * @property integer $city_id
 * @property integer $region_id
 * @property string $phone_confirmed_at
 * @property string $email_confirmed_at
 * @property string $uniqid
 * @property string $pers_at
 * @property string $checked_at
 * @property integer $external_id
 * @property string $external_token
 * @property integer $clubcard
 * @property integer $is_checked
 * @property integer $is_blocked
 * @property integer $is_banned
 *
 * @property string $phone_mobile_local
 * @property Dealer $dealer
 * @property City $city
 * @property Region $region
 * @property string $avatarUrl
 * @property string $dealer_name
 * @property boolean $confirmed
 * @property integer $age
 * @property RoleManager $roleManager
 * @property Sale[] $sales
 */
class Profile extends \yz\db\ActiveRecord implements IdentityInterface, \ms\loyalty\contracts\identities\IdentityInterface, ModelInfoInterface,
                                                     HasEmailInterface, HasPhoneMobileInterface, UserNameInterface, IdentityRoleInterface,
                                                     PurseOwnerInterface, PrizeRecipientInterface, ProfileInterface,
                                                     ValidateLoginFormInterface, SetPasswordInterface
{
    use HasEmail, HasPhoneMobile, UserName, PurseOwnerTrait;
    use JwtProfileTrait, JwtLoginPhoneOrClubcardPasshashTrait, SetPasswordTrait;

    const GENDER_MALE = 'male';
    const GENDER_FEMALE = 'female';

    const EVENT_AFTER_LOGIN = 'event_login';
    const EVENT_AFTER_REGISTER = 'event_register';

    /*
     * @var RoleManager
     */
    private $_roleManager;

    public $avatar_local;
    public $city_local = null;
    public $dealer_name;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%profiles}}';
    }

    public static function getCheckedOptions()
    {
        return [
            '1' => 'Проверен',
            '0' => 'Не проверен',
        ];
    }

    public static function genderOptions()
    {
        return [
            self::GENDER_MALE => 'Мужской',
            self::GENDER_FEMALE => 'Женский',
        ];
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Профиль участника';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Профили участников';
    }

    /**
     * Returns purse's owner by owner's id
     *
     * @param int $id
     * @return $this
     */
    public static function findPurseOwnerById($id)
    {
        return static::findOne($id);
    }

    protected static function purseOwnerType()
    {
        return self::class;
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
            'phonenumber' => [
                'class' => PhoneNumberBehavior::class,
                'attributes' => [
                    'phone_mobile_local' => 'phone_mobile',
                ],
                'defaultRegion' => 'RU',
            ],
            'datetime' => [
                'class' => DateTimeBehavior::class,
                'performValidation' => false,
                'attributes' => [
                    'birthday_on' => [
                        'targetAttribute' => 'birthday_on_local',
                        'originalFormat' => ['date', 'yyyy-MM-dd'],
                        'targetFormat' => ['date', 'dd.MM.yyyy'],
                    ],
                ]
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['passhash', 'string', 'max' => 255],

            [['first_name', 'last_name', 'middle_name'], 'string', 'max' => 255],

            ['full_name', 'string', 'max' => 255],
            ['work_place', 'safe'],
            ['dealer_name', 'string'],

            ['email', 'string', 'max' => 64],
            ['email', 'email'],
            ['email', 'unique'],

            ['phone_mobile', 'string', 'max' => 16],
            ['phone_mobile_local', 'required'],
            ['phone_mobile_local', 'unique', 'targetAttribute' => ['phone_mobile' => 'phone_mobile']],

            ['dealer_id', 'integer'],
            ['is_banned', 'integer'],
            ['is_blocked', 'integer'],
            ['is_checked', 'integer'],

            ['city_id', 'integer'],
            ['city_local', 'checkCity'],

            ['region_id', 'integer'],

            ['gender', 'string'],

            ['avatar', 'string'],

            ['document', 'string'],

            ['role', 'string'],

            ['specialty', 'string'],

            ['avatar_local', 'file', 'extensions' => ['png', 'jpg', 'jpeg', 'gif', 'ico'], 'maxSize' => 1024 * 1024 * 15],

            ['blocked_at', 'string'],
            ['blocked_reason', 'string'],

            ['banned_at', 'string'],
            ['banned_reason', 'string'],

            ['birthday_on', 'safe'],
            ['birthday_on_local', 'safe'],

            ['uniqid', 'string', 'max' => 50],

            [['email_confirmed_at', 'phone_confirmed_at', 'checked_at', 'registered_at'], 'safe'],

            ['external_token', 'string', 'max' => 255],

            ['pers_at', 'string'],

            ['clubcard', 'integer'],
            ['clubcard', 'unique'],

            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'passhash' => 'Хэш пароля',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'middle_name' => 'Отчество',
            'full_name' => 'Полное имя',
            'phone_mobile' => 'Номер телефона',
            'phone_mobile_local' => 'Номер телефона',
            'email' => 'Email',
            'created_at' => 'Дата добавления',
            'updated_at' => 'Дата изменения',
            'registered_at' => 'Дата регистрации',
            'city_id' => 'Город',
            'city_local' => 'Город',
            'region_id' => 'Регион',
            'dealer_id' => 'Место работы',
            'work_place' => 'Место работы',
            'gender' => 'Пол',
            'role' => 'Роль',
            'specialty' => 'Должность',
            'avatar' => 'Аватар',
            'avatar_local' => 'Аватар',
            'document' => 'Документы',
            'blocked_at' => 'Заблокирован',
            'blocked_reason' => 'Причина блокировки',
            'banned_at' => 'Забанен',
            'banned_reason' => 'Причина бана',
            'birthday_on' => 'Дата рождения',
            'birthday_on_local' => 'Дата рождения',
            'email_confirmed_at' => 'Подтвердил e-mail',
            'phone_confirmed_at' => 'Подтвердил номер телефона',
            'checked_at' => 'Дата проверки участника',
            'pers_at' => 'Дата согласия на обработку персональных данных',
            'external_id' => 'Внешний ID',
            'external_token' => 'Внешний токен',
            'clubcard' => 'Номер клубной карты',
        ];
    }

    public function fields()
    {
        $fields = [
            'profile_id' => 'id',
            'clubcard',
            'full_name',
            'first_name',
            'last_name',
            'middle_name',
            'role',
            'gender',
            'city_id',
            'city_local' => function (Profile $model) {
                if ($city = $model->city) {
                    if ($region = $city->region) {
                        return implode(', ', [$city->title, $region->title]);
                    }
                    else {
                        return $city->title;
                    }
                }
                return null;
            },
            'city',
            'dealer',
            'specialty',
            'avatar_url' => 'avatarUrl',
            'phone_mobile',
            'email',
            'balance' => function (Profile $model) {
                $purse = $model->purse;
                return $purse ? $purse->balance : null;
            },
            'balance_total' => function (Profile $model) {
                return (new ProfileRepository($model))->getBonuses();
            },
            'birthday_on' => function (Profile $model) {
                return empty($model->birthday_on) ? null : (new \DateTime($model->birthday_on))->format('d.m.Y');
            },
            'registered_at',
            'work_place',
            'checked_at',
            'pers_at',
            'created_at',
            'blocked_at',
            'blocked_reason',
            'banned_at',
            'banned_reason',
            'account' => function (Profile $model) {
                /** @var Account $account */
                if ($account = Account::findOne(['profile_id' => $model->id])) {
                    /** @var AccountProfile $accountProfile */
                    if ($accountProfile = AccountProfile::findOne(['account_id' => $account->id])) {
                        return $accountProfile->toArray(['status', 'status_label']);
                    }
                }
                return null;
            },
        ];

        return $fields;
    }

    /**
     * @return string
     */
    public function getIdentityRole()
    {
        return 'profile';
    }

    /**
     * Returns id of the recipient
     *
     * @return integer
     */
    public function getRecipientId()
    {
        return $this->getPrimaryKey();
    }

    public function getUserFullName()
    {
        return $this->full_name;
    }

    /**
     * Returns purce for the recipient, that should contain enough money
     *
     * @return PurseInterface
     */
    public function getRecipientPurse()
    {
        return $this->purse;
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     *
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     *
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        return null;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     *
     * @param string $authKey the given auth key
     * @return boolean whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        return false;
    }

    /**
     * @return integer
     */
    public function getProfileId()
    {
        return $this->getPrimaryKey();
    }

    public function getProfile()
    {
        return $this;
    }

    public function beforeSave($insert)
    {
        $this->full_name = $this->last_name . ' ' . $this->first_name . ' ' . $this->middle_name;

        if (!empty($this->birthday_on)) {
            $this->birthday_on = (new \DateTime($this->birthday_on))->format('Y-m-d H:i:s');
        }

        if ($this->roleManager->isProfclub() && $this->clubcard === null) {
            $this->clubcard = $this->generateClubCard();
        }

        return parent::beforeSave($insert);
    }

    public function newUserSendMail()
    {
        //адреса ответственных за  регионы
        $leaderMail = self::find()
            ->select('yz_leaders.email')
            ->leftJoin('{{%cities}}', '{{%cities}}.id={{%profiles}}.city_id')
            ->leftJoin('{{%leader_admin_region}}', '{{%leader_admin_region}}.region_id={{%cities}}.region_id')
            ->leftJoin('{{%leaders}}', '{{%leaders}}.id={{%leader_admin_region}}.leader_id')
            ->distinct()
            ->column();
        //адреса главных администраторов
        $superadminMai = User::find()
            ->select('{{%admin_users}}.email')
            ->leftJoin('{{%admin_auth_assignment}}', '{{%admin_auth_assignment}}.user_id={{%admin_users}}.id')
            ->where(['{{%admin_auth_assignment}}.item_name' => Rbac::ROLE_COOL_ADMIN ])
            ->distinct()
            ->column();
        $toCoolAdmin = implode(";", $superadminMai);
        $toCoolAdmin = trim($toCoolAdmin, ";");
        if(!empty($leaderMail)){
            $newMail = new Mail();
            $newMail->status = Mail::STATUS_WAITING;
            $newMail->receivers_provider = ManualMailList::class;
            $to = implode(";", $leaderMail);
            $to = trim($to, ';');
            if(!empty($toCoolAdmin)){
                $toSend = ['to' => $to.";".$toCoolAdmin];
            }else{
                $toSend = ['to' => $to];
            }
            $newMail->receivers_provider_data = json_encode($toSend);
            $newMail->subject = 'На портале зарегистрировался новый пользователь из Вашего региона - '.$this->full_name;
            $message = "<h4>Здравствуйте!</h4>";
            $message .= "<p>На портале зарегистрировался новый пользователь из Вашего региона - ".$this->full_name."</p>";
            $message .= "Для просмотра данных по новому участнику пройдите по этой <a target='_blank' href='https://admin-tikkurila.msforyou.ru/profiles/profiles/index'>ссылке</a>";
            $newMail->body_html = $message;
            $newMail->created_at = date("Y-m-d H:i:s");
            $newMail->save(false);
            $newMail->updateAttributes(['status' => Mail::STATUS_WAITING]);
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $this->uniqid = uniqid();
            $this->createPurse();
            $this->newUserSendMail();
        }

        if (!$insert && $this->isAttributeChanged('full_name')) {
            $this->updatePurse();
        }
        if (!$insert && $this->dealer_name) {
            $dealer = Dealer::findOne($this->dealer_id);
            if($dealer){
                $dealer->name = $this->dealer_name;
                $dealer->update(false);
            }
        }

        $this->upload(['avatar']);

        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeDelete()
    {
        return parent::beforeDelete();
    }

    public function afterDelete()
    {
        Purse::remove(self::class, $this->id);

        parent::afterDelete();
    }

    public function getRoleManager()
    {
        if ($this->_roleManager === null) {
            $this->_roleManager = new RoleManager($this);
        }

        return $this->_roleManager;
    }

    /**
     * @return Jwt
     * @throws \Exception
     */
    public function generateJwtToken()
    {
        /** @var Jwt $jwt */
        if (null === ($jwt = Jwt::findOne(['profile_id' => $this->id]))) {
            $jwt = new Jwt();
            $jwt->profile_id = $this->id;
            $jwt->login = $this->phone_mobile;
            $jwt->token = Jwt::generateToken($this->id);
            $jwt->logged_at = (new \DateTime)->format('Y-m-d H:i:s');
            $jwt->logged_ip = Log::getIp();
            $jwt->save(false);
            $jwt->refresh();
        }

        return $jwt;
    }

    public function getAvatarUrl()
    {
        return empty($this->avatar)
            ? Yii::getAlias('@frontendWeb/images/avatar_blank.png') . "?v=2"
            : Yii::getAlias('@frontendWeb/data/photos/' . $this->avatar);
    }

    public function getAge()
    {
        $now = new \DateTime('now');
        $birthday = new \DateTime($this->birthday_on);
        $diff = $birthday->diff($now);

        return intval($diff->y);
    }

    public function createPurse()
    {
        Purse::create(self::class, $this->id, strtr('Счет пользователя #{id} ({name})', [
            '{id}' => $this->id,
            '{name}' => $this->full_name,
        ]));
    }

    protected function updatePurse()
    {
        $this->purse->updateAttributes([
            'title' => strtr('Счет пользователя #{id} ({name})', [
                '{id}' => $this->id,
                '{name}' => $this->full_name,
            ]),
        ]);
    }

    public function block($reason = null)
    {
        $this->updateAttributes([
            'blocked_at' => new Expression('NOW()'),
            'blocked_reason' => $reason,
        ]);
    }

    /**
     * Unblock profile from be able to login
     */
    public function unblock()
    {
        $this->updateAttributes([
            'blocked_at' => null,
            'blocked_reason' => null,
        ]);
    }

    public function ban($reason)
    {
        $this->updateAttributes([
            'banned_at' => new Expression('NOW()'),
            'banned_reason' => $reason,
        ]);
    }

    public function unban()
    {
        $this->updateAttributes([
            'banned_at' => null,
            'banned_reason' => null,
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDealer()
    {
        return $this->hasOne(Dealer::class, ['id' => 'dealer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::class, ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSales()
    {
        return $this->hasMany(Sale::class, ['recipient_id' => 'id']);
    }

    public function upload(array $fields)
    {
        foreach ($fields as $field) {
            $file = UploadedFile::getInstance($this, $field . '_local');

            if ($file instanceof UploadedFile) {
                $dir = Yii::getAlias("@data/photo");
                FileHelper::createDirectory($dir);
                $name = "{$this->id}_$field.{$file->extension}";
                $path = Yii::getAlias($dir . DIRECTORY_SEPARATOR . $name);
                $file->saveAs($path);
                $this->$field = $name;
                $this->updateAttributes([$field]);
            }
        }
    }

    public function updateRegisteredAt()
    {
        $this->updateTimeNow('registered_at');
    }

    public function updatePersAt()
    {
        $this->updateTimeNow('pers_at');
    }

    public function setPasshash($password)
    {
        $this->updateAttributes(['passhash' => \Yii::$app->security->generatePasswordHash($password)]);
    }

    public function confirmPhone()
    {
        $this->updateTimeNow('phone_confirmed_at');
    }

    public function confirmEmail()
    {
        $this->updateTimeNow('email_confirmed_at');
    }

    public static function generateRandomPassword($length = 5, $num = false)
    {
        $chars = $num ? '1234567890' : 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
        $numChars = strlen($chars);
        $string = '';

        for ($i = 0; $i < $length; $i++) {
            $string .= substr($chars, rand(1, $numChars) - 1, 1);
        }
        return $string;
    }

    public function isMale()
    {
        return $this->gender == self::GENDER_MALE;
    }

    public function checkCity()
    {
        if (!empty($this->city_local)) {
            $titles = explode(',', $this->city_local);
            $city = trim($titles[0]);
            $region = null;

            if (isset($titles[1])) {
                $region = trim($titles[1]);
            }

            $query = (new Query())
                ->select('c.id')
                ->from(['c' => City::tableName()])
                ->leftJoin(['r' => Region::tableName()], 'r.id = c.region_id')
                ->where("c.title LIKE :cityName", [':cityName' => $city . '%'])
                ->limit(1);

            if ($region) {
                $query->andWhere("r.title LIKE :region", [':region' => $region . '%']);
            }

            $result = $query->one();

            if (empty($result)) {
                $this->addError('city_local', 'Город не найден');
            }
            else {
                $this->city_id = $result['id'];
            }
        }
        else {
            $this->addError('city_local', 'Пожалуйста, укажите свой город');
        }
    }

    public function checkPhone()
    {
        $phone = $this->phone_mobile_local;
        if (PhoneNumber::validate($phone, 'RU') == false) {
            $this->addError('phone_mobile_local', 'Неверный формат номера телефона');
            return;
        }

        $phone = PhoneNumber::format($phone, PhoneNumberFormat::E164, 'RU');

        if (Profile::findOne(['phone_mobile' => $phone])) {
            $this->addError('phone_mobile_local', 'Данный номер телефона уже используется');
        }
    }

    public function getDocuments()
    {
        $documents = empty($this->document) ? [] : explode(';', $this->document);
        $urls = [];
        $baseUrl = Yii::getAlias("@frontendWeb/data/profile-documents/");
        for ($i = 0; $i < count($documents); $i++) {
            $urls[] = $baseUrl . $documents[$i];
        }
        return $urls;
    }

    public function addDocument($filename)
    {
        $documents = empty($this->document) ? [] : explode(';', $this->document);
        $documents[] = $filename;
        $this->document = implode(';', $documents);
        $this->save(false);
    }

    public function removeDocument($filename)
    {
        $path = Yii::getAlias("@frontendWebroot/data/profile-documents/$filename");
        if (file_exists($path)) {
            @unlink($path);
        }

        $documents = empty($this->document) ? [] : explode(';', $this->document);
        $documents = array_diff($documents, [$filename]);
        $this->document = implode(';', $documents);
        $this->save(false);
    }

    /**
     * @param Leader|null $leader
     * @return array
     */
    public static function getOptions(Leader $leader = null)
    {
        $query = self::find()
            ->select([
                "CONCAT(full_name, ' (', phone_mobile, ')')",
                "id",
            ])
            ->orderBy(['full_name' => SORT_ASC])
            ->indexBy('id');

        if ($leader) {
            $ids = $leader->roleManager->getProfileIds();
            $query->andWhere(['IN', 'id', $ids]);
        }

        return $query->column();
    }

    /**
     * Список регионов
     * @return array
     */
    public static function getRegions()
    {
        $query = Region::find()
            ->select(['title', 'id'])
            ->orderBy(['title' => SORT_ASC])
            ->indexBy('id');
        return $query->column();
    }

    /**
     * Список регионов для уже добавленного лидера с ролью Администратор
     * @param $leaderId
     * @return array
     */
    public static function getLeaderRegions($leaderId)
    {
        $regionId = LeaderAdminRegion::find()->select('region_id')->where(['leader_id'=> $leaderId])->column();
        return Region::find()
            ->select(['title', 'id'])
            ->where(['id' => $regionId])
            ->orderBy(['title' => SORT_ASC])
            ->indexBy('id')
            ->column();
    }

    private function updateTimeNow($attribute)
    {
        $now = (new \DateTime())->format('Y-m-d H:i:s');
        $this->updateAttributes([$attribute => $now]);
    }

    /**
     * @return int Returns unique club card number
     */
    public function generateClubCard()
    {
        $codes = static::find()
            ->select('clubcard')
            ->where(['IS NOT', 'clubcard', new Expression('NULL')])
            ->indexBy('clubcard')
            ->column();

        do {
            $code = mt_rand(10000, 99999);
        } while (isset($codes[$code]));

        return $code;
    }

    public static function isAccessToManualPay()
    {
        $isAdd = AuthAssignment::findOne(['user_id' => \Yii::$app->user->id]);
        if($isAdd && $isAdd->item_name == 'COOL_ADMIN'){
            return true;
        }
        return false;
    }

    public static function updateFilterParams()
    {
        $profiles = self::find()
            ->asArray()
            ->all();
        foreach ($profiles as $profile){
            if(($profile['checked_at'] && !$profile['is_checked']) || (!$profile['checked_at'] && $profile['is_checked'])){
                $addFilterField = self::findOne($profile['id']);
                if($profile['checked_at']) {
                    $addFilterField->is_checked = true;
                }else{
                    $addFilterField->is_checked = false;
                }
                $addFilterField->update(false);
            }
            if(($profile['blocked_at'] && !$profile['is_blocked']) || (!$profile['blocked_at'] && $profile['is_blocked'])){
                $addFilterField = self::findOne($profile['id']);
                if($profile['blocked_at']) {
                    $addFilterField->is_blocked = true;
                }else{
                    $addFilterField->is_blocked = false;
                }
                $addFilterField->update(false);
            }

            if(($profile['banned_at'] && !$profile['is_banned']) || (!$profile['banned_at'] && $profile['is_banned'])){
                $addFilterField = self::findOne($profile['id']);
                if($profile['banned_at']) {
                    $addFilterField->is_banned = true;
                }else{
                    $addFilterField->is_banned = false;
                }
                $addFilterField->update(false);
            }
        }
    }
}
