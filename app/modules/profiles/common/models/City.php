<?php

namespace modules\profiles\common\models;

use modules\profiles\backend\rbac\Rbac;
use Yii;
use yii\db\Query;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_cities".
 *
 * @property integer $id
 * @property string $title
 * @property integer $region_id
 * @property integer $country_id
 *
 * @property Region $region
 * @property Country $country
 * @property Profile[] $profiles
 */
class City extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cities}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Город';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'города';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'filter', 'filter' => 'trim'],
            ['title', 'string', 'max' => 255],
            ['title', 'required'],
            ['title', 'unique'],

            ['region_id', 'integer'],

            ['country_id', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Город',
            'region_id' => 'Регион',
            'country_id' => 'Страна',
        ];
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        return [
            'id',
            'title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::class, ['id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::class, ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfiles()
    {
        return $this->hasMany(Profile::class, ['city_id' => 'id']);
    }

    /**
     * @param Leader|null $leader
     * @return array
     */
    public static function getTitleOptions(Leader $leader = null)
    {
        $cityIdForAll = Profile::find()->select('city_id')->distinct()->column();
        $query = self::find()
            ->indexBy('id')
            ->select('title, id')
            ->where(['id' => $cityIdForAll])
            ->orderBy(['title' => SORT_ASC]);

        if ($leader && $leader->role == Rbac::ROLE_ADMIN_JUNIOR) {
            $query->andWhere(['id' =>$leader->getAdminProfiles() ]);
        }

        return $query->column();
    }

    public static function getOptions()
    {
        $raw = (new Query)
            ->select(['c.id', 'city' => 'c.title', 'region' => 'r.title'])
            ->from(['c' => City::tableName()])
            ->leftJoin(['r' => Region::tableName()], 'r.id = c.region_id')
            ->orderBy(['c.title' => SORT_ASC])
            ->all();

        $options = [];

        foreach ($raw as $r) {
            $key = $r['id'];
            $options[$key] = $r['city'] . ' (' . $r['region'] . ')';
        }

        return $options;
    }
}
