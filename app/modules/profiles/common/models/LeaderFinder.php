<?php

namespace modules\profiles\common\models;

use yz\admin\contracts\ProfileFinderInterface;

class LeaderFinder implements ProfileFinderInterface
{
    /**
     * @param $id
     * @return Leader|null
     */
    public static function findByIdentityId($id)
    {
        return Leader::findOne(['identity_id' => $id]);
    }
}