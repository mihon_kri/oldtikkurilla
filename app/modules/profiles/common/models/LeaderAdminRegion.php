<?php

namespace modules\profiles\common\models;

use Yii;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_leader_admin_region".
 *
 * @property integer $id
 * @property integer $leader_id
 * @property integer $region_id
 */
class LeaderAdminRegion extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%leader_admin_region}}';
    }

	/**
     * Returns model title, ex.: 'Person', 'Book'
     * @return string
     */
    public static function modelTitle()
    {
        return 'Leader Admin Region';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Leader Admin Regions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['leader_id', 'integer'],
            ['region_id', 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leader_id' => 'Leader ID',
            'region_id' => 'Region ID',
        ];
    }
}
