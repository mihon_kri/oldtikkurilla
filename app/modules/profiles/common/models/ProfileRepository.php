<?php

namespace modules\profiles\common\models;

use yii\db\ActiveQuery;
use modules\sales\common\models\SaleAction;
use modules\sales\common\sales\statuses\Statuses;

class ProfileRepository
{
    /**
     * @var Profile
     */
    private $_profile;

    /**
     * @param Profile $profile
     */
    public function __construct(Profile $profile)
    {
        $this->_profile = $profile;
    }

    /**
     * Total bonuses sum for approved and paid sales
     * With not available for spending bonuses
     * @return integer
     */
    public function getBonuses()
    {
        $query = SaleAction::find()
            ->select('SUM({{%sales_actions}}.bonuses) AS bonuses')
            ->innerJoinWith(['sale' => function (ActiveQuery $query) {
                $query->innerJoinWith(['profile']);
            }])
            ->where([
                'AND',
                ['{{%profiles}}.id' => $this->_profile->getProfileId()],
                ['IN', '{{%sales}}.status', [Statuses::APPROVED, Statuses::PAID]],
            ]);

        return (int) $query->sum('{{%sales_actions}}.bonuses');
    }
}