<?php

namespace modules\profiles\common\models;

use modules\profiles\backend\rbac\Rbac;
use Yii;
use yz\interfaces\ModelInfoInterface;

/**
 * This is the model class for table "yz_regions".
 *
 * @property integer $id
 * @property string $title
 * @property string $name
 * @property string $division_name
 * @property integer $country_id
 *
 * @property Country $country
 */
class Region extends \yii\db\ActiveRecord implements ModelInfoInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%regions}}';
    }

    /**
     * Returns model title, ex.: 'Person', 'Book'
     *
     * @return string
     */
    public static function modelTitle()
    {
        return 'Регион';
    }

    /**
     * Returns plural form of the model title, ex.: 'Persons', 'Books'
     *
     * @return string
     */
    public static function modelTitlePlural()
    {
        return 'Регионы';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'filter', 'filter' => 'trim'],
            ['title', 'string', 'max' => 255],
            ['division_name', 'string', 'max' => 255],
            ['title', 'required'],
            ['title', 'unique'],

            ['name', 'string', 'max' => 255],

            ['country_id', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Регион',
            'name' => 'Название региона',
            'division_name' => 'Дивизион',
            'country_id' => 'Страна',
        ];
    }

    /**
     * @param Leader|null $leader
     * @return array
     */
    public static function getOptions(Leader $leader = null)
    {
        $regionIdForAll = Profile::find()->select('{{%cities}}.region_id')->leftJoin('{{%cities}}', '{{%profiles}}.city_id={{%cities}}.id')->distinct()->column();
        $query = self::find()
            ->indexBy('id')
            ->select('title, id')
            ->where(['id' => $regionIdForAll])
            ->orderBy(['title' => SORT_ASC]);

        if ($leader &&  $leader->role == Rbac::ROLE_ADMIN_JUNIOR) {
            $regionId = LeaderAdminRegion::find()->select('region_id')->where(['leader_id' => $leader->id])->column();
            $query->andWhere(['id' => $regionId]);
        }

        return $query->column();
    }

    public static function getNameOptions()
    {
        return self::find()->indexBy('name')->select('name, name')->orderBy(['name' => SORT_ASC])->groupBy('name')->column();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::class, ['id' => 'country_id']);
    }
}
