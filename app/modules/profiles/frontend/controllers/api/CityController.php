<?php

namespace modules\profiles\frontend\controllers\api;

use Yii;
use ms\loyalty\api\frontend\base\ApiController;
use modules\profiles\common\models\City;

class CityController extends ApiController
{
    /**
     * @api {post} profiles/api/city/list Список городов
     * @apiDescription Получить список всех городов
     * @apiName ProfilesCityList
     * @apiGroup Profiles
     *
     * @apiParam {String} name Название города для фильтрации списка (необязательный параметр)
     *
     * @apiParamExample {json} Пример запроса:
     * {
     *   "title": "моск"
     * }
     *
     * @apiSuccess {String} result Статус ответа "OK"
     * @apiSuccess {Object} cities Список городов
     * @apiSuccess {Integer} cities.id Идентификатор
     * @apiSuccess {String} cities.title Название
     *
     * @apiSuccessExample {json} Пример успешного ответа:
     * {
     *   "result": "OK",
     *   "cities": [
     *     {
     *       "id": 1073,
     *       "title": "Абаза"
     *     },
     *     {
     *       "id": 1074,
     *       "title": "Абакан"
     *     },
     *     {
     *       "id": 2380,
     *       "title": "Абакан"
     *     }
     *   ]
     * }
     *
     */
    public function actionList()
    {
        $title = Yii::$app->request->post('title');

        $cities = City::find()
            ->orderBy(['title' => SORT_ASC]);

        if ($title) {
            $cities->andWhere(['LIKE', 'title', $title]);
        }

        $this->logResponse = false;

        return $this->ok(['cities' => $cities->limit(30)->all()], 'Получение списка городов');
    }
}