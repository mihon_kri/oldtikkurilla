<?php

namespace modules\profiles\frontend\controllers\api;

use Yii;
use libphonenumber\PhoneNumberFormat;
use ms\loyalty\api\frontend\base\ApiController;
use ms\loyalty\pages\common\models\Page;
use marketingsolutions\phonenumbers\PhoneNumber;
use modules\profiles\common\models\Profile;
use modules\profiles\common\managers\RoleManager;
use modules\profiles\frontend\forms\RegisterProfileProfClubForm;
use modules\profiles\frontend\forms\RegisterProfileRegularForm;

class RegisterController extends ApiController
{
    /**
     * @api {post} profiles/api/register/info Информация при регистрации
     * @apiDescription Возвращает информацию по регистрации участника
     * @apiName ProfilesRegistrationInfo
     * @apiGroup Profiles
     *
     * @apiParam {String} phone Мобильный телефон (необязательный параметр)
     *
     * @apiParamExample {json} Пример запроса:
     * {
     *   "phone": "+7 (915) 191-3583"
     * }
     *
     * @apiSuccess {String} result Статус ответа "OK"
     * @apiSuccess {Object} pageRules Правила акции
     * @apiSuccess {String} pageRules.url Адрес страницы
     * @apiSuccess {String} pageRules.title Заголовок
     * @apiSuccess {String} pageRules.content HTML содержимое
     * @apiSuccess {Integer} pageRules.hide_title
     * @apiSuccess {Object} pagePers Правила акции
     * @apiSuccess {String} pagePers.url Адрес страницы
     * @apiSuccess {String} pagePers.title Заголовок
     * @apiSuccess {String} pagePers.content HTML содержимое
     * @apiSuccess {Integer} pagePers.hide_title
     * @apiSuccess {Object} profile Профиль участника
     * @apiSuccess {Integer} profile.profile_id Индификаторв
     * @apiSuccess {String} profile.clubcard Номер клубной карты
     * @apiSuccess {String} profile.full_name Полное имя
     * @apiSuccess {String} profile.first_name Имя
     * @apiSuccess {String} profile.middle_name Отчество
     * @apiSuccess {String} profile.role Роль
     * @apiSuccess {String} profile.gender Пол
     * @apiSuccess {Integer} profile.city_id Индификатор города
     * @apiSuccess {String} profile.city_local Название города
     * @apiSuccess {Object} profile.city Торговая точка
     * @apiSuccess {Integer} profile.city.id Индификатор
     * @apiSuccess {String} profile.city.title Название
     * @apiSuccess {Object} profile.dealer Торговая точка
     * @apiSuccess {Integer} profile.dealer.id Индификатор
     * @apiSuccess {String} profile.dealer.title Название
     * @apiSuccess {String} profile.specialty
     * @apiSuccess {String} profile.avatar_url URL изобрадения аватарки
     * @apiSuccess {String} profile.phone_mobile Телефон
     * @apiSuccess {String} profile.email Email
     * @apiSuccess {Number} profile.balance Баланс (доступно для трат)
     * @apiSuccess {Number} profile.balance_total Подтвержденные баллы
     * @apiSuccess {String} profile.birthday_on День рождения
     * @apiSuccess {String} profile.registered_at Дата регистрации
     * @apiSuccess {String} profile.checked_at Дата подтверждения администратором
     * @apiSuccess {String} profile.pers_at Дата согласия на обработку персональных данных
     * @apiSuccess {String} profile.created_at Дата создания
     * @apiSuccess {String} profile.blocked_at Дата блокировки
     * @apiSuccess {String} profile.blocked_reason Причины блокировки
     * @apiSuccess {String} profile.banned_at Дата бана
     * @apiSuccess {String} profile.banned_reason Причины бана
     * @apiSuccess {String} profile.account НДФЛ анкета
     *
     * @apiSuccessExample {json} Пример успешного ответа:
     * {
     *   "result": "OK",
     *   "pageRules": {
     *     "url": "rules",
     *     "title": "Правила мотивационной программы",
     *     "content": "<p>Правила мотивационной программы. Заполняется из админки.</p>",
     *     "hide_title": 0
     *   },
     *   "pagePers": {
     *     "url": "pers",
     *     "title": "Согласие на использование персональных данных",
     *     "content": "<p>Согласие на использование персональных данных. Заполняется из админки.</p>",
     *     "hide_title": 0
     *   },
     *   "profile": {
     *     "profile_id": 1,
     *     "clubcard": null,
     *     "full_name": "Дмитрий Новиков Александрович",
     *     "first_name": "Новиков",
     *     "last_name": "Дмитрий",
     *     "middle_name": "Александрович",
     *     "role": "regular",
     *     "gender": null,
     *     "city_id": 1,
     *     "city_local": "Москва, Москва и Московская обл.",
     *     "city": {
     *       "id": 1,
     *       "title": "Москва"
     *     },
     *     "dealer": {
     *       "id": 1,
     *       "name": "Тестовая торговая точка 1"
     *     },
     *     "specialty": null,
     *     "avatar_url": "http://api.tikkurila.local/images/avatar_blank.png?v=2",
     *     "phone_mobile": "+79151913583",
     *     "email": "nd@msforyou.ru",
     *     "balance": 0,
     *     "balance_total": 0,
     *     "birthday_on": null,
     *     "registered_at": null,
     *     "checked_at": null,
     *     "pers_at": null,
     *     "created_at": "2019-02-01 11:52:52",
     *     "blocked_at": null,
     *     "blocked_reason": null,
     *     "banned_at": null,
     *     "banned_reason": null,
     *     "account": null
     *   }
     * }
     *
     */
    public function actionInfo()
    {
        $phone = Yii::$app->request->post('phone');
        $profile = null;

        if (!empty($phone) && PhoneNumber::validate($phone, 'RU')) {
            $phone = PhoneNumber::format($phone, PhoneNumberFormat::E164, 'RU');
            $profile = Profile::findOne(['phone_mobile' => $phone]);
        }

        $pageRules = Page::findOne(['url' => 'rules']);
        $pagePers = Page::findOne(['url' => 'pers']);

        return $this->ok(compact('pageRules', 'pagePers', 'profile'), 'Получение данных для регистрации');
    }

    /**
     * @api {post} profiles/api/register Регистрация участника
     * @apiDescription Регистрация нового участника<br><br>
     * У участников с ролью "profclub" закрытый вид регистрации, т.е. по заранее загруженным спискам.
     * Им не обязательно заполнять поля ФИО, Город, Email, т.к. они уже внесены в базу данных.<br><br>
     * У участников с ролью "regular" открытый вид регистрации, но есль участник не был заранее загружен по спискам в базу данных,
     * то его регистрация требует подтверждения администратора и функционал его ЛК ограничен.
     *
     * @apiName ProfilesRegistration
     * @apiGroup Profiles
     *
     * @apiParam {String} role Роль участника ("profclub" или "regular")
     * @apiParam {String} first_name Имя
     * @apiParam {String} last_name Фамилия
     * @apiParam {String} middle_name Отчество
     * @apiParam {String} phone Мобильный телефон
     * @apiParam {Integer} clubcard Номер клубной карты (для роли "profclub")
     * @apiParam {String} email Email
     * @apiParam {Integer} city_id Индификатор города
     * @apiParam {Integer} dealer_id Индификатор торговой точки
     * @apiParam {String} password Пароль
     * @apiParam {String} passwordConfirm Подтверждение пароля
     * @apiParam {Integer} checkedRules Согласие с правилами акции
     * @apiParam {Integer} checkedPers Согласие на обработку персональных данных
     * @apiParam {String} token Токен проверки телефона по СМС
     *
     * @apiParamExample {json} Пример запроса для роли "profclub":
     * {
     *   "role": "profclub",
     *   "phone": "+7 (915) 191-3581",
     *   "clubcard": 23062,
     *   "password": "123123",
     *   "passwordConfirm": "123123",
     *   "checkedRules": 1,
     *   "checkedPers": 1,
     *   "token": "9d90e96ca1472557caccbacf86a1a1cbdcdb033f9bd72bfcfe934ad8497c51f8"
     * }
     *
     * @apiParamExample {json} Пример запроса для роли "regular":
     * {
     *   "role": "regular",
     *   "first_name": "Тест",
     *   "last_name": "Тестов",
     *   "middle_name": "Тестович",
     *   "phone": "+7 (915) 191-3583",
     *   "email": "123@123.ru",
     *   "city_id": 1,
     *   "dealer_id": 1,
     *   "password": "123123",
     *   "passwordConfirm": "123123",
     *   "checkedPers": 1,
     *   "checkedRules": 1,
     *   "token": "f20f24b706ec7b0d4a6624516d2072de915edb93cbcedb5913c2e8ccecb59859"
     * }
     *
     * @apiSuccess {String} result Статус ответа "OK"
     * @apiSuccess {Number} profile_id Идентификатор участника
     * @apiSuccess {String} login Логин участника
     * @apiSuccess {String} token Токен проверки телефона по СМС
     * @apiSuccess {String} logged_at Дата и время последнего входа в систему
     * @apiSuccess {String} logged_ip IP адрес с которого последний раз заходил участник
     * @apiSuccess {String} created_at Дата создания профиля участника
     * @apiSuccess {String} created_at Дата обновления профиля участника
     * @apiSuccess {String} id
     * @apiSuccess {String} header
     * @apiSuccess {String} payload
     * @apiSuccess {String} secret
     * @apiSuccess {String} expires_at
     * @apiSuccess {String} used_at
     * @apiSuccess {String} used_ip
     *
     * @apiSuccessExample {json} Пример успешного ответа:
     * {
     *   "result": "OK",
     *   "profile_id": 10,
     *   "login": "+79151913583",
     *   "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcm9maWxlX2lkIjoxMH0.NDZlNDRhY2U3MGY4N2FlMDhiZGIxZGE2MDg5NTY1NTgyYmM4NjQyODdkM2ExMTA4ZDdmMDEzZjE4MjA4YTFmNA",
     *   "logged_at": "2019-01-31 19:23:02",
     *   "logged_ip": "192.168.7.1",
     *   "created_at": "2019-01-31 19:23:02",
     *   "updated_at": "2019-01-31 19:23:02",
     *   "id": 2,
     *   "header": null,
     *   "payload": null,
     *   "secret": null,
     *   "expires_at": null,
     *   "used_at": null,
     *   "used_ip": null
     *   }
     *
     */
    public function actionIndex()
    {
        $role = Yii::$app->request->post('role');

        if (in_array($role, [RoleManager::ROLE_PROFCLUB, RoleManager::ROLE_REGULAR]) === false) {
            return $this->error('Недопустимое значение роли участника');
        }

        if ($role === RoleManager::ROLE_PROFCLUB) {
            $model = new RegisterProfileProfClubForm;
        }

        if ($role === RoleManager::ROLE_REGULAR) {
            $model = new RegisterProfileRegularForm;
        }

        if ($model->load(Yii::$app->request->post(), '') && $model->process()) {
            return $this->ok($model->getProfile()->generateJwtToken()->toArray(), 'Успешная регистрация участника');
        }

        return $this->error($model->getFirstErrors(), 'Ошибка регистрации участника');
    }
}