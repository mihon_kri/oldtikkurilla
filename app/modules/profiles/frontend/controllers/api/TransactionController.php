<?php

namespace modules\profiles\frontend\controllers\api;

use modules\profiles\common\models\Profile;
use Yii;
use ms\loyalty\api\frontend\base\ApiController;
use marketingsolutions\finance\models\Purse;
use modules\profiles\frontend\models\ApiTransactions;

/**
 * Class TransactionController
 */
class TransactionController extends ApiController
{
    public function actionList()
    {
        $profile_id = Yii::$app->request->post('profile_id', null);

        /** @var Profile $profile */
        $profile = Profile::findOne($profile_id);

        if(null === $profile) {
            return $this->error("Не найден участник с ID $profile_id");
        }

        $purse = Purse::findOne(['owner_id' => $profile->id]);

        if(null === $purse) {
            return $this->error("Не найден кошелёк участника с ID $profile_id");
        }

        /** @var ApiTransactions[] $transactions */
        $transactions = ApiTransactions::find()
            ->where(['purse_id' => $purse->id])
            ->orderBy(['created_at' => SORT_DESC])
            ->all();

        $full_name= $profile->full_name;

        if (!empty($transactions)) {
            foreach ($transactions as $transaction) {
                $transaction->title = str_replace("\"{$full_name}\" ", '', $transaction->title);
                $transaction->title = str_replace("{$full_name} ", '', $transaction->title);
                $transaction->title = str_replace("{$full_name}", '', $transaction->title);
            }
        }

        return $this->ok(['transactions' => $transactions], 'Получение истории транзакций участника');
    }
}