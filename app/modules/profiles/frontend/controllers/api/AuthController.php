<?php

namespace modules\profiles\frontend\controllers\api;

use Yii;
use yii\db\Query;
use ms\loyalty\api\common\models\Log;
use ms\loyalty\api\frontend\base\ApiController;
use modules\profiles\common\models\Profile;
use modules\profiles\common\models\City;
use modules\profiles\common\models\Region;
use modules\profiles\frontend\models\ApiAvatar;
use modules\profiles\frontend\forms\LoginForm;

/**
 * @apiDefine ProfileNotFoundError
 * @apiError ProfileNotFound Участник не найден
 * @apiErrorExample {json} Пример ошибки "Участник не найден":
 * HTTP/1.1 400 Bad Request
 * {
 *     "result": "FAIL",
 *     "errors": {
 *         "profile": "Участник не найден"
 *     }
 * }
 */

/**
 * @apiDefine ServerUnknownError
 * @apiError ServerError Ошибка на сервере
 * @apiErrorExample {json} Пример ошибки "Ошибка на сервере":
 * HTTP/1.1 500 Bad Request
 * {
 *     "result": "FAIL",
 *     "errors": {
 *       "server": "Ошибка на сервере"
 *     }
 * }
 */

class AuthController extends ApiController
{
    /**
     * @api {post} profiles/api/auth/login Авторизация участника
     * @apiName ProfilesLogin
     * @apiGroup Profiles
     *
     * @apiParam {String} login Логин (номер клубной карты для роли "profclub" или телефон для роли regular")
     * @apiParam {String} password Пароль
     * @apiParam {String} role Роль участника ("profclub" или "regular")
     *
     * @apiParamExample {json} Пример запроса для роли "profclub":
     * {
     *   "login": "16219",
     *   "password": "123123",
     *   "role": "profclub"
     * }
     *
     * @apiParamExample {json} Пример запроса для роли "regular":
     * {
     *   "login": "+7 (915) 191-35-83",
     *   "password": "123123",
     *   "role": "regular"
     * }
     *
     * @apiSuccess {String} result Статус ответа "OK"
     * @apiSuccess {String} profile_id Индификатор участника
     * @apiSuccess {String} token JWT Токен
     *
     * @apiSuccessExample {json} Пример успешного ответа:
     * HTTP/1.1 200 OK
     * {
     *   "result": "OK",
     *   "profile_id": 4,
     *   "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcm9maWxlX2lkIjo0fQ.MDBkNTg1MjE1YzFhZmJlZGZhMGQwMDk2ZWI2NTNkYTlhNjljOTExM2FlZTcxZWE0MThkNjY2YWMwNjhlOGQzZA"*
     * }
     *
     */
    public function actionLogin()
    {
        $model = new LoginForm();
        $model->load(Yii::$app->request->post(), '');

        if ($model->process()) {
            return $this->ok([
                'token' => $model->getJwt()->token,
                'profile_id' => $model->getProfile()->id
            ], 'Авторизация: успешно');
        }

        return $this->error($model->getFirstErrors(), 'Ошибка авторизации', 403);
    }

    /**
     * @api {post} profiles/api/auth/get-profile Информация по участнику
     * @apiName ProfilesGet
     * @apiGroup Profiles
     *
     * @apiParam {Number} profile_id Идентификатор участника
     *
     * @apiParamExample {json} Пример запроса:
     * {
     *   "profile_id": 1
     * }
     *
     * @apiSuccess {Object} profile Данные по участнику
     * @apiSuccess {Number} profile.profile_id Идентификатор участника
     * @apiSuccess {String} profile.clubcard Номер клубной карты
     * @apiSuccess {String} profile.full_name Полное имя участника
     * @apiSuccess {String} profile.first_name Имя участника
     * @apiSuccess {String} profile.last_name Фамилия участника
     * @apiSuccess {String} profile.middle_name Отчество участника
     * @apiSuccess {String} profile.role Роль участника ("profclub" или "regular")
     * @apiSuccess {String} profile.gender Пол
     * @apiSuccess {Number} profile.city_id Идентификатор участника
     * @apiSuccess {String} profile.city_local
     * @apiSuccess {String} profile.specialty
     * @apiSuccess {String} profile.avatar_url
     * @apiSuccess {String} profile.phone_mobile
     * @apiSuccess {String} profile.email
     * @apiSuccess {Number} profile.balance Баллы доступные для трат
     * @apiSuccess {Number} profile.balance_total Подтвержденные баллы
     * @apiSuccess {String} profile.birthday_on
     * @apiSuccess {String} profile.registered_at
     * @apiSuccess {String} profile.created_at
     * @apiSuccess {String} profile.checked_at
     * @apiSuccess {String} profile.blocked_at
     * @apiSuccess {String} profile.blocked_reason
     * @apiSuccess {String} profile.banned_at
     * @apiSuccess {String} profile.banned_reason
     * @apiSuccess {Object} profile.account Налоговая анкета
     * @apiSuccess {String} profile.account.status
     * @apiSuccess {String} profile.account.status_label
     * @apiSuccess {String} result OK при успешном запросе
     *
     * @apiSuccessExample {json} Пример успешного ответа:
     * HTTP/1.1 200 OK
     * {
     *     "result": "OK",
     *     "profile": {
     *         "profile_id": 937,
     *         "clubcard": 91494,
     *         "full_name": "Иван Иванов",
     *         "first_name": "Иван",
     *         "last_name": "Иванов",
     *         "middle_name": "Иванович",
     *         "role": "profclub",
     *         "gender": null,
     *         "city_id": 1073,
     *         "city_local": "Абаза, Красноярский край",
     *         "specialty": null,
     *         "avatar_url": "http://unitile.f/images/avatar_blank.png?v=2",
     *         "phone_mobile": "+79299004040",
     *         "email": "user@mail.ru",
     *         "balance": 7000,
     *         "balance_total": 10690,
     *         "birthday_on": null,
     *         "registered_at": null,
     *         "created_at": "2018-10-26 15:10:25",
     *         "checked_at": "2018-10-26 17:10:00",
     *         "blocked_at": null,
     *         "blocked_reason": null,
     *         "banned_at": null,
     *         "banned_reason": null,
     *         "account": {
     *             "status": "approved",
     *             "status_label": "Подтверждена"
     *         }
     *     }
     * }
     *
     * @apiUse ProfileNotFoundError
     *
     */
    public function actionGetProfile()
    {
        $profile = Profile::findOne(['id' => Yii::$app->request->post('profile_id')]);

        if (null === $profile) {
            return $this->error(['profile' => 'Участник не найден'], 'Ошибка получения данных по участнику');
        }

        $this->logResponse = false;

        return $this->ok(['profile' => $profile], 'Получение данных по участнику');
    }

    public function actionProfileEdit()
    {
        $postData = Yii::$app->request->post();
        $profile_id = Yii::$app->request->post('profile_id', null);
        $profile = Profile::findOne(['id' => $profile_id]);

        if ($profile == null) {
            return $this->error('Участник не найден', 'Ошибка обновления профиля участника');
        }

        if (empty($postData)) {
            return $this->error('Пустой запрос', 'Ошибка обновления профиля участника');
        }

        if ($profile->load($postData, '') && $profile->save()) {
            return $this->ok(['profile' => $profile], 'Успешное обновление профиля участников');
        }

        return $this->error($profile->getFirstErrors(), 'Ошибка обновления профиля участника');
    }

    public function actionConfirmPers()
    {
        $profile_id = Yii::$app->request->post('profile_id', null);
        $profile = Profile::findOne(['id' => $profile_id]);

        if (null === $profile) {
            return $this->error('Участник не найден', 'Ошибка подтверждения обработки персональных данных');
        }

        if ($profile->pers_at == null) {
            $profile->updatePersAt();
            $profile->refresh();
        }

        return $this->ok(['profile' => $profile], 'Согласие на обработку персональных данных');
    }

    public function actionLoadAvatar()
    {
        $data = ['ApiAvatar' => Yii::$app->request->post()];
        $model = new ApiAvatar();

        if ($model->load($data) && $model->save()) {
            $return = ['result' => 'OK', 'filename' => $model->getFilename(), 'webpath' => $model->getWebpath()];
            Log::setComment("Успешная загрузка аватара");
            Log::setResponse($return);
            return $return;
        }
        else {
            $return = ['result' => 'FAIL', 'reason' => 'Не удалось сохранить аватар', 'errors' => $model->getFirstErrors()];
            Yii::$app->response->statusCode = 400;
            Log::setComment("Ошибка при загрузке аватара", 400);
            Log::setResponse($return);
            return $return;
        }
    }

    /**
     * @param $term
     * @return array
     */
    public function actionCity()
    {
        $term = Yii::$app->request->post('city', null);

        $raw = (new Query())
            ->select('c.title city, r.title region')
            ->from(['c' => City::tableName()])
            ->leftJoin(['r' => Region::tableName()], 'r.id = c.region_id')
            ->where("c.title LIKE :term", [':term' => $term . '%'])
            ->limit(15)
            ->all();

        $titles = array();

        foreach ($raw as $r) {
            $title = $r['city'];

            if (!empty($r['region'])) {
                $title .= ', ' . $r['region'];
            }

            $titles[] = $title;
        }
        $this->logResponse = false;

        return $this->ok(['cities' => $titles], 'Выдача городов подсказкой');
    }

    public function actionGcm()
    {
        $platform = Yii::$app->request->post('platform');
        $token = Yii::$app->request->post('token');
        $profile_id = Yii::$app->request->post('profile_id');

        if (!$platform || !$token || !$profile_id) {
            return $this->error('Необходимо указать поля: platform, token, profile_id', 'Ошибка добавления токена GCM');
        }

        $gcm = \ms\loyalty\mobile\common\models\ApiGcm::findOne(['token' => $token]);

        if ($gcm == null) {
            $gcm = new \ms\loyalty\mobile\common\models\ApiGcm();
            $gcm->profile_id = $profile_id;
            $gcm->token = $token;
            $gcm->platform = $platform;
            $gcm->api_key = \ms\loyalty\mobile\common\models\ApiGcm::API_KEY;
            $gcm->save(false);
        }

        return $this->ok(['gcm' => $gcm], 'Токен GCM добавлен');
    }
}