<?php

namespace modules\profiles\frontend\controllers\api;

use Yii;
use ms\loyalty\api\frontend\base\ApiController;
use modules\profiles\common\models\Dealer;

class DealerController extends ApiController
{
    /**
     * @api {post} profiles/api/dealer/list Список торговых точек
     * @apiDescription Получить список всех торговых точек
     * @apiName ProfilesDealerList
     * @apiGroup Profiles
     *
     * @apiParam {String} name Название торговой точки для фильтрации списка (необязательный параметр)
     *
     * @apiParamExample {json} Пример запроса:
     * {
     *   "name": "1"
     * }
     *
     * @apiSuccess {String} result Статус ответа "OK"
     * @apiSuccess {Object} dealers Список торговых точек
     * @apiSuccess {Integer} dealers.id Идентификатор
     * @apiSuccess {String} dealers.title Название
     *
     * @apiSuccessExample {json} Пример успешного ответа:
     * {
     *   "result": "OK",
     *   "dealers": [
     *     {
     *       "id": 1,
     *       "name": "Тестовая торговая точка 1"
     *     },
     *     {
     *       "id": 2,
     *       "name": "Тестовая торговая точка 2"
     *     },
     *     {
     *       "id": 3,
     *       "name": "Торговая точка 12313"
     *     }
     *   ]
     * }
     *
     */
    public function actionList()
    {
        $name = Yii::$app->request->post('name');

        $dealers = Dealer::find()
            ->orderBy(['name' => SORT_ASC]);

        if ($name) {
            $dealers->andWhere(['LIKE', 'name', $name]);
        }

        $this->logResponse = false;

        return $this->ok(['dealers' => $dealers->all()], 'Получение списка торговых точек');
    }
}