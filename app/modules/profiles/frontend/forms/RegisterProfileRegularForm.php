<?php

namespace modules\profiles\frontend\forms;

use Yii;
use yii\base\Model;
use libphonenumber\PhoneNumberFormat;
use marketingsolutions\phonenumbers\PhoneNumber;
use ms\loyalty\api\common\models\Token;
use modules\profiles\common\models\Profile;
use modules\profiles\common\managers\RoleManager;
use modules\profiles\frontend\forms\ProfileRegularRegisterForm;

class RegisterProfileRegularForm extends Model
{
    /** @var string */
    public $token;

    /** @var string */
    public $phone;

    /** @var string */
    public $password;

    /** @var string */
    public $passwordConfirm;

    /** @var bool */
    public $checkedRules = false;

    /** @var bool */
    public $checkedPers = false;

    /** @var Profile */
    protected $profile = null;

    /** @var Token */
    protected $tokenModel = null;

    public function rules()
    {
        return [
            ['token', 'required'],
            ['token', 'string', 'max' => 255],
            ['token', 'checkToken'],

            ['phone', 'required'],
            ['phone', 'string', 'max' => 255],
            ['phone', 'checkProfile'],

            ['password', 'string', 'max' => 255],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['passwordConfirm', 'string', 'max' => 255],
            ['passwordConfirm', 'required'],
            ['passwordConfirm', 'compare', 'compareAttribute' => 'password'],

            ['checkedRules', 'compare', 'compareValue' => 1,
                'message' => 'Вы должны согласиться с правилами акции для участния в программе'
            ],

            ['checkedPers', 'compare', 'compareValue' => 1,
                'message' => 'Вы должны разрешить обработку своих персональных данных для участния в программе'
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
            'passwordConfirm' => 'Подтверждение пароля',
            'checkedRules' => 'Согласен / согласна с правилами участия в программе',
            'checkedPers' => 'Даю согласие на обработку своих персональных данных',
        ];
    }

    public function checkToken()
    {
        $tokenModel = Token::findOne([
            'token' => $this->token,
//            'type' => Token::TYPE_SMS_NOPROFILE
        ]);

        if (null === $tokenModel) {
            $this->addError('token', 'Ошибка токена. Регистрация доступна лишь по токену');
            return false;
        }

        $this->tokenModel = $tokenModel;
    }

    public function checkProfile()
    {
        $phone = $this->phone;

        // TODO: move to phoneValidator
        if (empty($phone) || PhoneNumber::validate($phone, 'RU') == false) {
            $this->addError('phone', 'Ошибка в номере телефона');
            return false;
        }

        $phoneNumber = PhoneNumber::format($phone, PhoneNumberFormat::E164, 'RU');

        $profile = ProfileRegularRegisterForm::findOne([
            'phone_mobile' => $phoneNumber,
            'role' => RoleManager::ROLE_PROFCLUB,
        ]);

        if ($profile) {
            $this->addError('phone', "По номеру $phoneNumber уже прошла регистрация участника ProfClub");
            return false;
        }

        $profile = ProfileRegularRegisterForm::findOne([
            'phone_mobile' => $phoneNumber,
            'role' => RoleManager::ROLE_REGULAR,
        ]);

        if (null === $profile) {
            $profile = new ProfileRegularRegisterForm;
        }

        if ($profile->registered_at) {
            $this->addError('phone', "По номеру $phoneNumber уже прошла регистрация");
            return false;
        }

        $this->profile = $profile;
        $this->profile->phone_mobile = $phoneNumber;
    }

    public function process()
    {
        if (!$this->validate()) {
            return false;
        }

        $this->profile->load(Yii::$app->request->post(), '');
        $this->profile->phone_mobile_local = $this->profile->phone_mobile;


        if ($this->profile->save()) {
            $this->profile->updateRegisteredAt();
            $this->profile->updatePersAt();
            $this->profile->setPasshash($this->password);
            $this->tokenModel->delete();

            return true;
        }

        $errors = array_values($this->profile->getFirstErrors());
        $this->addError('profile', $errors[0]);

        return false;
    }

    /**
     * @return Profile
     */
    public function getProfile()
    {
        $this->profile->refresh();

        return $this->profile;
    }
}