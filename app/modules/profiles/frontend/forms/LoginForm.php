<?php

namespace modules\profiles\frontend\forms;

use yii\base\Model;
use ms\loyalty\api\ApiModuleTrait;
use ms\loyalty\api\common\models\Jwt;
use ms\loyalty\api\common\models\LoginTry;
use modules\profiles\common\models\Profile;
use modules\profiles\common\managers\RoleManager;
use modules\profiles\common\interfaces\ValidateLoginFormInterface;

class LoginForm extends Model
{
    use ApiModuleTrait;

    /** @var string */
    public $login;

    /** @var string */
    public $password;

    /** @var string */
    public $role;

    /** @var Jwt */
    private $jwt;

    /** @var Profile */
    private $profile;

    public function rules()
    {
        return [
            ['login', 'required'],
            ['login', 'string'],
            ['login', 'checkLogin'],

            ['password', 'required'],
            ['password', 'string'],

            ['role', 'required'],
            ['role', 'string'],
            ['role', 'in', 'range' => array_keys(RoleManager::getList())],
        ];
    }

    public function attributeLabels()
    {
        return [
            'login' => 'Номер телефона',
            'password' => 'Пароль',
            'role' => 'Роль',
        ];
    }

    public function checkLogin()
    {
        $class = new \ReflectionClass(Profile::class);
        if (false == $class->implementsInterface(ValidateLoginFormInterface::class)) {
            throw new \Exception("Class Profile have to implement ValidateLoginFormInterface");
        }

        $max = self::getApiModule()->jwtTriesPerHour;
        if (LoginTry::isBruteforce($this->login)) {
            $this->addError('login', "Превышен лимит попыток в час ($max)");
        }
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function process()
    {
        if (!$this->validate()) {
            return false;
        }

        /** @var Profile|string|null $result */
        $result = Profile::validateLoginPassword($this);

        if ($result instanceof Profile) {
            LoginTry::create($this->login, true);
            $this->profile = $result;
            $this->jwt = Jwt::login($this->login, $this->profile);

            return true;
        }
        elseif (is_string($result)) {
            $this->addError('login', $result);
        }
        else {
            LoginTry::create($this->login, false);
            $max = self::getApiModule()->jwtTriesPerHour;
            $msg = "Неверный логин или пароль";

            if ($max) {
                $msg .= '. Осталось попыток: ' . ($max - LoginTry::failedLastHour($this->login));
            }

            $this->addError('login', $msg);
        }

        return false;
    }

    /**
     * @return Jwt
     */
    public function getJwt()
    {
        return $this->jwt;
    }

    /**
     * @return Profile
     */
    public function getProfile()
    {
        return $this->profile;
    }
}