<?php

namespace modules\profiles\frontend\forms;

use modules\profiles\common\models\Profile;
use modules\profiles\common\managers\RoleManager;

class ProfileRegularRegisterForm extends Profile
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['role', 'required'],
            ['role', 'string', 'max' => 16],
            ['role',  'compare', 'compareValue' => RoleManager::ROLE_REGULAR],

            ['first_name', 'required'],

            ['last_name', 'required'],

            ['middle_name', 'required'],

            ['city_id', 'required'],

            ['email', 'required'],

           // ['dealer_id', 'required'],
        ]);
    }
}