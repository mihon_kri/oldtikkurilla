<?php

use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\web\View $this
 * @var \yii\mail\BaseMessage $content
 */

$settings = \ms\loyalty\api\common\models\ApiSettings::get();
$font = "font-size: 15px; color: #333; font-family: Arial, Helvetica, sans-serif; background: white;";
$bgColor = $settings->email_color_bg;
$frontUrl = empty(getenv('FRONTEND_SPA')) ? getenv('FRONTEND_WEB') : getenv('FRONTEND_SPA');
?>

<?php $this->beginPage() ?>
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?= Html::encode($this->title) ?></title>
      <?php $this->head() ?>
  </head>
  <body style="background: <?= $bgColor ?>">
  <?php $this->beginBody() ?>

  <table style="background: <?= $bgColor ?>; width: 100%; padding:40px 0;">
    <tr>
      <td>
        <table style="background:white; width:720px; margin: 0 auto; <?= $font ?>">
            <?php if ($logoUrl = $settings->logoUrl): ?>
              <tr>
                <td style="padding:10px; border-bottom: 1px solid <?= $bgColor ?>; text-align:center; <?= $font ?>">
                  <a href="<?= $frontUrl ?>" target="_blank">
                    <img src="<?= $logoUrl ?>" style="max-height:100px;max-width:500px"/>
                  </a>
                </td>
              </tr>
            <?php endif; ?>
          <tr>
            <td style="padding:20px 30px; <?= $font ?>" class="email-content"><?= $content ?></td>
          </tr>
            <?php if (!empty($settings->email_text_footer)): ?>
              <tr>
                <td style="padding:20px 30px; border-top: 1px solid <?= $bgColor ?>; <?= $font ?>">
                    <?= $settings->email_text_footer ?>
                </td>
              </tr>
            <?php endif; ?>
        </table>
      </td>
    </tr>
  </table>

  <?php $this->endBody() ?>
  </body>
  </html>
<?php $this->endPage() ?>