<?php

return [
    'id' => 'yz2-app-standard',
    'language' => 'ru',
    'sourceLanguage' => 'ru-RU',
    'timeZone' => 'Europe/Moscow',
    'extensions' => require(YZ_VENDOR_DIR . '/yiisoft/extensions.php'),
    'vendorPath' => YZ_VENDOR_DIR,
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'bootstrap' => [
        'log',
    ],
    'components' => [
        'db' => [
            'class' => yii\db\Connection::class,
            'charset' => 'utf8',
            'dsn' => getenv('DB_DSN'),
            'username' => getenv('DB_USER'),
            'password' => getenv('DB_PASSWORD'),
            'tablePrefix' => getenv('DB_TABLE_PREFIX'),
            'attributes' => [
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));",
            ],
        ],
        'formatter' => [
            'class' => \yii\i18n\Formatter::class,
            'timeZone' => 'Europe/Moscow',
            'defaultTimeZone' => 'Europe/Moscow',
            'dateFormat' => 'dd.MM.yyyy',
            'timeFormat' => 'HH:mm:ss',
            'datetimeFormat' => 'dd.MM.yyyy HH:mm',
        ],
        'i18n' => [
            'translations' => [
                'loyalty' => [
                    'class' => yii\i18n\PhpMessageSource::class,
                    'basePath' => '@common/messages',
                    'sourceLanguage' => 'ru-RU',
                ],
                'common' => [
                    'class' => yii\i18n\PhpMessageSource::class,
                    'basePath' => '@common/messages',
                    'sourceLanguage' => 'en-US',
                ],
                'frontend' => [
                    'class' => yii\i18n\PhpMessageSource::class,
                    'basePath' => '@frontend/messages',
                    'sourceLanguage' => 'en-US',
                ],
                'backend' => [
                    'class' => yii\i18n\PhpMessageSource::class,
                    'basePath' => '@backend/messages',
                    'sourceLanguage' => 'en-US',
                ],
                'console' => [
                    'class' => yii\i18n\PhpMessageSource::class,
                    'basePath' => '@console/messages',
                    'sourceLanguage' => 'en-US',
                ],
            ]
        ],
        'taxesManager' => [
            'class' => \ms\loyalty\taxes\common\components\TaxesManager::class,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => yii\log\FileTarget::class,
                    'levels' => ['error', 'warning', 'trace'],
                ],
            ],
        ],
        'mailer' => [
            'class' => \yii\swiftmailer\Mailer::class,
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
            'messageConfig' => [
                'from' => [getenv('MAIL_FROM') => getenv('MAIL_NAME')],
            ],
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.yandex.ru',
                'username' => getenv('MAIL_FROM'),
                'password' => getenv('MAIL_PASSWORD'),
                'port' => '465',
                'encryption' => 'SSL',
            ],
        ],
        'sms' => [
            'class' => \marketingsolutions\sms\Sms::class,
            'services' => [
                'service' => [
                    'class' => \marketingsolutions\sms\services\Smsc::class,
                    'login' => getenv('SMSC_LOGIN'),
                    'password' => getenv('SMSC_PASSWORD'),
                    'from' => getenv('SMS_FROM'),
                ]
            ]
        ],
        'api' => [
            'class' => \ms\loyalty\api\common\components\ApiComponent::class,
            'domain' => getenv('API_DOMAIN'),
            'headers' => [
                'X-Token' => getenv('API_XTOKEN'),
            ]
        ],
        'financeChecker' => [
            'class' => \ms\loyalty\finances\common\components\FinanceChecker::class,
            'moneyDifferenceThreshold' => 50000,
            'emailNotificationThreshold' => 100000,
            'email' => 'alex@zakazpodarka.ru',
        ],
        'zp1c' => [
            'class' => \ms\zp1c\client\Client::class,
            'url' => getenv('ZP_1C_HOST'),
            'login' => getenv('ZP_1C_LOGIN'),
            'password' => getenv('ZP_1C_PASSWORD'),
        ],
    ],
    'modules' => [
        'profiles' => [
            'class' => modules\profiles\common\Module::class,
            'profileType' => modules\profiles\common\Module::PROFILE_TYPE_CITY_REGION,
        ],
        'sales' => [
            'class' => modules\sales\common\Module::class,
            'documentsBaseUrl' => 'https://api-tikkurila.msforyou.ru',
        ],
        'actions' => [
            'class' => modules\actions\common\Module::class,
        ],
        'tickets' => [
            'class' => ms\loyalty\tickets\common\Module::class,
            'zmq_port' => 5556,
            'websocket_port' => 8081
        ],
        'feedback' => [
            'class' => ms\loyalty\feedback\common\Module::class,
            'userReplyText' => ' ',
            'userAddText' => 'Мы приняли в работу Ваше обращение и ответим в течение двух рабочих дней. Но постараемся как можно быстрее.',
        ],
        'catalog' => [
            'class' => ms\loyalty\catalog\common\Module::class,
            'disableOrderingForNoTaxAccount' => false,
            'classMap' => [
                'prizeRecipient' => \modules\profiles\common\models\Profile::class,
            ],
            'loyaltyName' => getenv('LOYALTY_1C_NAME'),
        ],
        'payments' => [
            'class' => ms\loyalty\prizes\payments\common\Module::class,
            'disablePaymentsForNoTaxAccounts' => false,
            'loyaltyName' => getenv('LOYALTY_1C_NAME'),
        ],
        'taxes' => [
            'class' => ms\loyalty\taxes\common\Module::class,
            'incomeTaxPaymentMethod' => \ms\loyalty\taxes\common\Module::INCOME_TAX_PAYMENT_METHOD_COMPANY,
        ],
        'pages' => [
            'class' => ms\loyalty\pages\common\Module::class,
            'layoutGuest' => 'index',
            'layoutUser' => 'main',
        ],
        'banners' => [
            'class' => ms\loyalty\banners\common\Module::class,
        ],
        'survey' => [
            'class' => ms\loyalty\survey\common\Module::class,
        ],
        'news' => [
            'class' => ms\loyalty\news\common\Module::class,
        ],
        'sms' => [
            'class' => ms\loyalty\sms\common\Module::class,
        ],
        'mailing' => [
            'class' => \yz\admin\mailer\common\Module::class,
            'mailLists' => [
                \modules\profiles\common\mailing\ProfileMailingList::class,
            ]
        ],
        'notifications' => [
            'class' => ms\loyalty\notifications\common\Module::class,
        ],
        'checker' => [
            'class' => ms\loyalty\checker\common\Module::class,
            'emails' => '7binary@bk.ru'
        ],
        'mobile' => [
            'class' => ms\loyalty\mobile\common\Module::class,
        ],
        'api' => [
            'class' => ms\loyalty\api\common\Module::class,
            'authType' => \ms\loyalty\api\common\Module::AUTH_TOKEN,
            'jwtEnabled' => getenv('YII_ENV') === 'prod',
        ],

    ],
    'params' => [
    ],
];
