<?php
/**
 * @var \marketingsolutions\scheduling\Schedule $schedule
 */

/** E-mail & SMS */
$schedule->command('mailing/mails/send');
$schedule->command('sms/send/run');

/** Certificates */
$schedule->command('catalog/zakazpodarka-orders/create-soap --method=processed')->everyFiveMinutes();
$schedule->command('catalog/zakazpodarka-orders/check-status --interval=600');
$schedule->command('catalog/card-items/send-to-user')->everyTenMinutes();

/** Payments */
$schedule->command('payments/process/index')->everyMinute();
$schedule->command('payments/process/check')->everyNMinutes(3);

/** Finishes active actions that has expired */
$schedule->command('actions/action/finish')->everyMinute();

/**Send new publication email */
$schedule->command('profiles/profile-news/index')->everyMinute();

/** Creates bonuses for completed plans */
$schedule->command('sales/bonuses/create-plan-bonuses')->everyMinute();

/** Pay bonuses for finished actions */
$schedule->command('sales/bonuses/pay')->everyMinute();

/** Api Logs */
$schedule->command('api/clean')->cron('3 3 * * *');

/** Updates paid_at from 1C */
$schedule->command('payments/process/update')->cron('30 05 * * *');
$schedule->command('catalog/zakazpodarka-orders/update-status')->cron('45 05 * * *');

/** Tickets */
$schedule->command('tickets/run/restart tikkurila')->cron('00 * * * *');