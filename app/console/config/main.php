<?php

return [
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'console\controllers',
    'controllerMap' => [
        'migrate' => [
            'class' => dmstr\console\controllers\MigrateController::class,
            'migrationTable' => '{{%migrations}}',
            'migrationPath' => '@migrations',
        ],
    ],
    'components' => [
        'schedule' => [
            'class' => \marketingsolutions\scheduling\Schedule::class,
        ],
    ],
    'modules' => [
        'mailing' => [
            'class' => yz\admin\mailer\console\Module::class,
        ],
        'api' => [
            'class' => ms\loyalty\api\console\Module::class,
        ],
        'catalog' => [
            'class' => ms\loyalty\catalog\console\Module::class,
        ],
        'payments' => [
            'class' => ms\loyalty\prizes\payments\console\Module::class,
        ],
        'sales' => [
            'class' => modules\sales\console\Module::class,
        ],
        'profiles' => [
            'class' => modules\profiles\console\Module::class,
        ],
        'actions' => [
            'class' => modules\actions\console\Module::class,
        ],
        'sms' => [
            'class' => ms\loyalty\sms\console\Module::class,
        ],
        'checker' => [
            'class' => ms\loyalty\checker\console\Module::class,
        ],
        'tickets' => [
            'class' => ms\loyalty\tickets\console\Module::class,
        ],
    ],
    'params' => [
        'yii.migrations' => [
            '@modules/profiles/migrations',
            '@modules/sales/migrations',
            '@modules/actions/migrations',
        ],
    ],
];
