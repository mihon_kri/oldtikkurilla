<?php

namespace console\controllers;

use ms\loyalty\catalog\common\cards\CardItem;
use ms\loyalty\catalog\common\models\CatalogOrder;
use ms\loyalty\catalog\common\models\OrderedCard;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Application configuration controller
 *
 * @author Eugene Terentev <eugene@terentev.net>
 */
class AppController extends Controller
{
    public $writablePaths = [
        '@common/runtime',
        '@frontend/runtime',
        '@frontend/web/assets',
        '@backend/runtime',
        '@backend/web/assets',
    ];
    public $executablePaths = [
        '@base/yii',
    ];
    public $generateKeysPaths = [
        '@base/.env'
    ];

    public function actionSetup()
    {
        $this->setWritable($this->writablePaths);
        $this->setExecutable($this->executablePaths);
        $this->setGeneratedKey($this->generateKeysPaths);
    }

    public function setWritable($paths)
    {
        foreach ($paths as $writable) {
            $writable = Yii::getAlias($writable);
            Console::output("Setting writable: {$writable}");
            @chmod($writable, 0777);
        }
    }

    public function setExecutable($paths)
    {
        foreach ($paths as $executable) {
            $executable = Yii::getAlias($executable);
            Console::output("Setting executable: {$executable}");
            @chmod($executable, 0755);
        }
    }

    public function setGeneratedKey($paths)
    {
        foreach ($paths as $file) {
            $file = Yii::getAlias($file);
            Console::output("Generating keys in {$file}");
            $content = file_get_contents($file);
            $content = preg_replace_callback('/<generated_key>/', function () {
                $length = 32;
                $bytes = openssl_random_pseudo_bytes(32, $cryptoStrong);
                return strtr(substr(base64_encode($bytes), 0, $length), '+/', '_-');
            }, $content);
            file_put_contents($file, $content);
        }
    }

    public function actionEmail()
    {
        $mailbox = new \PhpImap\Mailbox('{imap.yandex.ru:993/imap/ssl}INBOX', 'b24leads@msforyou.ru', 'leadsb24');

        $mailsIds = $mailbox->searchMailbox('ALL');
        if (!$mailsIds) {
            die('Mailbox is empty');
        }

        foreach ($mailsIds as $mailId) {
            $mail = $mailbox->getMail($mailId);
            $fromEmail = $mail->fromAddress;
            $messageId = $mail->messageId;
            $messageHtml = $mail->textHtml;

            preg_match_all("|(.*)Имя: (.*)<(.*)|U", $messageHtml, $out, PREG_PATTERN_ORDER);
            $name = $out[2][0];
            preg_match_all("|(.*)Телефон: (.*)<(.*)|U", $messageHtml, $out, PREG_PATTERN_ORDER);
            $phone = $out[2][0];
            preg_match_all("|(.*)Email: (.*)<(.*)|U", $messageHtml, $out, PREG_PATTERN_ORDER);
            $email = $out[2][0];
            preg_match_all("|(.*)Адрес страницы: (.*)<(.*)|U", $messageHtml, $out, PREG_PATTERN_ORDER);
            $url = $out[2][0];
            preg_match_all("|(.*)Страница: (.*)<(.*)|U", $messageHtml, $out, PREG_PATTERN_ORDER);
            $source = $out[2][0];

            echo implode(', ', [$fromEmail, $messageId, $name, $phone, $email, $url, $source]) . PHP_EOL;
        }
    }

    public function actionCheck()
    {
        $url = 'https://proverkacheka.nalog.ru:9999/v1/mobile/users/login';
        $username = '+79032577093';
        $password = '371322';

        $ch = curl_init();
        $headers = array(
            'Device-ID: AndroidId',
            'Device-OS: Android 6.0.1',
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        var_dump($result);

        # не рабочий от ВП от 01.01.2017
        $fn = '8710000100017230';
        $fd = '8206';
        $fpd = '3078882490';
        $s = '267.50';
        $t = '2017-01-01T13:01:00';

        # 1 - рабочий от 21.06.2018
        $fn = '8715000100008848';
        $fd = '6325';
        $fpd = '2820094859';
        $s = '1530.00';
        $t = '2018-06-21T17:16:00';

        # 2 - рабочий от 28.05.2018
        $fn = '8710000101863106';
        $fd = '2805';
        $fpd = '2252889725';
        $s = '200.00';
        $t = '2018-05-28T18:55:00';

        for ($i = 1; $i <= 1000; $i++) {
            $ch3 = curl_init();
            $q = "?fiscalSign={$fpd}&sendToEmail=no";
            $url = "https://proverkacheka.nalog.ru:9999/v1/inns/*/kkts/*/fss/$fn/tickets/{$fd}" . $q;

            $headers = array(
                'Device-ID: AndroidId',
                'Device-OS: Android 6.0.1',
            );
            curl_setopt($ch3, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch3, CURLOPT_URL, $url);
            curl_setopt($ch3, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch3, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($ch3, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch3, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch3, CURLOPT_SSL_VERIFYHOST, false);

            $res3 = curl_exec($ch3);
            echo "#{$i} : $res3" . PHP_EOL;
            curl_close($ch3);

            //        $params = json_decode($res3, true);
            //        echo '<pre>'; print_r($params); echo '</pre>';
            //        exit;
        }
    }
}