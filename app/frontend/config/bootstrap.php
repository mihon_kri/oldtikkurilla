<?php

$listener = new \marketingsolutions\events\Listener(
    new \marketingsolutions\events\PatternEventsProvider(),
    new \marketingsolutions\events\PrefixMethodFinder()
);

\Yii::$container->set(
    \ms\loyalty\contracts\prizes\PrizeRecipientInterface::class,
    function () {
        if (\Yii::$app->user->isGuest) {
            \Yii::$app->response->redirect('/')->send();
            die;
        }

        return \Yii::$app->user->profile;
    }
);

// Payments create validate
\yii\base\Event::on(
    \ms\loyalty\prizes\payments\frontend\forms\CreatePaymentForm::class,
    \ms\loyalty\prizes\payments\frontend\forms\CreatePaymentForm::EVENT_BEFORE_VALIDATE_AMOUNT,
    [\frontend\listeners\CreatePaymentFormListener::class, 'whenBeforeValidateAmount']
);

// Catalog orders create validate
\yii\base\Event::on(
    \ms\loyalty\catalog\frontend\forms\CatalogOrderForm::class,
    \ms\loyalty\catalog\frontend\forms\CatalogOrderForm::EVENT_BEFORE_VALIDATE_AMOUNT,
    [\frontend\listeners\CreateCatalogOrderFormListener::class, 'whenBeforeValidateAmount']
);



