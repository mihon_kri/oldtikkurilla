define({ "api": [
  {
    "type": "post",
    "url": "actions/api/action/current-list",
    "title": "Список акций",
    "description": "<p>Получить список всех доступных для участника акций</p>",
    "name": "ActionsCurrent",
    "group": "Actions",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "profile_id",
            "description": "<p>Индификатор пользователя</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример запроса:",
          "content": "{\n  \"profile_id\": 1\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>Статус ответа &quot;OK&quot;</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "actions",
            "description": "<p>Список акций</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "actions.id",
            "description": "<p>Индификатор</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "actions.title",
            "description": "<p>Название</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "actions.planForAction",
            "description": "<p>План по акции</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "actions.short_description",
            "description": "<p>Короткое описание</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "actions.description",
            "description": "<p>HTML описание</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "actions.start_on",
            "description": "<p>Дата начала</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "actions.end_on",
            "description": "<p>Дата окончания</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "actions.has_products",
            "description": "<p>Отображать или нет каталог продукции</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример успешного ответа:",
          "content": "{\n  \"result\": \"OK\",\n  \"actions\": [\n    {\n      \"id\": 10,\n      \"title\": \"Название\",\n      \"short_description\": \"Короткое описание\",\n      \"planForAction\": \"План по акции\",\n      \"description\": \"<p>HTML описание акции</p>\",\n      \"start_on\": \"2019-02-28\",\n      \"end_on\": \"2019-02-21\",\n      \"has_products\": 0\n    },\n    {\n      \"id\": 11,\n      \"title\": \"Название другой акции\",\n      \"short_description\": \"Короткое описание\",\n      \"description\": \"<p>HTML описание акции</p>\",\n      \"start_on\": \"2019-02-18\",\n      \"end_on\": \"2019-03-05\",\n      \"has_products\": 1\n    }\n  ],\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/modules/actions/frontend/controllers/api/ActionController.php",
    "groupTitle": "Actions"
  },
  {
    "type": "post",
    "url": "actions/api/action/view",
    "title": "Получить акцию",
    "description": "<p>Получить конкретную акцию со списками брендов, категорий и товаров</p>",
    "name": "ActionsView",
    "group": "Actions",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "action_id",
            "description": "<p>Индификатор акции</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример запроса:",
          "content": "{\n  \"action_id\": 2\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>Статус ответа &quot;OK&quot;</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "actions",
            "description": "<p>Список акций</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "actions.id",
            "description": "<p>Индификатор</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "actions.title",
            "description": "<p>Название</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "actions.short_description",
            "description": "<p>Короткое описание</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "actions.description",
            "description": "<p>HTML описание</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "actions.start_on",
            "description": "<p>Дата начала</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "actions.end_on",
            "description": "<p>Дата окончания</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "actions.has_products",
            "description": "<p>Отображать или нет каталог продукции</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "actions.groups",
            "description": "<p>Список брендов</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "actions.groups.id",
            "description": "<p>Идентификатор</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "actions.groups.name",
            "description": "<p>Название</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "actions.categories",
            "description": "<p>Список категорий</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "actions.categories.id",
            "description": "<p>Идентификатор</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "actions.categories.name",
            "description": "<p>Название</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "actions.products",
            "description": "<p>Список товаров</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "actions.products.id",
            "description": "<p>Идентификатор товара</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "actions.products.group_id",
            "description": "<p>Идентификатор бренда</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "actions.products.category_id",
            "description": "<p>Идентификатор категории</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "actions.products.unit_id",
            "description": "<p>Идентификатор единицы измерения</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "actions.products.name",
            "description": "<p>Название</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "actions.products.code",
            "description": "<p>Номенклатура</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "actions.products.enabled",
            "description": "<p>Признак активности</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "actions.products.weight",
            "description": "<p>Фасовка</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "actions.products.group",
            "description": "<p>Бренд товара</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "actions.products.group.id",
            "description": "<p>Индификаторв бренда</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "actions.products.group.name",
            "description": "<p>Название бренда</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "actions.products.category",
            "description": "<p>Категория товара</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "actions.products.category.id",
            "description": "<p>Индификаторв категории</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "actions.products.category.name",
            "description": "<p>Название категории</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "actions.products.unit",
            "description": "<p>Единица измерения</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "actions.products.unit.id",
            "description": "<p>Индификатов единицы измерения</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "actions.products.unit.name",
            "description": "<p>Название единицы измерения</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "actions.products.unit.short_name",
            "description": "<p>Короткое название единицы измерения</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "actions.products.unit.quantity_divider",
            "description": "<p>Делитель количества</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример успешного ответа:",
          "content": "{\n  \"result\": \"OK\",\n  \"action\": {\n  \"id\": 2,\n  \"title\": \"123213\",\n  \"short_description\": \"\",\n  \"description\": \"\",\n  \"start_on\": \"2019-02-28\",\n  \"end_on\": \"2019-02-21\",\n  \"has_products\": 1,\n  \"groups\": [\n    {\n      \"id\": 1,\n      \"name\": \"Finncolor\"\n    },\n    {\n      \"id\": 3,\n      \"name\": \"TEKS\"\n    }\n  ],\n  \"categories\": [\n    {\n      \"id\": 11,\n      \"name\": \"Грунт под антисептики\"\n    }\n  ],\n  \"products\": [\n    {\n      \"id\": 199,\n      \"group_id\": 3,\n      \"category_id\": 2,\n      \"unit_id\": 1,\n      \"name\": \"Антисептик грунтовочный Биотекс Эко Грунт ПРОФИ бесцв 0,8л\",\n      \"code\": \"700006970\",\n      \"enabled\": 1,\n      \"weight\": 0.8,\n      \"group\": {\n        \"id\": 3,\n        \"name\": \"TEKS\"\n      },\n      \"category\": {\n        \"id\": 11,\n        \"name\": \"Грунт под антисептики\"\n      },\n      \"unit\": {\n        \"id\": 1,\n        \"name\": \"Литр\",\n        \"short_name\": \"л.\",\n        \"quantity_divider\": 100\n      }\n    }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/modules/actions/frontend/controllers/api/ActionController.php",
    "groupTitle": "Actions"
  },
  {
    "type": "post",
    "url": "api/token/check-sms",
    "title": "Проверить СМС код",
    "description": "<p>Проверияет СМС код и возвращает token</p>",
    "name": "ApiCheckSms",
    "group": "Api",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>Мобильный телефон</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Тип кода (необязательный параметр)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>СМС код для проверки</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример запроса:",
          "content": "{\n  \"phone\": \"+7 (915) 191-3583\",\n  \"type\": \"sms_profile_unregistered\",\n  \"code\": \"03377\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>&quot;OK&quot; при успешном запросе</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Токен</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример успешного ответа:",
          "content": "{\n  \"result\": \"OK\",\n  \"token\": \"108867c27c909d3110ebeb09993cad9a7450200dda953ae6d5b6ef028066d061\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/vendor/marketingsolutions/loyalty-api/frontend/controllers/TokenController.php",
    "groupTitle": "Api"
  },
  {
    "type": "post",
    "url": "api/token/get-sms",
    "title": "Получения СМС",
    "description": "<p>Получения СМС кода проверки телефона</p>",
    "name": "ApiGetSms",
    "group": "Api",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>Мобильный телефон</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Тип кода (необязательный параметр), доступые значения:<br> &quot;sms&quot; - СМС<br> &quot;sms_profile&quot; - СМС по участнику<br> &quot;sms_profile_unregistered&quot; - СМС по участнику еще не зарегистрированному<br> &quot;sms_noprofile&quot; - СМС если нет участника<br> &quot;email&quot; - E-mail</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример запроса для роли \"profclub\":",
          "content": "{\n  \"phone\": \"+7 (915) 191-3583\",\n  \"type\": \"sms_profile_unregistered\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/vendor/marketingsolutions/loyalty-api/frontend/controllers/TokenController.php",
    "groupTitle": "Api"
  },
  {
    "type": "post",
    "url": "/banners/api/banners/by-group",
    "title": "Получить список баннеров по группе",
    "name": "ByGroup",
    "group": "Banners",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "group",
            "description": "<p>Название группы</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример запроса:",
          "content": "{\n  \"group\": \"guest-index\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "banners",
            "description": "<p>Данные по участнику</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "banners.position",
            "description": "<p>Позиция баннера в группе</p>"
          },
          {
            "group": "Success 200",
            "type": "Url",
            "optional": false,
            "field": "banners.banner_url",
            "description": "<p>Ссылка на картинку баннера</p>"
          },
          {
            "group": "Success 200",
            "type": "Link",
            "optional": false,
            "field": "banners.link",
            "description": "<p>Ссылка баннера, переход туда по нажатию в новое окно</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "banners.width",
            "description": "<p>Высота баннера</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "banners.height",
            "description": "<p>Ширина баннера</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "banners.group",
            "description": "<p>Группа баннера</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "banners.group.width",
            "description": "<p>Высота группы баннера</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "banners.group.height",
            "description": "<p>Ширина группы баннера</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример успешного ответа:",
          "content": "HTTP/1.1 200 OK\n{\n  \"result\": \"OK\",\n  \"banners\": [\n    {\n      \"id\": 2,\n      \"width\": null,\n      \"height\": null,\n      \"position\": 1,\n      \"banner_url\": \"http://unitile.f/data/banners/2_banner_5b34e80d612a9.jpg\",\n      \"link\": \"\",\n      \"mobile_banner_url\": null,\n      \"mobile_width\": null,\n      \"mobile_height\": null,\n      \"mobile_on\": 1,\n      \"group\": {\n        \"id\": 1,\n        \"name\": \"guest-index\",\n        \"title\": \"На главной гостевой\",\n        \"width\": 1212,\n        \"height\": 591\n      }\n    },\n    {\n      \"id\": 4,\n      \"width\": null,\n      \"height\": null,\n      \"position\": 3,\n      \"banner_url\": \"http://unitile.f/data/banners/4_banner_5b34e816e494f.jpg\",\n      \"link\": \"\",\n      \"mobile_banner_url\": null,\n      \"mobile_width\": null,\n      \"mobile_height\": null,\n      \"mobile_on\": 1,\n      \"group\": {\n        \"id\": 1,\n        \"name\": \"guest-index\",\n        \"title\": \"На главной гостевой\",\n        \"width\": 1212,\n        \"height\": 591\n      }\n    },\n    {\n      \"id\": 5,\n      \"width\": null,\n      \"height\": null,\n      \"position\": 4,\n      \"banner_url\": \"http://unitile.f/data/banners/5_banner_5b34e81b83328.jpg\",\n      \"link\": \"\",\n      \"mobile_banner_url\": null,\n      \"mobile_width\": null,\n      \"mobile_height\": null,\n      \"mobile_on\": 1,\n      \"group\": {\n        \"id\": 1,\n        \"name\": \"guest-index\",\n        \"title\": \"На главной гостевой\",\n        \"width\": 1212,\n        \"height\": 591\n      }\n    },\n    {\n      \"id\": 13,\n      \"width\": null,\n      \"height\": null,\n      \"position\": 5,\n      \"banner_url\": \"http://unitile.f/data/banners/13_banner_5be28498b0dfd.jpg\",\n      \"link\": \"https://shop.saint-gobain.ru/\",\n      \"mobile_banner_url\": null,\n      \"mobile_width\": null,\n      \"mobile_height\": null,\n      \"mobile_on\": 1,\n      \"group\": {\n        \"id\": 1,\n        \"name\": \"guest-index\",\n        \"title\": \"На главной гостевой\",\n        \"width\": 1212,\n        \"height\": 591\n      }\n    },\n    {\n      \"id\": 12,\n      \"width\": null,\n      \"height\": null,\n      \"position\": 7,\n      \"banner_url\": \"http://unitile.f/data/banners/12_banner_5b990a2d92aec.jpg\",\n      \"link\": \"http://gipgips.ru/products/heat/\",\n      \"mobile_banner_url\": null,\n      \"mobile_width\": null,\n      \"mobile_height\": null,\n      \"mobile_on\": 1,\n      \"group\": {\n        \"id\": 1,\n        \"name\": \"guest-index\",\n        \"title\": \"На главной гостевой\",\n        \"width\": 1212,\n        \"height\": 591\n      }\n    }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/vendor/marketingsolutions/loyalty-banners/frontend/controllers/api/BannersController.php",
    "groupTitle": "Banners"
  },
  {
    "type": "post",
    "url": "/mobile/api/firebase/register-device",
    "title": "Зарегистрировать устройство для PUSH-уведомлений",
    "name": "RegisterDevice",
    "group": "Mobile",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "profile_id",
            "description": "<p>Идентификатор участника</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "platform",
            "description": "<p>Платформа устройства: web / ios / android</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Токен устройства, зарегистрированного в Firebase Cloud Messaging (FCM)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример запроса:",
          "content": "{\n    \"profile_id\": 937,\n    \"platform\": \"web\",\n    \"token\": \"dNhNz_PI-kM:APA91bG4AyyQqmQUbdWRJ041K93r75UFuHJJUuuNO8c3623NVBx4h4CLW4tjWL8R7UPchEaG_9wi2qho_ocT4dUbqSKKthuFm1Wd2BfM1s5yMb6wsnD17wViVtiAoiwdy1-J22aQejgO\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Пример успешного ответа:",
          "content": "HTTP/1.1 200 OK\n{\n    \"result\": \"OK\",\n    \"gcm\": {\n        \"platform\": \"web\",\n        \"token\": \"dNhNz_PI-kM:APA91bG4AyyQqmQUbdWRJ041K93r75UFuHJJUuuNO8c3623NVBx4h4CLW4tjWL8R7UPchEaG_9wi2qho_ocT4dUbqSKKthuFm1Wd2BfM1s5yMb6wsnD17wViVtiAoiwdy1-J22aQejgO\",\n        \"profile_id\": 937,\n        \"created_at\": \"2018-11-24 18:04:59\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/vendor/marketingsolutions/loyalty-mobile/frontend/controllers/api/FirebaseController.php",
    "groupTitle": "Mobile"
  },
  {
    "type": "post",
    "url": "profiles/api/city/list",
    "title": "Список городов",
    "description": "<p>Получить список всех городов</p>",
    "name": "ProfilesCityList",
    "group": "Profiles",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Название города для фильтрации списка (необязательный параметр)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример запроса:",
          "content": "{\n  \"title\": \"моск\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>Статус ответа &quot;OK&quot;</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "cities",
            "description": "<p>Список городов</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "cities.id",
            "description": "<p>Идентификатор</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "cities.title",
            "description": "<p>Название</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример успешного ответа:",
          "content": "{\n  \"result\": \"OK\",\n  \"cities\": [\n    {\n      \"id\": 1073,\n      \"title\": \"Абаза\"\n    },\n    {\n      \"id\": 1074,\n      \"title\": \"Абакан\"\n    },\n    {\n      \"id\": 2380,\n      \"title\": \"Абакан\"\n    }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/modules/profiles/frontend/controllers/api/CityController.php",
    "groupTitle": "Profiles"
  },
  {
    "type": "post",
    "url": "profiles/api/dealer/list",
    "title": "Список торговых точек",
    "description": "<p>Получить список всех торговых точек</p>",
    "name": "ProfilesDealerList",
    "group": "Profiles",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Название торговой точки для фильтрации списка (необязательный параметр)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример запроса:",
          "content": "{\n  \"name\": \"1\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>Статус ответа &quot;OK&quot;</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "dealers",
            "description": "<p>Список торговых точек</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "dealers.id",
            "description": "<p>Идентификатор</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "dealers.title",
            "description": "<p>Название</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример успешного ответа:",
          "content": "{\n  \"result\": \"OK\",\n  \"dealers\": [\n    {\n      \"id\": 1,\n      \"name\": \"Тестовая торговая точка 1\"\n    },\n    {\n      \"id\": 2,\n      \"name\": \"Тестовая торговая точка 2\"\n    },\n    {\n      \"id\": 3,\n      \"name\": \"Торговая точка 12313\"\n    }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/modules/profiles/frontend/controllers/api/DealerController.php",
    "groupTitle": "Profiles"
  },
  {
    "type": "post",
    "url": "profiles/api/auth/get-profile",
    "title": "Информация по участнику",
    "name": "ProfilesGet",
    "group": "Profiles",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "profile_id",
            "description": "<p>Идентификатор участника</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример запроса:",
          "content": "{\n  \"profile_id\": 1\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "profile",
            "description": "<p>Данные по участнику</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "profile.profile_id",
            "description": "<p>Идентификатор участника</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.clubcard",
            "description": "<p>Номер клубной карты</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.full_name",
            "description": "<p>Полное имя участника</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.first_name",
            "description": "<p>Имя участника</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.last_name",
            "description": "<p>Фамилия участника</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.middle_name",
            "description": "<p>Отчество участника</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.role",
            "description": "<p>Роль участника (&quot;profclub&quot; или &quot;regular&quot;)</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.gender",
            "description": "<p>Пол</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "profile.city_id",
            "description": "<p>Идентификатор участника</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.city_local",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.specialty",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.avatar_url",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.phone_mobile",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.email",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "profile.balance",
            "description": "<p>Баллы доступные для трат</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "profile.balance_total",
            "description": "<p>Подтвержденные баллы</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.birthday_on",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.registered_at",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.created_at",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.checked_at",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.blocked_at",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.blocked_reason",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.banned_at",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.banned_reason",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "profile.account",
            "description": "<p>Налоговая анкета</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.account.status",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.account.status_label",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>OK при успешном запросе</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример успешного ответа:",
          "content": "HTTP/1.1 200 OK\n{\n    \"result\": \"OK\",\n    \"profile\": {\n        \"profile_id\": 937,\n        \"clubcard\": 91494,\n        \"full_name\": \"Иван Иванов\",\n        \"first_name\": \"Иван\",\n        \"last_name\": \"Иванов\",\n        \"middle_name\": \"Иванович\",\n        \"role\": \"profclub\",\n        \"gender\": null,\n        \"city_id\": 1073,\n        \"city_local\": \"Абаза, Красноярский край\",\n        \"specialty\": null,\n        \"avatar_url\": \"http://unitile.f/images/avatar_blank.png?v=2\",\n        \"phone_mobile\": \"+79299004040\",\n        \"email\": \"user@mail.ru\",\n        \"balance\": 7000,\n        \"balance_total\": 10690,\n        \"birthday_on\": null,\n        \"registered_at\": null,\n        \"created_at\": \"2018-10-26 15:10:25\",\n        \"checked_at\": \"2018-10-26 17:10:00\",\n        \"blocked_at\": null,\n        \"blocked_reason\": null,\n        \"banned_at\": null,\n        \"banned_reason\": null,\n        \"account\": {\n            \"status\": \"approved\",\n            \"status_label\": \"Подтверждена\"\n        }\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/modules/profiles/frontend/controllers/api/AuthController.php",
    "groupTitle": "Profiles",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ProfileNotFound",
            "description": "<p>Участник не найден</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример ошибки \"Участник не найден\":",
          "content": "HTTP/1.1 400 Bad Request\n{\n    \"result\": \"FAIL\",\n    \"errors\": {\n        \"profile\": \"Участник не найден\"\n    }\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "profiles/api/auth/login",
    "title": "Авторизация участника",
    "name": "ProfilesLogin",
    "group": "Profiles",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "login",
            "description": "<p>Логин (номер клубной карты для роли &quot;profclub&quot; или телефон для роли regular&quot;)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Пароль</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "role",
            "description": "<p>Роль участника (&quot;profclub&quot; или &quot;regular&quot;)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример запроса для роли \"profclub\":",
          "content": "{\n  \"login\": \"16219\",\n  \"password\": \"123123\",\n  \"role\": \"profclub\"\n}",
          "type": "json"
        },
        {
          "title": "Пример запроса для роли \"regular\":",
          "content": "{\n  \"login\": \"+7 (915) 191-35-83\",\n  \"password\": \"123123\",\n  \"role\": \"regular\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>Статус ответа &quot;OK&quot;</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile_id",
            "description": "<p>Индификатор участника</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>JWT Токен</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример успешного ответа:",
          "content": "HTTP/1.1 200 OK\n{\n  \"result\": \"OK\",\n  \"profile_id\": 4,\n  \"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcm9maWxlX2lkIjo0fQ.MDBkNTg1MjE1YzFhZmJlZGZhMGQwMDk2ZWI2NTNkYTlhNjljOTExM2FlZTcxZWE0MThkNjY2YWMwNjhlOGQzZA\"*\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/modules/profiles/frontend/controllers/api/AuthController.php",
    "groupTitle": "Profiles"
  },
  {
    "type": "post",
    "url": "profiles/api/register",
    "title": "Регистрация участника",
    "description": "<p>Регистрация нового участника<br><br> У участников с ролью &quot;profclub&quot; закрытый вид регистрации, т.е. по заранее загруженным спискам. Им не обязательно заполнять поля ФИО, Город, Email, т.к. они уже внесены в базу данных.<br><br> У участников с ролью &quot;regular&quot; открытый вид регистрации, но есль участник не был заранее загружен по спискам в базу данных, то его регистрация требует подтверждения администратора и функционал его ЛК ограничен.</p>",
    "name": "ProfilesRegistration",
    "group": "Profiles",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "role",
            "description": "<p>Роль участника (&quot;profclub&quot; или &quot;regular&quot;)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>Имя</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>Фамилия</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "middle_name",
            "description": "<p>Отчество</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>Мобильный телефон</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "clubcard",
            "description": "<p>Номер клубной карты (для роли &quot;profclub&quot;)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "city_id",
            "description": "<p>Индификатор города</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "dealer_id",
            "description": "<p>Индификатор торговой точки</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Пароль</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "passwordConfirm",
            "description": "<p>Подтверждение пароля</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "checkedRules",
            "description": "<p>Согласие с правилами акции</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "checkedPers",
            "description": "<p>Согласие на обработку персональных данных</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Токен проверки телефона по СМС</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример запроса для роли \"profclub\":",
          "content": "{\n  \"role\": \"profclub\",\n  \"phone\": \"+7 (915) 191-3581\",\n  \"clubcard\": 23062,\n  \"password\": \"123123\",\n  \"passwordConfirm\": \"123123\",\n  \"checkedRules\": 1,\n  \"checkedPers\": 1,\n  \"token\": \"9d90e96ca1472557caccbacf86a1a1cbdcdb033f9bd72bfcfe934ad8497c51f8\"\n}",
          "type": "json"
        },
        {
          "title": "Пример запроса для роли \"regular\":",
          "content": "{\n  \"role\": \"regular\",\n  \"first_name\": \"Тест\",\n  \"last_name\": \"Тестов\",\n  \"middle_name\": \"Тестович\",\n  \"phone\": \"+7 (915) 191-3583\",\n  \"email\": \"123@123.ru\",\n  \"city_id\": 1,\n  \"dealer_id\": 1,\n  \"password\": \"123123\",\n  \"passwordConfirm\": \"123123\",\n  \"checkedPers\": 1,\n  \"checkedRules\": 1,\n  \"token\": \"f20f24b706ec7b0d4a6624516d2072de915edb93cbcedb5913c2e8ccecb59859\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>Статус ответа &quot;OK&quot;</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "profile_id",
            "description": "<p>Идентификатор участника</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "login",
            "description": "<p>Логин участника</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Токен проверки телефона по СМС</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "logged_at",
            "description": "<p>Дата и время последнего входа в систему</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "logged_ip",
            "description": "<p>IP адрес с которого последний раз заходил участник</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "created_at",
            "description": "<p>Дата создания профиля участника</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "header",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "payload",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "secret",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "expires_at",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "used_at",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "used_ip",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Пример успешного ответа:",
          "content": "{\n  \"result\": \"OK\",\n  \"profile_id\": 10,\n  \"login\": \"+79151913583\",\n  \"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcm9maWxlX2lkIjoxMH0.NDZlNDRhY2U3MGY4N2FlMDhiZGIxZGE2MDg5NTY1NTgyYmM4NjQyODdkM2ExMTA4ZDdmMDEzZjE4MjA4YTFmNA\",\n  \"logged_at\": \"2019-01-31 19:23:02\",\n  \"logged_ip\": \"192.168.7.1\",\n  \"created_at\": \"2019-01-31 19:23:02\",\n  \"updated_at\": \"2019-01-31 19:23:02\",\n  \"id\": 2,\n  \"header\": null,\n  \"payload\": null,\n  \"secret\": null,\n  \"expires_at\": null,\n  \"used_at\": null,\n  \"used_ip\": null\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/modules/profiles/frontend/controllers/api/RegisterController.php",
    "groupTitle": "Profiles"
  },
  {
    "type": "post",
    "url": "profiles/api/register/info",
    "title": "Информация при регистрации",
    "description": "<p>Возвращает информацию по регистрации участника</p>",
    "name": "ProfilesRegistrationInfo",
    "group": "Profiles",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>Мобильный телефон (необязательный параметр)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример запроса:",
          "content": "{\n  \"phone\": \"+7 (915) 191-3583\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>Статус ответа &quot;OK&quot;</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "pageRules",
            "description": "<p>Правила акции</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pageRules.url",
            "description": "<p>Адрес страницы</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pageRules.title",
            "description": "<p>Заголовок</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pageRules.content",
            "description": "<p>HTML содержимое</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "pageRules.hide_title",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "pagePers",
            "description": "<p>Правила акции</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pagePers.url",
            "description": "<p>Адрес страницы</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pagePers.title",
            "description": "<p>Заголовок</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "pagePers.content",
            "description": "<p>HTML содержимое</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "pagePers.hide_title",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "profile",
            "description": "<p>Профиль участника</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "profile.profile_id",
            "description": "<p>Индификаторв</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.clubcard",
            "description": "<p>Номер клубной карты</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.full_name",
            "description": "<p>Полное имя</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.first_name",
            "description": "<p>Имя</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.middle_name",
            "description": "<p>Отчество</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.role",
            "description": "<p>Роль</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.gender",
            "description": "<p>Пол</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "profile.city_id",
            "description": "<p>Индификатор города</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.city_local",
            "description": "<p>Название города</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "profile.city",
            "description": "<p>Торговая точка</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "profile.city.id",
            "description": "<p>Индификатор</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.city.title",
            "description": "<p>Название</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "profile.dealer",
            "description": "<p>Торговая точка</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "profile.dealer.id",
            "description": "<p>Индификатор</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.dealer.title",
            "description": "<p>Название</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.specialty",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.avatar_url",
            "description": "<p>URL изобрадения аватарки</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.phone_mobile",
            "description": "<p>Телефон</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.email",
            "description": "<p>Email</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "profile.balance",
            "description": "<p>Баланс (доступно для трат)</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "profile.balance_total",
            "description": "<p>Подтвержденные баллы</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.birthday_on",
            "description": "<p>День рождения</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.registered_at",
            "description": "<p>Дата регистрации</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.checked_at",
            "description": "<p>Дата подтверждения администратором</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.pers_at",
            "description": "<p>Дата согласия на обработку персональных данных</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.created_at",
            "description": "<p>Дата создания</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.blocked_at",
            "description": "<p>Дата блокировки</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.blocked_reason",
            "description": "<p>Причины блокировки</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.banned_at",
            "description": "<p>Дата бана</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.banned_reason",
            "description": "<p>Причины бана</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.account",
            "description": "<p>НДФЛ анкета</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример успешного ответа:",
          "content": "{\n  \"result\": \"OK\",\n  \"pageRules\": {\n    \"url\": \"rules\",\n    \"title\": \"Правила мотивационной программы\",\n    \"content\": \"<p>Правила мотивационной программы. Заполняется из админки.</p>\",\n    \"hide_title\": 0\n  },\n  \"pagePers\": {\n    \"url\": \"pers\",\n    \"title\": \"Согласие на использование персональных данных\",\n    \"content\": \"<p>Согласие на использование персональных данных. Заполняется из админки.</p>\",\n    \"hide_title\": 0\n  },\n  \"profile\": {\n    \"profile_id\": 1,\n    \"clubcard\": null,\n    \"full_name\": \"Дмитрий Новиков Александрович\",\n    \"first_name\": \"Новиков\",\n    \"last_name\": \"Дмитрий\",\n    \"middle_name\": \"Александрович\",\n    \"role\": \"regular\",\n    \"gender\": null,\n    \"city_id\": 1,\n    \"city_local\": \"Москва, Москва и Московская обл.\",\n    \"city\": {\n      \"id\": 1,\n      \"title\": \"Москва\"\n    },\n    \"dealer\": {\n      \"id\": 1,\n      \"name\": \"Тестовая торговая точка 1\"\n    },\n    \"specialty\": null,\n    \"avatar_url\": \"http://api.tikkurila.local/images/avatar_blank.png?v=2\",\n    \"phone_mobile\": \"+79151913583\",\n    \"email\": \"nd@msforyou.ru\",\n    \"balance\": 0,\n    \"balance_total\": 0,\n    \"birthday_on\": null,\n    \"registered_at\": null,\n    \"checked_at\": null,\n    \"pers_at\": null,\n    \"created_at\": \"2019-02-01 11:52:52\",\n    \"blocked_at\": null,\n    \"blocked_reason\": null,\n    \"banned_at\": null,\n    \"banned_reason\": null,\n    \"account\": null\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/modules/profiles/frontend/controllers/api/RegisterController.php",
    "groupTitle": "Profiles"
  },
  {
    "type": "post",
    "url": "sales/api/category/list",
    "title": "Список категорий",
    "description": "<p>Получить список всех категорий</p>",
    "name": "SalesCategoryList",
    "group": "Sales",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>Статус ответа &quot;OK&quot;</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "categories",
            "description": "<p>Список категорий</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "categories.id",
            "description": "<p>Идентификатор</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "categories.name",
            "description": "<p>Название</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример успешного ответа:",
          "content": "{\n  \"result\": \"OK\",\n  \"categories\": [\n    {\n    \"id\": 185,\n     \"name\": \"Адгезионный грунт\"\n    },\n    {\n      \"id\": 171,\n      \"name\": \"Антисептик кроющий\"\n     }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/modules/sales/frontend/controllers/api/CategoryController.php",
    "groupTitle": "Sales"
  },
  {
    "type": "post",
    "url": "sales/api/sales/create",
    "title": "Добавить продажу",
    "description": "<p>Добавить новую продажу</p>",
    "name": "SalesCreate",
    "group": "Sales",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "profile_id",
            "description": "<p>Индификатор участника</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "action_id",
            "description": "<p>Индификатор акции</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "number",
            "description": "<p>Номер накладной</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sold_on_local",
            "description": "<p>Дата продажи (в формате &quot;dd.mm.yyyy&quot;)</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "positions",
            "description": "<p>Позиции</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "positions.product_id",
            "description": "<p>Индификатор товара</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "positions.quantity",
            "description": "<p>Количество</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "positions.bonuses",
            "description": "<p>Стоимость</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "documents",
            "description": "<p>Документы</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "documents.name",
            "description": "<p>Название</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "documents.base64",
            "description": "<p>Изображение</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример запроса:",
          "content": "{\n  \"profile_id\": 1,\n  \"action_id\": 1,\n  \"number\": \"123123\",\n  \"sold_on_local\": \"01.02.2019\",\n  \"positions\": [\n    {\n      \"product_id\": 1,\n      \"quantity\": 1,\n      \"bonuses\": 10\n    },\n    {\n      \"product_id\": 199,\n      \"quantity\": 2,\n      \"bonuses\": 150\n    }\n  ],\n  \"documents\": [\n    {\n      \"name\": \"burj-khalifa-dubai-03.jpg\",\n      \"base64\": \"data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAZABkAAD=\"\n    },\n    {\n      \"name\": \"empty.png\",\n      \"base64\": \"data:image/png;base64,iVBORw0KGgo=\"\n    }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>Статус ответа &quot;OK&quot;</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "sale",
            "description": "<p>Продажа</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "sale.id",
            "description": "<p>Индификатор</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "sale.action_id",
            "description": "<p>Индификатор акции</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "sale.status",
            "description": "<p>Статус</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "sale.status_label",
            "description": "<p>Описание статуса</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "sale.bonuses",
            "description": "<p>Сумма бонусов</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "sale.sold_on_local",
            "description": "<p>Дата продажи</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "sale.created_at",
            "description": "<p>Дата создания</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "sale.number",
            "description": "<p>Номер накладной</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "sale.documents",
            "description": "<p>Документы</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "sale.documents.id",
            "description": "<p>Индификатор</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sale.documents.name",
            "description": "<p>Имя файла</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sale.documents.url",
            "description": "<p>URL адрес</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "sale.positions",
            "description": "<p>Список позиций</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "sale.positions.product_id",
            "description": "<p>Индификатор товара</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "sale.positions.quantity",
            "description": "<p>Количество товаров</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "sale.positions.bonuses",
            "description": "<p>Суммарная стоимость</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "sale.positions.product",
            "description": "<p>Товар</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "sale.positions.product.id",
            "description": "<p>Идентификатор товара</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "sale.positions.product.group_id",
            "description": "<p>Идентификатор бренда</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "sale.positions.product.category_id",
            "description": "<p>Идентификатор категории</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "sale.positions.product.unit_id",
            "description": "<p>Идентификатор единицы измерения</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sale.positions.product.name",
            "description": "<p>Название</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sale.positions.product.code",
            "description": "<p>Номенклатура</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "sale.positions.product.enabled",
            "description": "<p>Признак активности</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "sale.positions.product.weight",
            "description": "<p>Фасовка</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "sale.positions.product.group",
            "description": "<p>Бренд товара</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "sale.positions.product.group.id",
            "description": "<p>Индификаторв бренда</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sale.positions.product.group.name",
            "description": "<p>Название бренда</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "sale.positions.product.category",
            "description": "<p>Категория товара</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "sale.positions.product.category.id",
            "description": "<p>Индификаторв категории</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sale.positions.product.category.name",
            "description": "<p>Название категории</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "sale.positions.product.unit",
            "description": "<p>Единица измерения</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "sale.positions.product.unit.id",
            "description": "<p>Индификатов единицы измерения</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sale.positions.product.unit.name",
            "description": "<p>Название единицы измерения</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sale.positions.product.unit.short_name",
            "description": "<p>Короткое название единицы измерения</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "sale.positions.product.unit.quantity_divider",
            "description": "<p>Делитель количества</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "sale.history",
            "description": "<p>История изменения</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "sale.history.id",
            "description": "<p>Индификатор</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sale.history.created_at",
            "description": "<p>Дата создания</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sale.history.note",
            "description": "<p>Описание</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sale.history.comment",
            "description": "<p>Коментарий</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "sale.action",
            "description": "<p>Акция</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "sale.action.id",
            "description": "<p>Индификатор акции</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sale.action.title",
            "description": "<p>Название</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sale.action.short_description",
            "description": "<p>Короткое описание</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sale.action.description",
            "description": "<p>Описание</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sale.action.start_on",
            "description": "<p>Дата начала</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sale.action.end_on",
            "description": "<p>Дата окончания</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "sale.action.has_products",
            "description": "<p>Отображать или нет выбор продукции</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "sale.saleAction",
            "description": "<p>Бонус за акцию</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "sale.saleAction.id",
            "description": "<p>Индификатор начисления</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "sale.saleAction.bonuses",
            "description": "<p>Баллы</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "sale.saleAction.status",
            "description": "<p>Статус (null - баллы начислены или 'paid' - баллы доступны дял трат)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример успешного ответа:",
          "content": "{\n  \"result\": \"OK\",\n  \"sale\": {\n    \"id\": 32,\n    \"action_id\": 1,\n    \"status\": \"adminReview\",\n    \"status_label\": \"На проверке\",\n    \"bonuses\": 160,\n    \"sold_on_local\": \"01.02.2019\",\n    \"created_at\": \"11.02.2019\",\n    \"number\": \"123123\",\n    \"documents\": [\n      {\n        \"id\": 76,\n        \"name\": \"32_document_5c618a1d62697.jpg\",\n        \"url\": \"http://api.tikkurila.local/data/sales/32_document_5c618a1d62697.jpg\"\n      },\n      {\n        \"id\": 77,\n        \"name\": \"32_document_5c618a1d6b258.png\",\n        \"url\": \"http://api.tikkurila.local/data/sales/32_document_5c618a1d6b258.png\"\n      }\n    ],\n    \"positions\": [\n      {\n        \"product_id\": 1,\n        \"quantity\": 1,\n        \"bonuses\": 10,\n        \"product\": {\n          \"id\": 1,\n          \"group_id\": 1,\n          \"category_id\": 1,\n          \"unit_id\": 1,\n          \"name\": \"Антисептик SPILL WOOD бесцв п/гл 0,9л\",\n          \"code\": \"700007255\",\n          \"enabled\": 1,\n          \"weight\": 0.9,\n          \"group\": {\n            \"id\": 1,\n            \"name\": \"Finncolor\"\n          },\n          \"category\": {\n            \"id\": 1,\n            \"name\": \"Антисептик лессирующий\"\n          },\n           \"unit\": {\n            \"id\": 1,\n            \"name\": \"Литр\",\n            \"short_name\": \"л.\",\n            \"quantity_divider\": 100\n          }\n        }\n      }\n    ],\n    \"history\": [\n      {\n        \"id\": 20,\n        \"created_at\": \"11.02.2019 17:41\",\n        \"note\": \"Продажа обновлена участником\",\n        \"comment\": null\n      }\n    ],\n    \"action\": {\n      \"id\": 5,\n      \"title\": \"Акция для PROFCLUB\",\n      \"short_description\": \"10% от продаж\",\n      \"description\": \"Участники получают 10% от всех подтвержденных продаж\",\n      \"start_on\": \"2019-02-01\",\n      \"end_on\": \"2019-03-10\",\n      \"has_products\": 1\n    }\n    \"saleAction\": {\n      \"id\": 10,\n      \"bonuses\": 290,\n      \"status\": null\n    }\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/modules/sales/frontend/controllers/api/SaleController.php",
    "groupTitle": "Sales"
  },
  {
    "type": "post",
    "url": "sales/api/group/list",
    "title": "Список брендов",
    "description": "<p>Получить список всех брендов</p>",
    "name": "SalesGroupList",
    "group": "Sales",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>Статус ответа &quot;OK&quot;</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "groups",
            "description": "<p>Список брендов</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "groups.id",
            "description": "<p>Идентификатор</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "groups.name",
            "description": "<p>Название</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример успешного ответа:",
          "content": "{\n  \"result\": \"OK\",\n  \"groups\": [\n    {\n    \"id\": 21,\n     \"name\": \"Finncolor\"\n    },\n    {\n      \"id\": 21,\n      \"name\": \"TEKS\"\n     }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/modules/sales/frontend/controllers/api/GroupController.php",
    "groupTitle": "Sales"
  },
  {
    "type": "post",
    "url": "sales/api/product/list",
    "title": "Список товаров",
    "description": "<p>Получить список всех товаров</p>",
    "name": "SalesProductList",
    "group": "Sales",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "group_id",
            "description": "<p>Индификатор бренда (необязательный параметр)</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "category_id",
            "description": "<p>Индификатор категории (необязательный параметр)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример запроса:",
          "content": "{\n  \"category_id\": 185\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>Статус ответа &quot;OK&quot;</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "products",
            "description": "<p>Список товаров</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "products.id",
            "description": "<p>Идентификатор товара</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "products.group_id",
            "description": "<p>Идентификатор бренда</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "products.category_id",
            "description": "<p>Идентификатор категории</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "products.unit_id",
            "description": "<p>Идентификатор единицы измерения</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "products.name",
            "description": "<p>Название</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "products.code",
            "description": "<p>Номенклатура</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "products.enabled",
            "description": "<p>Признак активности</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "products.weight",
            "description": "<p>Фасовка</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "products.group",
            "description": "<p>Бренд товара</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "products.group.id",
            "description": "<p>Индификаторв бренда</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "products.group.name",
            "description": "<p>Название бренда</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "products.category",
            "description": "<p>Категория товара</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "products.category.id",
            "description": "<p>Индификаторв категории</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "products.category.name",
            "description": "<p>Название категории</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "products.unit",
            "description": "<p>Единица измерения</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "products.unit.id",
            "description": "<p>Индификатов единицы измерения</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "products.unit.name",
            "description": "<p>Название единицы измерения</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "products.unit.short_name",
            "description": "<p>Короткое название единицы измерения</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "products.unit.quantity_divider",
            "description": "<p>Делитель количества</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример успешного ответа:",
          "content": "{\n  \"result\": \"OK\",\n  \"products\": [\n    {\n      \"id\": 6859,\n      \"group_id\": 3,\n      \"category_id\": 2,\n      \"unit_id\": 1,\n      \"name\": \"Адгезионный грунт MINERAL GRUND RPA 2,7л\",\n      \"code\": \"700010658\",\n      \"enabled\": 1,\n      \"weight\": 2.7,\n      \"group\": {\n        \"id\": 21,\n        \"name\": \"Finncolor\"\n      },\n      \"category\": {\n        \"id\": 185,\n        \"name\": \"Адгезионный грунт\"\n      },\n      \"unit\": {\n        \"id\": 1,\n        \"name\": \"Литр\",\n        \"short_name\": \"л.\",\n        \"quantity_divider\": 100\n      }\n    },\n    {\n      \"id\": 6860,\n      \"name\": \"Адгезионный грунт MINERAL GRUND RPA 9л\",\n      \"code\": \"700010659\",\n      \"enabled\": 1,\n      \"weight\": 9,\n      \"group\": {\n        \"id\": 21,\n        \"name\": \"Finncolor\"\n      },\n      \"category\": {\n        \"id\": 185,\n        \"name\": \"Адгезионный грунт\"\n      },\n      \"unit\": {\n        \"id\": 1,\n        \"name\": \"Литр\",\n        \"short_name\": \"л.\",\n        \"quantity_divider\": 100\n      }\n    }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/modules/sales/frontend/controllers/api/ProductController.php",
    "groupTitle": "Sales"
  },
  {
    "type": "post",
    "url": "sales/api/product/view",
    "title": "Просмотр товара",
    "description": "<p>Получить товар по его индификатору</p>",
    "name": "SalesProductView",
    "group": "Sales",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "product_id",
            "description": "<p>Индификатор товара</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример запроса:",
          "content": "{\n  \"product_id\": 185\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>Статус ответа &quot;OK&quot;</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "product",
            "description": "<p>Объект товара</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "product.id",
            "description": "<p>Идентификатор товара</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "product.group_id",
            "description": "<p>Идентификатор бренда</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "product.category_id",
            "description": "<p>Идентификатор категории</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "product.unit_id",
            "description": "<p>Идентификатор единицы измерения</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "product.name",
            "description": "<p>Название</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "product.code",
            "description": "<p>Номенклатура</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "product.enabled",
            "description": "<p>Признак активности</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "product.weight",
            "description": "<p>Фасовка</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "product.group",
            "description": "<p>Бренд товара</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "product.group.id",
            "description": "<p>Индификаторв бренда</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "product.group.name",
            "description": "<p>Название бренда</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "product.category",
            "description": "<p>Категория товара</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "product.category.id",
            "description": "<p>Индификаторв категории</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "product.category.name",
            "description": "<p>Название категории</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "product.unit",
            "description": "<p>Единица измерения</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "product.unit.id",
            "description": "<p>Индификатов единицы измерения</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "product.unit.name",
            "description": "<p>Название единицы измерения</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "product.unit.short_name",
            "description": "<p>Короткое название единицы измерения</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "product.unit.quantity_divider",
            "description": "<p>Делитель количества</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример успешного ответа:",
          "content": "{\n  \"result\": \"OK\",\n  \"product\": {\n    \"id\": 6859,\n    \"group_id\": 3,\n    \"category_id\": 2,\n    \"unit_id\": 1,\n    \"name\": \"Адгезионный грунт MINERAL GRUND RPA 2,7л\",\n    \"code\": \"700010658\",\n    \"enabled\": 1,\n    \"weight\": 2.7,\n    \"group\": {\n      \"id\": 21,\n      \"name\": \"Finncolor\"\n    },\n    \"category\": {\n      \"id\": 185,\n      \"name\": \"Адгезионный грунт\"\n    },\n    \"unit\": {\n      \"id\": 1,\n      \"name\": \"Литр\",\n      \"short_name\": \"л.\",\n      \"quantity_divider\": 100\n    }\n  }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/modules/sales/frontend/controllers/api/ProductController.php",
    "groupTitle": "Sales",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ProductNotFound",
            "description": "<p>Товар не найден</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример ошибки \"Товар не найден\":",
          "content": "HTTP/1.1 400 Bad Request\n{\n    \"result\": \"FAIL\",\n    \"errors\": {\n        \"product_id\": \"Товар не найден\"\n    }\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "sales/api/unit/list",
    "title": "Список ед. измерения",
    "description": "<p>Получить список всех единиц измерения</p>",
    "name": "SalesUnitList",
    "group": "Sales",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>Статус ответа &quot;OK&quot;</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "units",
            "description": "<p>Список единиц измерения</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "units.id",
            "description": "<p>Идентификатор</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "units.name",
            "description": "<p>Название</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "units.short_name",
            "description": "<p>Короткое название</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "units.quantity_divider",
            "description": "<p>Делитель количества</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Пример успешного ответа:",
          "content": "{\n  \"result\": \"OK\",\n  \"units\": [\n    {\n      \"id\": 2,\n      \"name\": \"Килограмм\",\n      \"short_name\": \"кг.\",\n      \"quantity_divider\": 100\n    },\n    {\n      \"id\": 1,\n      \"name\": \"Литр\",\n      \"short_name\": \"л.\",\n      \"quantity_divider\": 100\n    }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/modules/sales/frontend/controllers/api/UnitController.php",
    "groupTitle": "Sales"
  }
] });
