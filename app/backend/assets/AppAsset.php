<?php

namespace backend\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $sourcePath = '@backend/assets/dist';

    public $css = [
        'css/main.css',
    ];

    public $js = [
        'js/common.js',
        'js/profiles.js',
        'js/leaders.js',
        'js/actions.js',
    ];

}