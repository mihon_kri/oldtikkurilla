/**
 * Show/hide relation fields
 * @param options
 */
var fieldHandler = function (options) {
    let $element = $(options.value);
    let value = $element.is(':checkbox') ? $element.is(':checked') : $element.val();

    $.each(options.fields, function (selector, activeValues) {
        let $el = $(selector);

        if (activeValues.find(element => value == element)) {
            $el.removeClass('hidden').hide().fadeIn('fast');
        } else {
            $el.addClass('hidden');
            $('input', $el).val('');
        }
    });
};