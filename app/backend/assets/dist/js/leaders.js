$(function () {

    let $form = $('.js-leader-form');

    if (!$form.length) {
        return false;
    }

    const ADMIN_REGION = 'ADMIN_REGION';
    const ADMIN_OP = 'ADMIN_OP';
    const ADMIN_JUNIOR = 'ADMIN_JUNIOR';

    let options = {
        'value': 'select.js-leader-role-select',
        'fields': {
            '.js-leader-region-field': [ADMIN_REGION],
            '.js-leader-adminsOp-field': [ADMIN_REGION],
            '.js-leader-leader-field': [ADMIN_OP],
            '.js-leader-profiles-field': [ADMIN_OP],
            '.js-regions_for_role_admin': [ADMIN_JUNIOR],
        }
    };

    fieldHandler(options);
    $('.js-leader-role-select').on('change', function() {
        fieldHandler(options);
    });


});