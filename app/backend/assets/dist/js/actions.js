$(function () {
    let $form = $('.js-action-form');

    if (!$form.length) {
        return false;
    }

    /**
     * Action type ids from backend
     */
    let TYPE_COMPLETE_PERSONAL_PLAN = null;
    let TYPE_BONUSES_AMOUNT = null;
    let TYPE_COMPLETE_PLAN = null;
    let TYPE_PRODUCTS = null;

    /**
     * Init action types ids
     */
    $.each(window.actionTypes, function (index, type) {
        if (type.className.indexOf('BonusesAmountActionType') > -1) {
            TYPE_BONUSES_AMOUNT = type.id;
        }

        if (type.className.indexOf('PlanCompleteActionType') > -1) {
            TYPE_COMPLETE_PLAN = type.id;
        }

        if (type.className.indexOf('PlanCompletePersonalActionType') > -1) {
            TYPE_COMPLETE_PERSONAL_PLAN = type.id;
        }

        if (type.className.indexOf('ProductsActionType') > -1) {
            TYPE_PRODUCTS = type.id;
        }
    });

    /**
     * Action types radio button and relation field options
     */
    let typeFieldOptions = {
        'value': '.js-action-type input:checked', // value field selector
        'fields': {
            '.js-action-bonuses-amount': [TYPE_BONUSES_AMOUNT], // field selector: [active values]
            '.js-action-plan-amount': [TYPE_COMPLETE_PLAN],
            '.js-personal_plan_formula' : [TYPE_COMPLETE_PERSONAL_PLAN]
        }

    };

    fieldHandler(typeFieldOptions);
    $('.js-action-type .radio').on('click', function () {
        fieldHandler(typeFieldOptions);
    });


    /**
     * Pay types select and relation field options
     */
    let payFieldOptions = {
        'value': 'select.js-action-pay-type', // value field selector
        'fields': { // field selector: [active values]
            '.js-action-pay-threshold': ['threshold'],
        }

    };

    fieldHandler(payFieldOptions);
    $('.js-action-pay-type').on('change', function() {
        fieldHandler(payFieldOptions);
    });



    /**
     * Has products checkbox field options
     */
    let productOptions = {
        'value': '.js-action-has-products input[type=checkbox]', // value field selector
        'fields': { // field selector: [active values]
            '.js-action-products-box': [1],
        }

    };

    fieldHandler(productOptions);
    $('.js-action-has-products').on('click', function() {
        fieldHandler(productOptions);
    });


    $('.js-action-bonuses-info-show').on('click', function() {
        let $box = $('.js-action-bonuses-info-box');

        if ($box.is(':visible')) {
            $box.fadeOut('fast');
        } else {
            $box.hide().removeClass('hidden').fadeIn('fast');
        }

        return false;
    });


});