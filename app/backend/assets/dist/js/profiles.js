$(function () {

    let $form = $('.js-profile-form');

    if (!$form.length) {
        return false;
    }

    const ROLE_PROFCLUB = 'profclub';
    const ROLE_REGULAR = 'regular';

    let options = {
        'value': 'select.js-profile-role-select',
        'fields': {
            '.js-profile-dealer-field': [ROLE_REGULAR],
            '.js-profile-clubcard-field': [ROLE_PROFCLUB],
        }

    };

    fieldHandler(options);
    $('.js-profile-role-select').on('change', function() {
        fieldHandler(options);
    });

});