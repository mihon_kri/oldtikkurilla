<?php
/** @var \yii\db\ActiveQuery $files */
/** @var yii\web\View $this */
/** @var boolean $form */

\ms\loyalty\assets\FancyBoxRunAsset::register($this);
$form = empty($form) ? false : true;
?>

<?php if ($form): ?>
	<div class="row" style="margin-bottom:20px">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">

            <?php foreach ($files->each() as $file): ?>

                <?php
                /** @var \ms\files\attachments\common\models\AttachedFile $file */
                $isPdf = strpos($file->file_name, '.pdf') !== false;
                $isXls = strpos($file->file_name, '.xls') != false;
                $isXlsx = strpos($file->file_name, '.xlsx') != false;
                $url = $file->getUrl();
                ?>
				<a href="<?= $url ?>" title="<?= $url ?>" target="_blank" style="margin-right:20px"
				   class="btn btn-default <?= $isPdf ? 'fancybox-pdf' : 'fancybox' ?>">
                    <?php if ($isPdf): ?>
						<i class="fa fa-file-pdf" style="color: darkred; font-size: 40px;"></i> PDF
                    <?php elseif($isXlsx):?>
                        <i class="fa fa-file-excel" style="color: darkred; font-size: 40px;"></i> XLS
                    <?php elseif($isXls):?>
                        <i class="fa fa-file-excel" style="color: darkred; font-size: 40px;"></i> XLSX
                    <?php else: ?>
						<img src="<?= $url ?>" style="max-width:150px; max-height:100px;"/>
                    <?php endif; ?>
				</a>
            <?php endforeach; ?>
		</div>
	</div>
<?php else: ?>

    <?php foreach ($files->each() as $file): ?>

        <?php
        /** @var \ms\files\attachments\common\models\AttachedFile $file */
        $isPdf = strpos($file->file_name, '.pdf') !== false;
        $url = $file->getUrl();
        ?>
		<div style="text-align:center; margin-bottom:5px;">
			<a href="<?= $url ?>" title="<?= $url ?>" target="_blank"
			   class="btn btn-default <?= $isPdf ? 'fancybox-pdf' : 'fancybox' ?>">
                <?php if ($isPdf): ?>
					<i class="fa fa-file-pdf" style="color: darkred; font-size: 40px;"></i> PDF
                <?php else: ?>
					<img src="<?= $url ?>" style="max-width:100px; max-height:100px;"/>
                <?php endif; ?>
			</a>
		</div>
    <?php endforeach; ?>

<?php endif; ?>